<!--
function initImage(srcUrl) {
	$('#p7ssm_im').loadthumb({src:srcUrl});	
	$(".SmallImg ul li img").each(function(i){				
		if(this.src.indexOf(srcUrl)!=-1) {$(this).addClass("down");}else{$(this).removeClass("down");}
	});
}
function opendlg()
{
var href='/products/zoom.asp?id='+$("#pid").attr("value");
window.showModalDialog(href,window,'dialogWidth:700px;status:no;toolbar:no;menubar:no;scrollbars:no;resizable:no;location:no;dialogHeight:702px');
}
//选中颜色
function selectColor(id)
{
	var obj=document.getElementById(id);	
	if(obj.className=="selected") 
		obj.className="null";
	else
		obj.className="selected";	
	var colorshow="";colorinput="";
	var objlistframe=document.getElementById("colorList");
	var objlist=objlistframe.getElementsByTagName("li");
	for(var i=0;i<objlist.length;i++)
	{		
		if(objlist[i].className=="selected")
			{
				if(colorshow==""){colorshow=objlist[i].title;} else {colorshow=colorshow+","+objlist[i].title;}
				if(colorinput==""){colorinput=objlist[i].value;} else {colorinput=colorinput+","+objlist[i].value;}
			}
	}	
	if(colorshow!="")
	{
		document.getElementById("colorShow").innerText="当前您选中的颜色:"+colorshow;
		document.getElementById("colorInput").value=colorinput;		
	}
	else
	{
		document.getElementById("colorShow").innerText="";
		document.getElementById("colorInput").value="";
	}
	//document.getElementById("gm").href="/products/savetocart.asp?id="+$("#pid").attr("value")+"&color="+document.getElementById("colorInput").value+"&num="+document.getElementById("num").value;
}
//检查输入的产品数量
function chkNum()
{
	var numobj=document.getElementById("num");	
	var num=numobj.value;
	var hrefobj=document.getElementById("gm");
	var cmin=$("#min").attr("value");
	var cmax=$("#max").attr("value");
	cmin=parseFloat(cmin);
	cmax=parseFloat(cmax);	
	if(isNaN(num)){
		alert("请输入有效的产品数量");
		//numobj.focus();
		//numobj.select();
		return false;
	}
	num=parseFloat(num);
	if((cmax<cmin)&&cmax!=-1)
		{alert("产品库存不足，不能购买");return false;}
	else if((num<cmin||num>cmax)&&cmax!=-1){
		alert("产品数量必须在"+cmin+"和"+cmax+"之间");
		//numobj.focus();
		//numobj.select();
		return false;
	}
	//document.getElementById("gm").href="/products/savetocart.asp?id="+$("#pid").val()+"&color="+document.getElementById("colorInput").value+"&num="+document.getElementById("num").value;	
	return true;	
}
//添加到购物车是确定
function chkAddShopCart()
{
	var numobj=document.getElementById("num");	
	var num=numobj.value;
	var pid=$("#pid").val();
	var cmin=$("#min").attr("value");
	var cmax=$("#max").attr("value");
	cmin=parseFloat(cmin);
	cmax=parseFloat(cmax);	
	if(isNaN(num)){
		alert("请输入有效的产品数量");
		//numobj.focus();
		//numobj.select();
		return false;
	}
	num=parseFloat(num);
	if((cmax<cmin)&&cmax!=-1)
		{alert("产品库存不足，不能购买");return false;}
	else if((num<cmin||num>cmax)&&cmax!=-1){
		alert("产品数量必须在"+cmin+"和"+cmax+"之间");
		//numobj.focus();
		//numobj.select();
		return false;
	}
	var colorobj=document.getElementById("colorInput");	
	if(colorobj.value==""){	
		//return confirm("您还没有选中产品颜色，默认为当前产品的所有产品颜色，继续吗？")
	}	
	addShopcart(pid,num,colorobj.value);
	return false;
}
//增加鼠标事件
function addMouseover(listid,obj)
{	
	 $("#"+listid+" li").each(function(i){	
					  if(this!=obj)
					  $("#"+this.id+i).hide("fast");
					  else
					  $("#"+this.id+i).show("fast");
			})
}

//将浏览历史保存到cookies
function setHistory(id)
{
	var listid=cjx.GetCookies("historyid");
	var tlistid='';
	if(chkPara(listid)){
		if(listid.indexOf(id)==-1){
			var listarr=listid.split(",");			
			for(var i=0;i<listarr.length;i++)
				{
					if(tlistid=='') 
						tlistid=listarr[i]; 
					else 
						tlistid=tlistid+','+listarr[i];
					if(i==9) break;
					
				}
			tlistid=id+","+tlistid;
		}
		
	}else{
		tlistid=id;
	}
	var Days = 1;   
	var exp1 = new Date();    
	exp1.setTime(exp1.getTime() + Days*24*60*60*1000);   
	document.cookie="historyid="+tlistid+";path=/;expires=" + exp1.toGMTString()+";";
	document.cookie="direct=/html/pro"+id+".htm;path=/;expires=" + exp1.toGMTString()+";";
	//alert(GetCookies("direct"));
}

//获取产品评论
function getPL()
{
	$.ajax({
	type:"get",	
	dataType:"html",	
	url:"/products/getProductsPl.asp",
	beforeSend:function(XMLHttpRequest){
		$("#culturelist").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
		},
	success:function(data, textStatus){
		$("#culturelist").html(data);
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			alert("产品评论结果可能没有显示");
		}
	
	});
}
//获取产品浏览历史记录
function getHistory(bid)
{
	$.ajax({
	type:"get",
	data:"bid="+bid,
	dataType:"html",	
	url:"/products/getHistory.asp",
	beforeSend:function(XMLHttpRequest){
		$("#prolist3").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
		},
	success:function(data, textStatus){
		$("#prolist3").html(data);
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			alert("浏览历史记录结果可能没有显示");
		}
	
	});
}
//获取热门产品
function getHot(bid)
{
	$.ajax({
	type:"get",
	data:"bid="+bid,
	dataType:"html",	
	url:"/products/getHot.asp",
	beforeSend:function(XMLHttpRequest){
		$("#prolist2").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
		},
	success:function(data, textStatus){
		$("#prolist2").html(data);
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			alert("热门产品结果可能没有显示");
		}
	
	});
}
//获取热门产品
function relaxProducts(id,k)
{
	$.ajax({
	type:"get",
	data:"id="+id+"&bid="+k,
	dataType:"html",	
	url:"/products/getrelax.asp",
	beforeSend:function(XMLHttpRequest){
		$("#relaxProducts").html("<img src='/common/jquery/images/loading.gif' border='0' height='30' width='30' />");
		},
	success:function(data, textStatus){
		$("#relaxProducts").html(data);
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			
		}
	
	});
}
//获取产品价格 在这里同时也设置产品的浏览次数
function getprice(pid)
{
	$.ajax({
	type:"get",
	data:"id="+pid,
	dataType:"html",	
	url:"/products/getPrice.asp",
	beforeSend:function(XMLHttpRequest){		
		},
	success:function(data, textStatus){
		if(data=="0"){
			alert("产品价格可能没有正确显示，具体价格请参考产品报价单");}
		else{
			var tarr=data.split("|");			
			$("#price3").html("<em>"+tarr[0]+"</em>");
			$("#spmax1").html(tarr[1]);
			$("#spmax2").html(tarr[1]);			
			}
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			alert("产品价格没有显示，请参考报价单");
		}
	
	});
}
function changejpuery(objid)
{
	if($("#"+objid).css("display")=="none")
		$("#"+objid).show("fast");
	else
		$("#"+objid).hide("fast");	
	return false;
}
function messageLogin(obj)
{		
	if($("#niming").attr("checked")){		
		$("#spLogin").css("display","none");
	}else{
		$("#spLogin").css("display","");
		if(cjx.chkCookiesValue("email",4,20)){			
			$("#username").attr("value",cjx.GetCookies("email"));
			$("#password").attr("value",cjx.GetCookies("pass"));
			}
	}
}
//详细页的产品介绍切换
function setTab(m,n){
var menu=document.getElementById("tab"+m).getElementsByTagName("li");
var div=document.getElementById("tablist"+m).getElementsByTagName("div");
var showdiv=[];
for (i=0; j=div[i]; i++){
if ((" "+div[i].className+" ").indexOf(" tablist ")!=-1){
showdiv.push(div[i]);
}
}
for(i=0;i<menu.length;i++)
{
menu[i].className=i==n?"now":"";
showdiv[i].style.display=i==n?"block":"none";
}
}
//获取帮助内容
function getServer(spanid,id)
{
	$.ajax({
	type:"get",
	data:"id="+id,
	dataType:"html",	
	url:"/products/getServer.asp",
	beforeSend:function(XMLHttpRequest){		
		},
	success:function(data, textStatus){		
		$("#"+spanid).html(data);		
		},
	complete: function(XMLHttpRequest, textStatus){
			//HideLoading();
		},
	error: function(){
			//alert("产品价格没有显示，请参考报价单");
		}
	
	});
}
//页面加载完成进行的操作
document.write("<script language='javascript' src='/qq/qq.js'></script>");
$(document).ready(function(){						  
						   $("#smallPicList").show("slow");						  
						   $("#prolist2 li").bind("mouseover",function(){addMouseover("prolist2",this);});
						   $("#prolist3 li").bind("mouseover",function(){addMouseover("prolist3",this);});	
						  // $(".CateList:first").show("fast");						 
						  if(cjx.chkCookiesValue("email",4,20)){
						   $("#spLogin").css("display","");
						   $("#username").attr("value",cjx.GetCookies("email"));
						   $("#password").attr("value",cjx.GetCookies("pass"));						  
						   $("#niming").attr("checked",false);
						  }else{							 
							$("#spLogin").hide("fast");
							$("#niming").attr("checked",true);
						  }	
						   $(".mybigpic").hover(function(e){showBigPicDiv(this,e.pageY,e.pageX); },
                            function(){ hideBigPicDiv();
                          });			  						   document.cookie="propath="+escape(location.href);
						  
						  
})
-->