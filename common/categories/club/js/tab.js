    window.onload=resetTab;
    function getTabTitle(tab) {
        var childNodesList=tab.childNodes;
        var titleNodes=new Array();
        var j=0;
        var i;
        for (i=0;i<childNodesList.length;i++) {
            if(childNodesList[i].nodeName=="H1") {
                titleNodes[j]=childNodesList[i];
                j++;
            }
        }
        return titleNodes;
    }
    function getTabContent(tab) {
        var childNodesList=tab.childNodes;
        var tabContent=new Array();
        var j=0;
        var i;
        for (i=0;i<childNodesList.length;i++) {
            if(childNodesList[i].nodeName=="DIV") {
                tabContent[j]=childNodesList[i];
                j++;
            }
        }
        return tabContent;
    }
    function resetTab() {
        var allDiv=document.getElementsByTagName("div");
        var tab=new Array();
        var j=0;
        var i;
        for (i=0;i<allDiv.length;i++) {
            if(allDiv[i].className=="tabs") {
                tab[j]=allDiv[i];
                j++;
            }
        }
        var tabTitle,tabContent;
        for(i=0;i<tab.length;i++) {
            tabTitle=getTabTitle(tab[i]);
            tabTitle[0].className="selectTab";
            tabContent=getTabContent(tab[i]);
            tabContent[0].className="selectContent";
            for (j=1;j<tabTitle.length;j++) {
                tabTitle[j].className="unselectTab";
                tabContent[j].className="unselectContent";
            }
        }
    }
    function changTab(tab) {
        var tabTitle,tabContent,i;
        if(tab.className!="selectTab") {
            tabTitle=getTabTitle(tab.parentNode);
            tabContent=getTabContent(tab.parentNode);
            for(i=0;i<tabTitle.length;i++) {
                if(tabTitle[i].className=="selectTab") {
                    tabTitle[i].className="unselectTab";
                }
                if(tabContent[i].className=="selectContent") {
                    tabContent[i].className="unselectContent";
                }
            }
        tab.className="selectTab";
        for(i=0;i<tabTitle.length;i++) {
            if(tabTitle[i].className=="selectTab") {
                tabContent[i].className="selectContent";
                break;
            }
        }
        }
    }