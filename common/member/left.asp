﻿<!--left-->
<div class="PagesLeft">
<div class="column2">
<div class="ColumnTitle">
<div class="title">
<h2>我的帐户</h2>
</div>
</div>
<div class="SubAboutTitle">个人信息</div>
<div class="about"> 
<ul>
<li><a href="basic.asp">基本资料</a></li>
<li><a href="password.asp">密码修改</a></li>
<li><a href="money.asp">预存款查看</a></li>
<li><a href="jifen.asp">积分管理</a></li>
<li><a href="djifen.asp">历史消费积分</a></li>
</ul>
</div>
<div class="SubAboutTitle">交易管理</div>
<div class="about"> 
<ul>
<li><a href="index.asp">订单管理</a></li>
<li><a href="address.asp">收货地址管理</a></li>
<li><a href="/products/my_cart.asp">购物车</a></li>
</ul>
</div>
<div class="SubAboutTitle">应用管理</div>
<div class="about"> 
<ul>
<li><a href="collection.asp">我的收藏</a></li>
<li><a href="Recommended.asp">推荐给朋友</a></li>
<li><a href="book.asp">意见及评论</a></li>
<li><a href="messages.asp">短信息</a></li>
<li><a href="../reg/logout.asp">退出登录</a></li>
</ul>
</div>

</div>

<div class="column2">
<div class="ColumnTitle">
<div class="title">
<h2>常见问题</h2>
</div>
</div>

<div class="about"> 
<ul>
<%=getHotQuestion(5)%>
</ul>
</div>

</div>

</div>