<!--#include file="top.asp"-->
<%
call showNotice()
pageTitle="确认付款 会员中心"

id=request.QueryString("id")
if not chkrequest(id) then alert "error","",1


set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 flag,buymethod,payMethod,bank from dingdan where id="&id&" and userid="&userid,conn,1,1
if rs.bof and rs.eof then
call showInfo("操作失败","javascript:history.back()","找不到此定单,可能该定单已经删除")
response.End()
end if
flag=cint(rs("flag"))
buymethod=cint(rs("buymethod"))
payMethod=rs("payMethod")
bank=rs("bank")
'flag的值必须为 已经下单 付款没有确认
if flag<>1 and flag<>33 then
call showInfo("操作失败","javascript:history.back()","当前定单状态不允许此操作")
response.End()
end if
'付款方式必须为普通支付
if buymethod<>0 then
	call showInfo("操作失败","javascript:history.back()","只有普通支付才能进行此操作")
end if
'支付方式必须为银行转账
if payMethod<>"0" then
	call showInfo("操作失败","javascript:history.back()","只有通过银行打款才能进行此操作，请点击在线支付，选择银行支付在使用此功能")
end if
bank=cint(bank)
call closers(rs)

if request.QueryString("act")="save" then

f_memo=checkstr(request.Form("f_memo"))

if not chkRange(f_memo,10,300) then
	showmsg "请输入付款的备注信息，比如转账时间，支付流水号，方便我们查询","",0
end if
sql="update dingdan set fkmemo='"&f_memo&"',createdon2='"&now()&"',flag=2 where id="&id&" and userid="&userid
call setDBvalue(sql)
call showInfo("操作成功","dingdan.asp?id="&id,"当前订单状态为 已经付款 ,我们会尽快查询并发货")

end if
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->
<script language="javascript" src="/js/validator.js"></script>
<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <a href="/member">订单管理</a> >> <a href="/member/dingdan.asp?id=<%=id%>">当前订单：<%=id%></a> >> <strong>确认付款</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>确认付款</h2>
</div>
</div>
<ul class="ColumnNews">
<form action="?act=save&id=<%=id%>" method="post">  
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">  
   <tr>
    <td height=30 align=right width="43%">选择支付的银行：&nbsp;</td>
    <td width="57%"><%
	sql="select top 1 bank,url from bank where id="&bank
	tarr=getdbvalue(sql,2)
	if tarr(0)<>0 then
		bankname=tarr(1)
		href=tarr(2)
	end if
	erase tarr
'	select case bank
'	case 1
'		bankname="工商银行"
'		href="https://mybank.icbc.com.cn/icbc/perbank/index.jsp"
'	case 2
'		bankname="农业银行"
'		href="https://easyabc.95599.cn/commbank/ebank/logonguest.jsp"
'	case 3
'		bankname="建设银行"
'		href="https://ibsbjstar.ccb.com.cn/app/V5/CN/STY1/login.jsp"
'	end select	
	%><span class="yellow"><%=bankname%></span>&nbsp;您还没有支付？点 <a href="<%=href%>" target="_blank">这里</a> 进入银行网站</td>
  </tr> 
    <tr>
    <td height=30 align=right width="43%">请输入支付说明：&nbsp;</td>
    <td width="57%"></td>
  </tr> 
     <tr>
    <td height=30 align=center colspan="2">
   <textarea name="f_memo" cols="50" rows="10" id="f_memo" style="color:#CCCCCC" onfocus="if(this.value==this.defaultValue){this.value='';} this.style.color='#000';" onblur="if(this.value==''){this.value=this.defaultValue;this.style.color='#cccccc';}">这里请填写相关的支付信息，如：支付时间等。以方便我们的工作人员查询</textarea></td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交" onclick="return chkMemo();">&nbsp; 
      <input type="button" name="Submit2" value="返回" onClick="javascript:history.back()">&nbsp;</label></td>
  </tr>
  <tr>
    <td height=30 align="center" colspan="2"><span class="yellow">友情提示：在您没有在这里<strong>提交确认</strong>付款前，订单的状态不会改变，将还是“已经下单”。所以这一步很重要。</span></td>
  </tr>
</table>
</form>  
</ul>
</div>

</div>

</div>
<!--content end-->
<script language="javascript">
function chkMemo()
{
	var m=document.getElementById("f_memo");
	if(m.value.length<10||m.value==m.defaultValue||m.value.length>300)
	{alert("请输入支付说明，方便我们查询(10-300字)");m.focus();return false;}
	return true;
}
</script>
  
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>