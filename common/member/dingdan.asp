<!--#include file="top.asp"-->
<!--#include virtual="/common/inc/xmlsub.asp"-->
<!--#include virtual="/common/inc/myxml.asp"-->
<%
call showNotice()
pageTitle="定单详细 会员中心"
const xmlFileName="/xml/city.xml"
id=request.QueryString("id")
if not chkrequest(id) then
	call showmsg("-找不到此定单","",0)
end if

set rs=server.CreateObject("adodb.recordset")
sql="select top 1 flag,shID,yunfei,jifen,total,createdon,createdon2,createdon3,createdon4,createdon5,createdon6,createdon7,buyMethod,tdmemo,jjmemo,fkmemo,[memo],memo,paymethod,bank,addressID,lxr,area,address,yb,tel,mobile,flagbak,number from dingdan where id="&id&" and userid="&userid

rs.open sql,conn,1,1
if rs.bof and rs.eof then
	showmsg "-找不到此订单,请确认","",0
end if

flag=cint(rs("flag"))
if request.QueryString("act")="sure" then
	if flag<>3 then
		call showInfo("发生错误","","只有在我们确认您的定单,并发货后,您才能执行此操作")	
	else
		set comm=server.CreateObject("adodb.command")
		with comm
		.activeconnection=conn
		.commandtype=4
		.commandtext="finishDingdan"
		.prepared=true
		.parameters.append .createparameter("@return",16,4)
		.parameters.append .createparameter("@id",20,1,8,id)
		.parameters.append .createparameter("@userid",20,1,8,userid)
		.execute()
		end with
		yesno=cint(comm(0))
		set comm=nothing
		if yesno=0 then		
			call showInfo("操作成功","dingdan.asp?id="&id,"恭喜您,确认收货成功")
		else
			call showInfo("操作失败","","错误ID号:"&yesno)
		end if
	end if
end if
'取消申请退单
flagbak=rs("flagbak")
if request.QueryString("act")="canceltuidan" then
	'当前状态不是申请退单 退单拒绝
	if flag<>5 and flag<>7 then
		alert "当前订单状态不允许此操作","",1
	end if
	if not chkNumber(flagbak) then
		alert "订单原始状态丢失，取消失败","",1
	end if
	flagbak=cint(flagbak)
	sql="update dingdan set flag="&flagbak&" where id="&id&" and userid="&userid
	call setdbvalue(sql)
	sql="update dingdanproducts set flag="&flagbak&",flagtime='"&now()&"' where id="&id&" and userid="&userid
	call setdbvalue(sql)			
	alert "取消订单退单成功，订单还原申请退单之前的状态","dingdan.asp?id="&id,1
end if


truename=rs("lxr")
address=getTextList(rs("area"))&"&nbsp;"&rs("address")
postcode=rs("yb")
tel=rs("tel")
mobile=rs("mobile")	


shID=cint(rs("shID"))
if shID<1 then
shname="无运费"
else
sql="select top 1 [name] from [class] where id="&rs("shID")
tarr=getdbvalue(sql,1)
if tarr(0)<>0 then shname=tarr(1) else shname=""
erase tarr
end if
num=rs("jifen")
total=rs("total")
createdon=rs("createdon")
buyMethod=cint(rs("buyMethod"))
paymethod=rs("paymethod")
yunfei=cint(rs("yunfei"))
memo=rs("memo")
bank=rs("bank")
number=rs("number")
tcontent=""
cstate=getdingdanstate(flag)

select case flag
case 0:	
	footer="<input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';"" value=""返回"" />" 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>关闭原因： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"" class=""yellow"">"&rs("tdmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>关闭时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon7")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
case 1:	
	footer="<input type=""button"" class=""button"" onclick=""location.href='surepay.asp?id="&id&"';return false;"" value=""确认付款"" />  <input type=""button"" class=""button"" onclick=""location.href='/products/pay.asp?id="&id&"';return false;"" value=""在线支付"" />  <input type=""button"" class=""button"" onclick=""location.href='closedingdan.asp?id="&id&"';return false;"" value=""关闭订单"" />  <input type=""button"" class=""button"" onclick=""location.href='modidingdan.asp?id="&id&"';return false;"" value=""修改订单"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
case 2:	
	footer="<input type=""button"" class=""button"" onclick=""location.href='canceldingdan.asp?id="&id&"';return false;"" value=""申请退单"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款说明： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("fkmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
case 3	
	footer="<input type=""button"" class=""button"" onclick=""if(confirm('警告,如果您还没有收到货或者货不齐,请点取消按钮并与我们联系!')){location.href='?act=sure&id="&id&"';}return false;"" value=""确认收货"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>发货时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon3")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>发货说明： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("jjmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
case 4	
	footer="<input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>发货时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon3")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>交易完成时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon4")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
case 5:	
	footer="<input type=""button"" class=""button"" onclick=""if(confirm('确定要取消申请退单吗？')){location.href='?act=canceltuidan&id="&id&"';}return false;"" value=""取消退单申请"" /> <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>退单原因： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("tdmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>申请退单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon5")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
case 6:	
	footer="<input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> "
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>退单原因： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("tdmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>申请退单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon5")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>管理员回复： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"" class=""yellow"">"&rs("jjmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>成功退单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon6")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
case 7:	
	footer="<input type=""button"" class=""button"" onclick=""if(confirm('确定要取消申请退单吗？')){location.href='?act=canceltuidan&id="&id&"';}return false;"" value=""修改重新申请"" /> <input type=""button"" class=""button"" onclick=""location.href='?act=canceltuidan&id="&id&"';return false;"" value=""取消退单申请"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> "
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>付款时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon2")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>退单原因： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("tdmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>申请退单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon5")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>管理员回复： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"" class=""yellow"">"&rs("jjmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 	
	tcontent=tcontent&"    <td height=30 align=right>退单拒绝时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon6")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 	
case 33	
	footer="<input type=""button"" class=""button"" onclick=""location.href='modifysurepay.asp?id="&id&"';return false;"" value=""重新确认付款"" />  <input type=""button"" class=""button"" onclick=""location.href='/products/pay.asp?id="&id&"';return false;"" value=""选择其它支付"" />  <input type=""button"" class=""button"" onclick=""location.href='closedingdan.asp?id="&id&"';return false"" value=""关闭订单"" />  <input type=""button"" class=""button"" onclick=""location.href='modidingdan.asp?id="&id&"';return false;"" value=""修改订单"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>未确认时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon3")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
		tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>管理员回复： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("jjmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
case 44
	footer="<input type=""button"" class=""button"" onclick=""location.href='canceldingdan.asp?id="&id&"';return false;"" value=""申请退单"" />  <input type=""button"" class=""button"" onclick=""location.href='"&webvirtual&"/member';return false;"" value=""返回"" /> " 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>下单时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
	tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>确认时间： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("createdon3")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
		tcontent=tcontent&"<tr>" & vbcrlf 
	tcontent=tcontent&"    <td height=30 align=right>管理员回复： &nbsp;</td>" & vbcrlf 
	tcontent=tcontent&"	<td align=""left"">"&rs("jjmemo")&"</td>" & vbcrlf 
	tcontent=tcontent&"  </tr>" & vbcrlf 
end select

rs.close
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->

<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>订单详细</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>订单详细信息</h2>
</div>
</div>
<ul class="ColumnNews">
   <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">  
  <tr>
    <td height=30 width="21%" align=right>定单ID号： &nbsp;</td>
	<td width="79%" align="left"><a href="shopcartDetail.asp?id=<%=id%>" target="_blank"><%=number%></a> <a href="shopcartDetail.asp?id=<%=id%>" target="_blank" class="red">查看产品详细</a></td>
  </tr> 
  <tr>
    <td height=30 align=right>&nbsp;&nbsp;收货人： &nbsp;</td>
	<td align="left"><%=truename%></td>
  </tr>
  <tr>
    <td height=30 align=right>收货地址： &nbsp;</td>
	<td align="left"><%=address%></td>
  </tr>  
   <tr>
    <td height=30 align=right>邮政编码： &nbsp;</td>
	<td align="left"><%=postcode%></td>
  </tr>  
  <tr>
    <td height=30 align=right>电话号码： &nbsp;</td>
	<td align="left"><%=tel%></td>
  </tr> 
  <tr>
    <td height=30 align=right>联系手机： &nbsp;</td>
	<td align="left"><%=mobile%></td>
  </tr>    
  <%if len(memo)>0 then%>
   <tr>
    <td height=30 align=right>附加说明： &nbsp;</td>
	<td align="left"><%=memo%></td>
  </tr>
  <%end if%>
  <tr>
    <td height=30 align=right>运输方式： &nbsp;</td>
	<td align="left"><%=shname%></td>
  </tr>  
  <tr>
    <td height=30 align=right>所需积分： &nbsp;</td>
    <td align="left"><%=num%>点
	</td>
  </tr>
  <tr>
    <td height=30 align=right>总共金额： &nbsp;</td>
    <td align="left">
		<span class="yellow"><%=getPrice((total+yunfei),2)%></span>元(其中包括运费<span class="yellow"><%=yunfei%></span>元)</td>
  </tr>  
   <tr>
    <td height=30 align=right>购买方式： &nbsp;</td>
	<td align="left"><%if buymethod=0 then response.Write("普通购买") else response.Write("积分购买")%></td>
  </tr>
  <%if flag>1 then
  paymethod=cint(paymethod)
  select case paymethod
  case 0
  	payname="银行转账"
  case 1
  	payname="预存款支付"
  case 2
  	payname="积分支付"
  case 3
  	payname="支付宝支付"
  end select
  if paymethod=0 then
  	bank=cint(bank)
	select case bank
	case 1
		payname=payname&"->工商银行支付"
	case 2
		payname=payname&"->农业银行支付"
	case 3
		payname=payname&"->建设银行支付"
	end select
  end if
  %>
    <tr>
    <td height=30 align=right>支付方式： &nbsp;</td>
    <td align="left">
		<%=payname%></td>
  </tr>
   <%
  end if
  %>
  <tr>
    <td height=30 align=right>当前状态： &nbsp;</td>
	<td align="left"><p style="line-height:180%"><%=cstate%></td>
  </tr>
  <%=tcontent%>
  <%tcontent=""%>
  <tr>
    <td height=30 align="center" colspan="2"><%=footer%></td>
  </tr>
<%

set rs=nothing
closeconn
%>
</table>
</ul>
</div>

</div>

</div>
<!--content end-->
 
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>