<!--#include file="top.asp"-->
<%
pageTitle="密码修改 会员中心"
pageKeyword="密码修改,会员中心"
pageDescription=""
if request.QueryString("act")="save" then
oldpw=checkstr(request.Form("f_oldpw"))
newpw1=checkstr(request.Form("f_newpw1"))
newpw2=checkstr(request.Form("f_newpw2"))

oldaw=checkstr(request.Form("f_oldaw"))
newqu=checkstr(request.Form("f_newqu"))
newaw1=checkstr(request.Form("f_newaw1"))
newaw2=checkstr(request.Form("f_newaw2"))
if len(oldpw)>0 then
	if len(oldpw)<4 or len(oldpw)>20 or len(newpw1)<4 or len(newpw1)>20 or len(newpw2)<4 or len(newpw2)>20 then
	call showInfo("发现错误","","请输入正确的密码,如果您不想修改原密码,请留空!")
	else
	oldpw=md5(oldpw)
		if oldpw<>pass then
		call showInfo("发现错误","","旧密码输入不对,如果您不想修改原密码,请留空!")
		elseif newpw1<>newpw2 then
		call showInfo("发现错误","","两次输入的新密码不一样,如果您不想修改原密码,请留空!")
		else
		newpw1=md5(newpw1)
		call update_record("[user]","password='"&newpw1&"'","id="&userid)	
		call showInfo("操作成功","/member/password.asp","操作完成,如果您更改了密码,请重新登录!")
		end if
	end if
end if

if len(oldaw)>0 then
	if len(oldaw)<4 or len(oldaw)>30 or len(newaw1)<4 or len(newaw1)>30 or len(newaw2)<4 or len(newaw2)>30 then
	call showInfo("发现错误","javascript:history.back()","请输入正确的密码答案,如果您不想修改原密码答案,请留空!")
	else
	oldaw=md5(oldaw)
	aw=conn.execute("select answer from [user] where id="&userid)(0)
		if oldaw<>aw then
		call showInfo("发现错误","javascript:history.back()","旧密码答案输入不对,如果您不想修改原密码答案,请留空!")
		elseif newaw1<>newaw2 then
		call showInfo("发现错误","javascript:history.back()","两次输入的新密码答案不一样,如果您不想修改原密码答案,请留空!")
		else
		newaw1=md5(newaw1)
		st="answer='"&newaw1&"'"
		if newqu<>"" and len(newqu)>2 then st=st&",quesion='"&newqu&"'"
		call update_record("[user]",st,"id="&userid)
		call showInfo("操作成功","/member/password.asp","操作完成,如果您更改了密码,请重新登录!")		
		end if
	end if
end if

end if
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->

<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>我的帐户</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>安全管理</h2>
</div>
</div>
<ul class="ColumnNews">
  
<form action="?act=save" method="post">  
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1">密码修改</font></td>
  </tr>
   <tr>
     <td height=30 align=right>原始密码： &nbsp;</td>
     <td><input type="password" maxlength="30" name="f_oldpw" id="f_oldpw" DataType="Require" msg="请输入您的昵称">
       6-20位数的旧密码,如不修改请留空</td>
   </tr>
   <tr>
     <td height=30 align=right>设置密码： &nbsp;</td>
     <td><input type="password" maxlength="30" name="f_newpw1" id="f_newpw1" >
       6-20位数的新密码,如不修改请留空</td>
   </tr> 
  
  <tr>
    <td width="21%" height=30 align=right>确认密码： &nbsp;</td>
	<td width="79%"><input type="password" maxlength="30" name="f_newpw2" id="f_newpw2" ></td>
  </tr> 
   <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1">安全问题</font></td>
  </tr>  
  <tr>
    <td height=30 align=right>原安全问题： &nbsp;</td>
    <td><%qu=conn.execute("select quesion from [user] where id="&userid)(0)%><%=qu%></td>
  </tr>
  <tr>
    <td height=30 align=right>原安全答案： &nbsp;</td>
    <td>
		<input type="text" maxlength="30" name="f_oldaw" id="f_oldaw" >
输入原安全答案</td>
  </tr>
    <tr>
    <td height=30 align=right>新安全问题： &nbsp;</td>
    <td>
		<input type="text" maxlength="30" name="f_newqu" id="f_newqu" >
至少输入四个字符新安全问题,如不修改请留空</td>
  </tr>
     <tr>
    <td height=30 align=right>新安全答案： &nbsp;</td>
    <td><label>
      <input type="text" maxlength="30" name="f_newaw1" id="f_newaw1" >
    输入四个字符新安全答案,如不修改请留空</label></td>
  </tr>
   <tr>
    <td height=30 align=right>确认安全答案： &nbsp;</td>
    <td><input type="text" maxlength="30" name="f_newaw2" id="f_newaw2" ></td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交">
    </label></td>
  </tr>
</table>
</form>  
<%
call closeconn()
%>  
</ul>
</div>

</div>

</div>
<!--content end-->
  
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>