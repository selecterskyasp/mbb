<!--#include file="top.asp"-->
<%
pageTitle="密码修改 会员中心"
pageKeyword="密码修改,会员中心"
pageDescription="密码修改"
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->
<link rel="stylesheet" type="text/css" media="screen" href="/jquery/css/cmxform.css" />
<style type="text/css">
.warning { color: red; }
</style>
<!--content begin-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <a href="/member">我的帐户</a> >> <strong>登陆密码修改</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<!--right-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>安全管理</h2>
</div>
</div>
<ul class="ColumnNews">
<div class="ColumnDiv">
	<h2>密码修改</h2>
	<div>
	<script src="/js/jquery.js" type="text/javascript"></script>
<script src="/jquery/lib/jquery.form.js" type="text/javascript"></script>
<script src="/jquery/jquery.validate.js" type="text/javascript"></script>
<script src="/jquery/js/cmxforms.js" type="text/javascript"></script>
<script language="javascript">
jQuery(function() {
		// show a simple loading indicator		
		jQuery().ajaxStart(function() {
			$("#result").html('<img src="/jquery/images/loading.gif" alt="loading..." />正在提交信息...');
		}).ajaxStop(function() {
			if($("#result").html().indexOf("成功")!=-1){$("#result").moveClass("error");alert('安全问题修改成功，请重新登陆');location.href='/reg';}
			//$("#result").html('等待提交...');
		}).ajaxError(function(a, b, e) {
			//alert(e.error);
			//throw e;
		});
		
		var v = jQuery("#form1").validate({
			submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					target: "#result"
				});
			}
		});
		
		jQuery("#reset").click(function() {
			v.resetForm();
		});
	});
	</script>
	<form action="passwordsave.asp" name="form1" id="form1" method="post">  
	  <table width="100%" cellpadding="0" cellspacing="0">
	    <tr>
          <td align="right" height="30">原始密码：  </td>
	      <td><input id="f_oldaw" type="password"  value="" name="f_oldaw" title="请输入您的原始密码(6-20为字符)" class="required" minlength="6" maxlength="20" />
	       </td>
	      </tr>
        <tr>
          <td align="right" height="30">设置密码：  </td>
          <td><input id="f_newqu" type="password"  value="" name="f_newqu" title="请输入您的新密码(6-20为字符)" class="required" minlength="6" maxlength="20" />
          </td>
        </tr>
        <tr>
          <td align="right" width="21%" height="30">确认密码：  </td>
          <td width="79%"><input id="f_newaw1" type="password"  value="" name="f_newaw1" title="第二次密码输入和第一次不匹配，请确认" equalTo="#f_newpw1" minlength="6" maxlength="20"/>
            </td>
        </tr>
		<tr>
          <td align="middle" colspan="2" height="30"><label>
            <input type="submit" value="提交" name="Submit2" />
          </label><span id="result" class="error"></span></td>
        </tr>
	    </table>
		</form>
	  </div>	
</div> 
  
<%
call closeconn()
%>  
</ul>
</div>

</div>

</div>
<!--content end-->
  
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>