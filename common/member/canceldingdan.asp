<!--#include file="top.asp"-->
<%
call showNotice()
pageTitle="申请退单 会员中心"

id=request.QueryString("id")
if not chkrequest(id) then
call showInfo("操作失败","javascript:history.back()","错误的参数传递")
response.End()
end if

set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 flag,tdmemo,flagbak from dingdan where id="&id&" and userid="&userid,conn,1,1
if rs.bof and rs.eof then
call showInfo("操作失败","javascript:history.back()","找不能此定单")
response.End()
end if
flag=cint(rs("flag"))
flagbak=rs("flagbak")
if not chkrequest(flagbak) then
flagbak=flag
else
flagbak=cint(flagbak)
end if
if flag=7 then
tdmemo=rs("tdmemo")
else
tdmemo=""
end if
call closers(rs)
'申请退单：只有当用户付款，或卖家发货期间才能操作
if flag<>2 and flag<>44 and flag<>7 then
	call showInfo("操作失败","javascript:history.back()","您当前的订单状态不允许进行此操作")
end if
if request.QueryString("act")="save" then
f_memo=checkstr(request.Form("f_memo"))

if not chkRange(f_memo,10,300) then
	call showInfo("操作失败","javascript:history.back()","请填写详细的退单原因,方便我们处理(10-300字)")
else
	sql="update dingdanproducts set flag=5 where id="&id&" and userid="&userid	
	call setdbvalue(sql)
	sql="update dingdan set flag=5,flagbak="&flagbak&",tdmemo='"&f_memo&"',createdon5='"&now()&"' where id="&id&" and userid="&userid	
	call setdbvalue(sql)	
	call showInfo("操作成功","/member/dingdan.asp?id="&id,"申请退单成功,请耐心等待我们的回复")
end if
end if

call closers(rs)
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->
<script language="javascript" src="/js/validator.js"></script>
<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>我的帐户</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<%
call closeconn()
%>
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>申请退单</h2>
</div>
</div>
<ul class="ColumnNews">
<form action="?act=save&id=<%=id%>" method="post" onSubmit="return Validator.Validate(this,2)">  
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#fffbe3"><font color=#666666 size="+1">退单原因</font></td>
  </tr>  
    <tr>
    <td height=30 align=right width="43%">请详细填写退单原因,方便我们审核： &nbsp;</td>
    <td width="57%"></td>
  </tr> 
     <tr>
    <td height=30 align=center colspan="2">
   <textarea name="f_memo" cols="50" rows="10" id="f_memo" dataType="Require" msg="请填写退单原因"><%=tdmemo%></textarea></td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交" onclick="return confirm('确实要申请退单吗？');">&nbsp; 
      <input type="button" name="Submit2" value="返回" onClick="javascript:history.back()" />
    </label></td>
  </tr>
</table>
</form>  
</ul>
</div>

</div>

</div>
<!--content end-->
  
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>