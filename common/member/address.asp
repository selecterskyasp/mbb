﻿<!--#include file="top.asp"-->
<!--#include file="../inc/xmlsub.asp"-->
<!--#include file="../inc/myxml.asp"-->
<%
call showNotice()
pageTitle="地址管理 会员中心"
const xmlFileName="/xml/city.xml"

set rs=server.CreateObject("adodb.recordset")
if request.QueryString("act")="del" then
id=request.QueryString("id")
	if not chkrequest(id) then
		alert "参数错误！","",1
	else
		id=clng(id)
		sql="delete [address] where id="&id&" and userid="&userid
		call setdbvalue(sql)
	end if
end if
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->
<link rel="stylesheet" type="text/css" media="screen" href="<%=webvirtual%>/jquery/css/cmxform.css" />
<script src="<%=webvirtual%>/jquery/lib/jquery.form.js" type="text/javascript"></script>
<script src="<%=webvirtual%>/jquery/jquery.validate.js" type="text/javascript"></script>
<script src="<%=webvirtual%>/jquery/js/cmxforms.js" type="text/javascript"></script>
<script type="text/javascript" src="<%=webvirtual%>/js/extend.js"></script>
<script type="text/javascript">
	jQuery(function() {
		// show a simple loading indicator		
		jQuery().ajaxStart(function() {
			$("#result").html('<img src="<%=webvirtual%>/jquery/images/loading.gif" alt="loading..." />正在提交信息...');
		}).ajaxStop(function() {
			if($("#result").html().indexOf("成功")!=-1){
					$("#result").removeClass("errorshow");location.href='address.asp';
				}else{
					if($("#result").html().length>1){
						$("#result").addClass("errorshow");
					}
			}
			//$("#result").html('等待提交...');
		}).ajaxError(function(a, b, e) {
			//alert(e.error);
			//throw e;
		});
		
		var v = jQuery("#form1").validate({
			submitHandler: function(form) {
				jQuery(form).ajaxSubmit({
					target: "#result"
				});
			}
		});
		
		jQuery("#reset").click(function() {
			v.resetForm();
		});
	});
function loadsortpath() {
		var selects1 = new linkSelect({
		el: 'area',
	url: '/xml/city.xml',
			root: 'list78',
			field: 'list',
			text: 'Text',
			value: 'ID'
			});		
		}
</script>
<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>收货地址管理</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>收货地址管理</h2>
</div>
</div>
<ul class="ColumnNews">
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">    
    <tr>
	  <th width="10%" height="26" align="center" bgcolor="#fefbeb">收货人</th>
      <th width="*%" height="26" align="center" bgcolor="#fefbeb">省份城市地区</th>
      <th width="20%" align="center" bgcolor="#fefbeb">街道</th>
      <th width="6%" align="center" bgcolor="#fefbeb">邮编</th>
      <th width="15%" align="center" bgcolor="#fefbeb">联系电话</th>
	  <th width="12%" align="center" bgcolor="#fefbeb">操作</th>
    </tr>
	<%	
	sql="select id,area,address,lxr,tel,mobile,yb from [address] where userid="&userid
sql=sql&" order by id desc"
	rs.open sql,conn,1,1
if not rs.eof then 
	page=request.querystring("page")
	if not chkrequest(page) then page=1 else page=cint(page)
	rs.pagesize = 10
	if (page-rs.recordcount) > 0 then 
		page=rs.recordcount
	end if 
	rs.absolutepage = page
	n=1
	do while not rs.eof and n<rs.pagesize	
	tel=rs("tel")
	if chknull(tel,1) then tel=rs("mobile")
	%>
    <tr>
	<td height="26" align="center"><%=rs("lxr")%></td>
      <td height="26" align="center"><%=getTextList(rs("area"))%></a></td>
      <td align="center"><%=rs("address")%></td>
      <td align="center"><%=rs("yb")%></td>
      <td align="center"><%=tel%></td>     
	  <td align="center"><a href="?act=edit&id=<%=rs("id")%>">修改</a> | <a href="?act=del&id=<%=rs("id")%>" onClick="return confirm('删除收货地址不能恢复,确定要继续吗?');">删除</a></td> 
    </tr>
	<%
		n=n+1
		rs.movenext
	loop
else
%>
 <tr>      
      <td height="26" colspan="5" align="center">暂无相关信息</td>      
   </tr>
<%
end if 
%>
    
    <tr>
      <td height="38" colspan="8" align="right" class="membertr">
	  <%=getshowpage(rs.PageCount,rs.pagesize,page,rs.RecordCount,8) %>	  </td>
      </tr>
  </table>
  <% call closers(rs)%>
  <form action="addresssave.asp" id="form1" name="form1" method="post">
  <table width="100%" border="0" cellspacing="2" cellpadding="0" class="list" name="table" id="table">
  <%
  id=request.QueryString("id")
  lxr="":area="":address="":yb="":tel="":mobile="":arealist="":txt=""
  act="add"
  if chkrequest(id) and request.QueryString("act")="edit" then
  	sql="select lxr,area,address,yb,tel,mobile from address where id="&id&" and userid="&userid
	tarr=getdbvalue(sql,6)
	if tarr(0)<>0 then
		lxr=tarr(1)
		area=tarr(2)
		address=tarr(3)
		yb=tarr(4)
		tel=tarr(5)
		mobile=tarr(6)
		arealist=getTextlist(area)
		txt="<span class=""yellow"">您之前的省市是："&arealist&"(如果不修改请不要选择)</span>"
	end if
	act="edit"
  end if
  %>
    <tr>
      <th height="26" colspan="2" align="left" bgcolor="#fefbeb">新增/修改收货人信息</th>
      </tr>    
	  <input type="hidden" name="act" id="act" value="<%=act%>" />
	  <input type="hidden" name="id" id="id" value="<%=id%>" />
    <tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">收 货 人：</td>
      <td width="50%"><input type="text" name="lxr" id="lxr" class="required" maxlength="20" minlength="2" title="请输入收货人姓名" value="<%=lxr%>"/></td>     
    </tr>	
	 <tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">所在省市：</td>
      <td width="50%"><div id="area"></div><%=txt%>
	  <input type="hidden" value="<%=area%>" name="areaold" id="areaold" />
	  </td>     
    </tr>
	<script language="javascript">
	loadsortpath();
	</script>
	<tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">街道地址：</td>
      <td width="50%"><input name="address" type="text" class="required" id="address" title="请输入街道地址(2-100个字符，不要输入省份地区)" size="50" maxlength="100" minlength="2" value="<%=address%>" /></td>     
    </tr>
	<tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">邮政编码：</td>
      <td width="50%"><input name="yb" type="text" class="required" id="yb" title="请输入邮政编码" size="20" maxlength="10" minlength="2" value="<%=yb%>" /></td>     
    </tr>
	<tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">电话号码：</td>
      <td width="50%"><input type="text" name="tel" id="tel" class="required:false" maxlength="20" minlength="6" title="请输入电话号码(6-20位字符)" value="<%=tel%>"/></td>     
    </tr>	
	<tr>
	  <td width="20%" height="26" align="right" bgcolor="#fefbeb">手机号码：</td>
      <td width="50%"><input type="text" name="mobile" id="mobile" class="required:false" maxlength="20" minlength="6" title="请输入手机号码(6-20位字符)" value="<%=mobile%>"/></td>     
    </tr>	
	<tr>
      <th height="26" colspan="2" align="center" bgcolor="#fefbeb"><label>
        <input type="submit" name="Submit" value="提交" /> 
&nbsp;</label>
        <label>
        <input type="reset" name="Submit2" value="重置" />
        </label></th>
	  </tr>
	  <tr>
      <td height="26" colspan="2" align="center" bgcolor="#ffffff"><span id="result"></span></td>
	  </tr>
  </table>
  </form>
</ul>
</div>

</div>

</div>
<!--content end-->
 <%
 call closeconn()
 %>
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>