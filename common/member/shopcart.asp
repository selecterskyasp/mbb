<!--#include file="top.asp"-->
<%
call showNotice()
pageTitle="购物车 会员中心"
pageKeyword="购物车,会员中心"
pageDescription=""
fpage=checkstr(request.QueryString("fpage"))
if fpage="" then fpage="javascript:history.back();"

if request.QueryString("act")="del" then
	id=request.QueryString("id")
	if not chkrequest(id) then
		call showInfo("操作失败","","错误的参数传递")
	else
		call del_record("shopcart","pid="&id&" and userid="&userid)
		call showInfo("操作完成","shopcart.asp","删除购物车产品成功")
	end if
end if
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->

<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>我的购物车</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>购物车管理</h2>
</div>
</div>
<ul class="ColumnNews">
  
<table width="100%" border="0" cellspacing="0" cellpadding="0">   
    <tr>    
      <th width="*%" align="center" bgcolor="#f7f7f7" class="membertr">产品名称</th>
      <th width="11%" align="center" bgcolor="#f7f7f7" class="membertr">产品价格</th>
      <th width="10%" align="center" bgcolor="#f7f7f7" class="membertr">购买数量</th>
	  <th width="20%" align="center" bgcolor="#f7f7f7" class="membertr">购买颜色</th>
	 <!-- <th width="10%" align="center" bgcolor="#f7f7f7" class="membertr">购买尺寸</th>-->
      <th width="13%" align="center" bgcolor="#f7f7f7" class="membertr">小计金额</th>	       
	  <th width="10%" align="center" bgcolor="#f7f7f7" class="membertr">操作</th>       
    </tr>
	<%
	dim money,ttmoney,money1,ttmoney1
	ttmoney=0:ttmoney1=0
	dim pid,pnum,pcolor,psize,price,price1
	dim colorList,sizeList
	colorList="":sizeList=""
	tcontent=""
	
	sql="select id,pid,pnum,pcolor,psize from shopcart where userid="&userid
	
	tarr=getDBvalueList(sql)	
	if clng(tarr(0,0))<>0 then
		for i=0 to ubound(tarr,2)
			pid=tarr(1,i)
			pnum=clng(tarr(2,i))
			pcolor=tarr(3,i)
			psize=tarr(4,i)
			'颜色
			sql="select id,[name] from class where id in("&pcolor&")"
			
			tarr2=getDBvalueList(sql)
			if clng(tarr2(0,0))<>0 then
			for j=0 to ubound(tarr2,2)
				colorList=colorList&tarr2(1,j)&"&nbsp;"
			next
			end if
			erase tarr2
			'尺寸
			sql="select id,[name] from class where id in("&psize&")"
		
			tarr2=getDBvalueList(sql)
			if clng(tarr2(0,0))<>0 then
			for j=0 to ubound(tarr2,2)
				sizeList=sizeList&tarr2(1,j)&"&nbsp;"
			next
			end if
			erase tarr2
			'产品信息
			sql="select top 1 [name],price1 from products where flag=1 and id="&pid
			
			tarr2=getDBvalue(sql,2)
			if tarr2(0)=0 then
				sql="delete shopcart where pid="&pid
				call setDBvalue(sql)
			else
				pname=tarr2(1):price=getproductsprice(pid,flag):price1=cdbl(tarr2(2))			
				money=pnum*price
				money1=pnum*price1
				ttmoney=ttmoney+money
				ttmoney1=ttmoney1+money1
				tcontent=tcontent&"<tr>      " & vbcrlf 
				tcontent=tcontent&"      <td align=""center"">"&pname&"</td>" & vbcrlf 
				tcontent=tcontent&"      <td align=""center"">"&getPrice(price,2)&"</td>" & vbcrlf 
				tcontent=tcontent&"      <td align=""center"">"&pnum&"</td>" & vbcrlf 
				tcontent=tcontent&"      <td align=""center"">"&colorList&"</td>" & vbcrlf 
				tcontent=tcontent&"      <!--<td align=""center"">"&sizeList&"</td>-->" & vbcrlf 
				tcontent=tcontent&"	  <td align=""center"">"&getPrice(money,2)&"</td>" & vbcrlf 			
				tcontent=tcontent&"      <td align=""center""><a href=""?act=del&id="&pid&""">删除</a></td>" & vbcrlf 
				tcontent=tcontent&"    </tr>" & vbcrlf 
			end if
			erase tarr2

		next
	else
		tcontent=tcontent&"<tr><td align=""center"" colspan=""10"">暂无相关信息</td></tr>" & vbcrlf 
	end if
	erase tarr
	response.Write(tcontent)
	tcontent=""
%>
    
    <tr>
      <td height="38" colspan="10" align="right" class="membertr">
	  当前购物车中的商品总价是<span class="yellow"><%=ttmoney%></span>元，市场价格是<span class="yellow"><%=ttmoney1%></span>元，共节约<span class="yellow"><%=(ttmoney1-ttmoney)%></span>元
	 </td>
      </tr>
	  <tr>
      <td height="38" colspan="8" align="center" class="membertr">
	  <a href="/products/confirm_order.asp">马上去收银台</a>
	  </td>
      </tr>
  </table>
  
</ul>
</div>

</div>

</div>
<!--content end-->
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>