<!--#include file="top.asp"-->
<%
pageTitle="基本资料 会员中心"
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<script type="text/javascript" src="<%=webvirtual%>/js/jquery.js"></script>
<script type="text/javascript" src="<%=webvirtual%>/js/extend.js"></script>
<script language="JavaScript" type="text/JavaScript">
$(document).ready(function() {
		var selects1 = new linkSelect({
		el: 'area',
	url: '/xml/city.xml',
			root: 'list78',
			field: 'list',
			text: 'Text',
			value: 'ID'
			});
			$(".ColumnDiv h2").eq(2).addClass("active");
	$(".ColumnDiv div").eq(2).show();

	$(".ColumnDiv h2").click(function(){
		$(this).next("div").slideToggle("slow")
		.siblings("p:visible").slideUp("slow");
		$(this).toggleClass("active");
		$(this).siblings("h2").removeClass("active");
	});
		});	
</script>
<!-- header end -->
<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>我的帐户</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<!--right-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>基本资料</h2>
</div>
</div>
<ul class="ColumnNews">
<div class="ColumnDiv">
	<h2>会员信息</h2>
	<div>
<%
set rs=server.CreateObject("adodb.recordset")
if request.QueryString("act")="save" then
	f_ni=checkstr(request.Form("f_ni"))
	f_truename=checkstr(request.Form("f_truename"))
	sex=cint(request.Form("sex"))
	f_addr=checkstr(request.Form("f_addr"))
	f_qq=checkstr(request.Form("f_qq"))
	f_yb=checkstr(request.Form("f_yb"))
	area=replace(request.Form("area")," ","")
	areaold=replace(request.Form("areaold")," ","")
	if not chkPara(area) then area=areaold
	f_phone=checkstr(request.Form("f_phone"))
	f_mobile=checkstr(request.Form("f_mobile"))
	f_memo=checkstr(request.Form("f_memo"))
	if  not chkRange(f_truename,2,20) or (sex<>"0" and sex<>"1") or not chkRange(f_addr,4,100) or not chkpara(area) then
		call showInfo("发现错误","javascript:history.back()","带*号的信息为必填,并按提示要求填写")
	else
		call update_record("[user]","nicheng='"&f_ni&"',truename='"&f_truename&"',postcode='"&f_yb&"',sex="&sex&",address='"&f_addr&"',qqmsn='"&f_qq&"',tel='"&f_phone&"',mobile='"&f_mobile&"',content='"&f_memo&"',area='"&area&"'","id="&userid)
		call showInfo("修改成功","basic.asp","恭喜您,修改基本资料成功")
	end if
end if
rs.open "select top 1 * from [user] where id="&userid,conn,1,1
ttype=cint(rs("type"))

ctype=getUserFlagName(ttype)(1)
if ttype<>1 then
	ctype="<span class=""yellow"">"&ctype&"</span>"
	ctype=ctype&" <font color=red>开始时间:"&rs("sdate")&" 结束时间:"&rs("edate")&" 还有"&datediff("d",rs("sdate"),rs("edate"))&"天结束"
end if
if rs("flag") then	cstate="<font color=green>已审核</font>" else cstate="<font color=red>未审核</font>"
if not rs("isjihou") then cstate="<font color=red>未激活</font>"
area=rs("area")
%> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">
   <tr>
    <td width="21%" height="26" align="right" bgcolor="#ffffff">当前状态：</td>
	<td width="79%" align="left" bgcolor="#ffffff"><%=cstate%>&nbsp;<%if not rs("isjihou") then response.Write("<a href=""jihouag.asp"">重新发送激活邮件</a>")%></td>
  </tr> 
  <tr>
    <td height="26" align="right" bgcolor="#ffffff">您的等级：</td>
	<td align="left" bgcolor="#ffffff"><%=ctype%></td>
  </tr>
  <tr>
    <td height="26" align="right" bgcolor="#ffffff">注册邮箱：</td>
	<td align="left" bgcolor="#ffffff"><%=user%>&nbsp;<a href="editmail.asp">修改邮箱</a></td>
  </tr>
  <tr>
   <td height="26" align="right" bgcolor="#ffffff">可用积分：</td>
	<td align="left" bgcolor="#ffffff"><%=rs("jifen")%></td>
  </tr>
   <tr>
   <td height="26" align="right" bgcolor="#ffffff">预 存 款：</td>
	<td align="left" bgcolor="#ffffff"><%=getprice(rs("money"),2)%></td>
  </tr>
  <tr>
    <td height="26" align="right" bgcolor="#ffffff">登陆的IP：</td>
	<td align="left" bgcolor="#ffffff"><%=rs("loginip")%></td>
  </tr>
  <tr>
     <td height="26" align="right" bgcolor="#ffffff">登录次数：</td>
	<td align="left" bgcolor="#ffffff"><%=rs("loginnum")%></td>
  </tr> 
 </table>
	</div>
	<h2>联系信息</h2>
	<div>
	<form action="?act=save" method="post" onSubmit="return Validator.Validate(this,2)" name="form1"> 
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">

  <tr>
    <td width="21%" height="26" align="right" bgcolor="#ffffff">您的昵称：</td>
    <td width="79%" align="left" bgcolor="#ffffff"><input type="text" maxlength="30" name="f_ni" id="f_ni" value="<%=rs("nicheng")%>">		</td>
  </tr>
  <tr>
    <td height="26" align="right" bgcolor="#ffffff">真实姓名：</td>
   <td align="left" bgcolor="#ffffff">
		<input type="text" maxlength="30" name="f_truename" id="f_truename" DataType="Require" msg="请输入您的真实姓名" value="<%=rs("truename")%>">至少输入两个字符的真实姓名<font color="#FF0000">*</font></td>
  </tr>
     <tr>
    <td height="26" align="right" bgcolor="#ffffff">性&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;别：</td>
    <td align="left" bgcolor="#ffffff"><label>
      <input type="radio" name="sex" value="0" <%if rs("sex")=false then response.Write("checked")%>>
      先生
      <input type="radio" name="sex" value="1" <%if rs("sex")=true then response.Write("checked")%>>
      女士
    </label></td>
  </tr>
   <tr>
    <td height="26" align="right" bgcolor="#ffffff">省份城市：</td>
	<td align="left" bgcolor="#ffffff">
 <div id="area"></div>
 <%
 if chkPara(area) then
  sql="select id,name from class where id in("&area&") order by charindex(ltrim(id),'"&area&"')"
  tarr=getDBvalueList(sql)
  tst=""
  if clng(tarr(0,0))<>0 then
  	tst="<font color=""#FF0000"">当前您选择的地址是:"
  	for i=0 to ubound(tarr,2)
		tst=tst&tarr(1,i)&"&nbsp;"
	next
  end if
  erase tarr
  response.Write(tst&"(注意:如果您保持先前的地址,请留空)</font>")
 end if
 %>
 <input type="hidden" value="<%=area%>" name="areaold" />		<font color="#FF0000">*</font></td>
  </tr>
  <tr>
    <td height="26" align="right" bgcolor="#ffffff">街道地址：</td>
    <td align="left" bgcolor="#ffffff"><input name="f_addr" type="text" id="f_addr" value="<%=rs("address")%>" size="50" maxlength="100" DataType="Require" msg="请输入您的街道地址">
      <font color="#FF0000">*</font><br /> 请输入您的街道地址(注：这里不需要输入您所在的省份城市和地区)		</td>
  </tr>
    <tr>
    <td height="26" align="right" bgcolor="#ffffff">邮&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;编：</td>
    <td align="left" bgcolor="#ffffff"><input type="text" maxlength="30" name="f_yb" id="f_yb" value="<%=rs("postcode")%>">		</td>
  </tr>
    <tr>
    <td height="26" align="right" bgcolor="#ffffff">联系QQ/msn：</td>
    <td align="left" bgcolor="#ffffff"><input type="text" maxlength="30" name="f_qq" id="f_qq" value="<%=rs("QQmsn")%>">		</td>
  </tr>
   <tr>
    <td height="26" align="right" bgcolor="#ffffff">联系电话：</td>
    <td align="left" bgcolor="#ffffff"><input type="text" maxlength="30" name="f_phone" id="f_phone" value="<%=rs("tel")%>">		</td>
  </tr>
   <tr>
    <td height="26" align="right" bgcolor="#ffffff">联系手机：</td>
    <td align="left" bgcolor="#ffffff"><input type="text" maxlength="30" name="f_mobile" id="f_mobile" value="<%=rs("mobile")%>" >		</td>
  </tr>
     <tr>
    <td height="26" align="right" bgcolor="#ffffff">备&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;注：</td>
    <td align="left" bgcolor="#ffffff"><textarea name="f_memo" rows="5" id="f_memo"><%=rs("content")%></textarea>		</td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交">
    </label></td>
  </tr>
<%
call closers(rs)
call closeconn()
%>
</table>
</form>  
</div>
</div> 
</ul>
</div>

</div>

</div>
<!--content end-->

<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>