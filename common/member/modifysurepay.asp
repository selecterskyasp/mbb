<!--#include file="top.asp"-->
<%
call showNotice()
pageTitle="确认付款 会员中心"

id=request.QueryString("id")
if not chkrequest(id) then alert "error","",1

set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 flag,buymethod,payMethod,bank,fkmemo from dingdan where id="&id&" and userid="&userid,conn,1,1
if rs.bof and rs.eof then
call showInfo("操作失败","javascript:history.back()","找不到此定单,可能该定单已经删除")
response.End()
end if
flag=cint(rs("flag"))
buymethod=cint(rs("buymethod"))
payMethod=rs("payMethod")
bank=rs("bank")
fkmemo=rs("fkmemo")
'flag的值必须为 商家未收到到付款
if flag<>33 then
call showInfo("操作失败","javascript:history.back()","当前定单状态不允许此操作")
response.End()
end if
'付款方式必须为普通支付
if buymethod<>0 then
	call showInfo("操作失败","javascript:history.back()","只有普通支付才能进行此操作")
end if
'支付方式必须为银行转账
if payMethod<>"0" then
	call showInfo("操作失败","javascript:history.back()","只有通过银行打款才能进行此操作")
end if
bank=cint(bank)
call closers(rs)

if request.QueryString("act")="save" then

f_memo=checkstr(request.Form("f_memo"))
if not chkRange(f_memo,10,300) then
	showmsg "请输入付款的备注信息，比如转账时间，支付流水号，方便我们查询","",0
end if
sql="update dingdan set fkmemo='"&f_memo&"',flag=2 where id="&id&" and userid="&userid
call setDBvalue(sql)
call showInfo("操作成功","dingdan.asp?id="&id,"当前订单状态为 已经付款 ,我们会尽快查询并发货")

end if
%>

<%=head("会员中心","会员中心",pageTitle,11)%>
<!-- header end -->
<script language="javascript" src="/js/validator.js"></script>
<!-- content begin -->
<!--guidance-->
<div class="guidance">
您现在的位置：<a href="/">首页</a> >> <strong>我的帐户</strong>
</div>

<div id="content">
<!--#include file="left.asp"-->
<div class="PagesRight">
<div class="column2">
<div class="AboutTitle">
<div class="title">
<h2>确认付款</h2>
</div>
</div>
<ul class="ColumnNews">
<form action="?act=save&id=<%=id%>" method="post">  
 <table width="100%" border="0" cellspacing="0" cellpadding="0" class="list" name="table" id="table">
  <tr>
    <td height=30 align="left" colspan="2" bgcolor="#f7f7f7" class="membertr" ><font color=#666666 size="+1">付款备注</font></td>
  </tr> 
   <tr>
    <td height=30 align=right width="38%">您选择的转账银行：</td>
    <td width="62%"><%
	select case bank
	case 1
		bankname="工商银行"
	case 2
		bankname="农业银行"
	case 3
		bankname="建设银行"
	end select
	response.Write(bankname)
	%></td>
  </tr> 
    <tr>
    <td height=30 align=right width="38%">请填写付款方式等信息,方便我们查询：&nbsp;</td>
    <td width="62%"></td>
  </tr> 
     <tr>
    <td height=30 align=center colspan="2">
   <textarea name="f_memo" cols="50" rows="10" id="f_memo"><%=fkmemo%></textarea></td>
  </tr>
   
  <tr>
    <td height=30 align="center" colspan="2"><label>
      <input type="submit" name="Submit" value="提交" onclick="return confirm('确认提交吗？')">&nbsp; 
      <input type="button" name="Submit2" value="返回" onClick="javascript:history.back()">&nbsp;</label></td>
  </tr>
</table>
</form>  
<script language="javascript">
function setHref(id)
{
var obj=document.getElementById(id)
obj.checked=true;
document.getElementById('zh').innerText=obj.title;
}
</script>
</ul>
</div>

</div>

</div>
<!--content end-->
  
<!--footer begin-->
<%=foot()%>
<!-- footer end -->

</body>
</html>