﻿<%
'==================================================
'函数名：GetHttpPage
'作  用：获取网页源码
'参  数：HttpUrl ------网页地址
'==================================================
Function GetHttpPage(byVal HttpUrl)
on error resume next
   If IsNull(HttpUrl)=True Or Len(HttpUrl)<18 Or HttpUrl="$False$" Then
      GetHttpPage="$False$"
      Exit Function
   End If
   Dim Http
   Set Http=server.createobject("MSXML2.XMLHTTP")
   Http.open "GET",HttpUrl,False
   Http.Send()
   if err then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   end if
   If Http.Readystate<>4 then
      Set Http=Nothing 
      GetHttpPage="$False$"
      Exit function
   End if
   GetHTTPPage=bytesToBSTR(Http.responseBody,"utf-8")
   Set Http=Nothing
   If Err.number<>0 then
      Err.Clear
   End If
End Function

'==================================================
'函数名：BytesToBstr
'作  用：将获取的源码转换为中文
'参  数：Body ------要转换的变量
'参  数：Cset ------要转换的类型
'==================================================
Function BytesToBstr(byVal Body,byVal Cset)
   Dim Objstream
   Set Objstream = Server.CreateObject("adodb.stream")
   objstream.Type = 1
   objstream.Mode =3
   objstream.Open
   objstream.Write body
   objstream.Position = 0
   objstream.Type = 2
   objstream.Charset = Cset
   BytesToBstr = objstream.ReadText 
   objstream.Close
   set objstream = nothing
End Function


'==================================================
'函数名：UrlEncoding
'作  用：转换编码
'==================================================
Function UrlEncoding(byVal DataStr)
    Dim StrReturn,Si,ThisChr,InnerCode,Hight8,Low8
    StrReturn = ""
    For Si = 1 To Len(DataStr)
        ThisChr = Mid(DataStr,Si,1)
        If Abs(Asc(ThisChr)) < &HFF Then
            StrReturn = StrReturn & ThisChr
        Else
            InnerCode = Asc(ThisChr)
            If InnerCode < 0 Then
               InnerCode = InnerCode + &H10000
            End If
            Hight8 = (InnerCode  And &HFF00)\ &HFF
            Low8 = InnerCode And &HFF
            StrReturn = StrReturn & "%" & Hex(Hight8) &  "%" & Hex(Low8)
        End If
    Next
    UrlEncoding = StrReturn
End Function

'==================================================
'函数名：GetBody
'作  用：截取字符串
'参  数：ConStr ------将要截取的字符串
'参  数：StartPos ------开始查找位置
'参  数：StartStr ------开始字符串
'参  数：OverStr ------结束字符串
'参  数：IncluL ------是否包含StartStr
'参  数：IncluR ------是否包含OverStr

'==================================================
Function GetBody(byVal ConStr,byVal StartPos_,byVal StartStr,byVal OverStr,byVal IncluL,byVal IncluR)   
   If ConStr="$False$" or ConStr="" or IsNull(ConStr) Or StartStr="" or IsNull(StartStr) Or OverStr="" or IsNull(OverStr) or IsNull(StartPos_) or not isnumeric(StartPos_) or StartPos_<1 Then   	  
      GetBody="$False$|-1"
      Exit Function
   End If
   
   Dim ConStrTemp
   Dim Start,Over
   Dim tempStr
   ConStrTemp=Lcase(ConStr)
   StartStr=Lcase(StartStr)
   OverStr=Lcase(OverStr)
   Start = InStrB(StartPos_, ConStrTemp, StartStr, vbBinaryCompare)
   If Start<=0 then
      GetBody="$False$|-1"
      Exit Function
   Else
      If IncluL=False Then
         Start=Start+LenB(StartStr)
      End If
   End If
   Over=InStrB(Start,ConStrTemp,OverStr,vbBinaryCompare)
   If Over<=0 Or Over<=Start then
      GetBody="$False$|-1"
      Exit Function
   Else
      If IncluR=True Then
         Over=Over+LenB(OverStr)
      End If
   End If
   tempStr=MidB(ConStr,Start,Over-Start)
   tempStr=replace(tempStr,"|","")
   tempStr=tempStr+"|"+cstr(Over+1)
   GetBody=tempStr
End Function



'判断文件是否存在
Function isfileExists(byVal FileName) 
isfileExists = false
dim delobj
set delobj=server.CreateObject("Scripting.FileSystemObject")
filename=server.MapPath(filename)	
if delobj.FileExists(filename) then
	isfileExists=true
else
	isfileExists=false
end if
set delobj=nothing
End Function 

'==================================================
'过程名：SaveRemoteFile
'作  用：保存远程的图片到本地
'参  数：LocalFileName ------ 本地文件名
'参  数：RemoteFileUrl ------ 远程文件URL
'==================================================
Function SaveRemoteFile(byVal LocalFileName,byVal RemoteFileUrl)
on error resume next
	err.clear
    SaveRemoteFile=false
	dim Ads,Retrieval,GetRemoteData		
	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
	With Retrieval	
		.Open "Get", RemoteFileUrl, False, "", ""	
		if err.number=-2147467259 then exit function	
		.Send		
        If .Readystate<>4 or .status<>200 then  Exit Function        
		GetRemoteData = .ResponseBody
	End With
	Set Retrieval = Nothing
	Set Ads = Server.CreateObject("Adodb.Stream")
	With Ads
		.Type = 1
		.Open
		.Write GetRemoteData
		.SaveToFile server.MapPath(LocalFileName),2		
		if err.number=-2147467259  then Exit Function		
		.Cancel()
		.Close()
	End With
	Set Ads=nothing	
	SaveRemoteFile=true
end Function

'获取扩展名
function GetExt(byVal FileName)
	GetExt = "."&right(FileName,len(FileName)-instrrev(FileName,"."))	
	FileName=""	
end function

'在指定的位置创建文件夹
function Createfol(byVal folpath)
	dim fso
	set fso=server.CreateObject("Scripting.FileSystemObject")	
	if right(folpath,1)<>"/" then folpath=folpath&"/"
	folpath=server.MapPath(folpath)	
	if not fso.FolderExists(folpath) then		
		fso.CreateFolder(folpath)
	end if	
	if err.number=0 then
		Createfol=fso.GetBaseName(folpath)
	else
		Createfol=false
	end if
	set fso=nothing
end function

'生成一个文件名 注：无扩展名
function makeFilename()
dim tmpfilename
dim rndnum
randomize timer
rndnum=int(rnd*100)
tmpfilename=replace(replace(replace(replace(now(),":",""),"/","")," ","_"),"-","")
makeFilename=tmpfilename&rndnum
end function


'会员发布的各种信息过滤
Function Replace_Text(byVal fString)
If Not IsNull(fString) and not isempty(fString) and fstring<>"" Then
'fString = trim(fString)
dim regEx
set regEx=new regExp
regEx.ignoreCase=true
regEx.Global=True
'<script
regEx.pattern="<script.+?script>"
fString=regEx.replace(fString,"")
'过滤asp代码
regEx.pattern="<\x25.+?\x25>"
fString=regEx.replace(fString,"")
'过滤php代码
regEx.pattern="<\x3f.+?\x3f>"
fString=regEx.replace(fString,"")

'<ifream></ifream>
regEx.pattern="<ifream.+?ifream>"
fString=regEx.replace(fString,"")
'<frameset></frameset>
regEx.pattern="<frameset.+?frameset>"
fString=regEx.replace(fString,"")
set regEx=nothing
fString = replace(fString, chr(62), "&gt;") '>
fString = replace(fString, "<", "&lt;")
fString = replace(fString, chr(60), "&lt;") '<
fString = replace(fString, """", "&quot;")
fString = Replace(fString, CHR(34), "&quot;") '双引号过滤
fString = Replace(fString, "'", "''")	'单引号过滤
End If
Replace_Text = fString
fString=""
End Function


'完全过滤html
Function getInnerText(byVal strHTML)
strHTML=trim(strHTML)
  if strHTML="" or isnull(strHTML) or isempty(strHtml) then
    getInnerText="":exit function
  end if
    Dim objRegExp
    Set objRegExp = New Regexp    
    objRegExp.IgnoreCase = True
    objRegExp.Global = True
    '取闭合的<>
    objRegExp.Pattern = "<.+?>"
    '进行替换匹配
	strHTML=objRegExp.replace(strHTML,"")
	objRegExp.Pattern = "&lt;.+?&gt;"
	strHTML=objRegExp.replace(strHTML,"")
   	Set objRegExp = Nothing
	strHTML=replace(strHTML,"&nbsp;","")
	strHTML=replace(strHTML," ","")	
	strHTML = Replace(strHTML, CHR(32), "")		'&nbsp;
	strHTML = Replace(strHTML, CHR(9), "")			'&nbsp;
	strHTML = Replace(strHTML, CHR(10), "")			'&nbsp;
	strHTML = Replace(strHTML, CHR(13), "")	
    getInnerText=strHTML
	strHTML=""
    Set objRegExp = Nothing
End Function

'检测传递的参数是否为数字型
Function Chkrequest(byVal Para)
dim tempNum
Chkrequest=False
Para=trim(Para)
If not IsNull(Para) and not Para="" and IsNumeric(Para) Then
	tempNum=clng(Para)
	if tempNum>0 then
  	 Chkrequest=True
   	end if
End If
End Function

'检查字符串是满足长度
Function ChkNull(byVal tempPara,byVal strlen)
tempPara=trim(tempPara)
if isnull(tempPara) or tempPara="" or len(tempPara)<strlen then
ChkNull=True
else
ChkNull=False
end if
tempPara=""
end Function

'显示错误提示页面
'msg 错误信息 ref 返回页面 如果ref为空,即默认为history.back()
function showErrmsg(byVal msg,byVal ref)	
	if chkNull(ref,1) and ref<>"/" then 
	ref="javascript:history.back();"
	else
	ref="location.href='"&ref&"';"	
	end if
	response.Write("<script>alert('您的操作失败,可以引起的原因:\n\n·"&msg&"');"&ref&"</script>")	
	response.End()
end function



'---------------------数据处理---------------------------
'****************************************************
'函数名：getDbValue
'作  用：取一条记录
'参  数：Sql           传入参数 Sql语句
'       ArrayCount    传入参数 需要的字段的个数
'返回值:getDbValue      输出参数 为一个数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDbValue(byVal Sql,ByVal ArrayCount)
	ReDim NewArray(ArrayCount)
	Dim Recordcounts,Rs,i,Comm
	Set Comm = Server.CreateObject("ADODB.Command")
	With Comm
		Comm.ActiveConnection = Conn
		Comm.CommandType = 4
		Comm.CommandText = "ColumnGet"
		Comm.Prepared  = true
		Comm.Parameters.Append .CreateParameter("Return",2,4)
		Comm.Parameters.Append .CreateParameter("@Sql",200,1,1000,Sql)
		Set Rs = .Execute	
	End With
	Rs.Close
	Recordcounts=Comm(0)
	NewArray(0)=Recordcounts
	If Recordcounts=0 Then getDbValue=NewArray:Exit Function
	Set Comm=Nothing
	Rs.Open
	For i = 0 to cint(ArrayCount-1)
		NewArray(i+1)=Rs(i)
	Next
	Rs.Close
	getDbValue=NewArray
	Erase NewArray
	Sql =""
End Function

'****************************************************
'函数名：getDBValueList
'作  用：按指定sql语句取记录集
'参  数：Sql           传入参数 Sql语句
'返回值:getDBValueList      输出参数 为一个二维数组，数组的第一项为是否取到数据，如果有数据则为大于0的数字，否则为0。参数一次类推为字段
'****************************************************
Function getDBValueList(Sql)

	Dim Comm,Rs
	Dim Recordcounts
	Dim ArrayRecord
	Set Comm=Server.CreateObject("ADODB.Command")
	With Comm
	.ActiveConnection = Conn
	.CommandType = 4
	.CommandText = "Columnget"
	.Prepared = true
	.Parameters.Append .CreateParameter("@Return",3,4)
	.Parameters.Append .CreateParameter("@Sql",200,1,2000,Sql)'参数1为定义的变量 2为数据类型 3为数据宽度 4为值
	Set Rs=.Execute()
	End With
	Rs.Close 
	RecordCounts=Comm(0)
	Rs.Open	
	If RecordCounts=0 Then 
		Redim ArrayRecord(0,0):ArrayRecord(0,0)=0
	Else
		ArrayRecord=Rs.GetRows()
	end if
	Rs.Close
	getDBValueList=ArrayRecord
	erase ArrayRecord
	Sql =""
End Function

function chkPara(byVal para)
	if para="" or isnull(para) or isempty(para) then
		chkPara=false
		exit function
	end if
	dim regEx
	set regEx=new regexp
	regEx.global=true
	regEx.pattern="^\d+(,?\d+)*$"
	chkPara=regEx.test(para)
	set regEx=nothing
end function

'删除地址中的参数
'surl 地址
'spara 要查找的参数
'返回格式： 如果长度大于2 返回?a=2& 的形式 否则 返回 ?
function delUrlpara(byVal surl,spara)
if surl="" or len(surl)<1 or isnull(surl) or isempty(surl) then
delUrlpara="?"
exit function
end if
dim urlarr,tmparr,tmparr2,t,i,j
Dim re,tmpt
spara=lcase(spara)+"="
surl=lcase(surl)

'提取参数
urlarr=split(surl,"?")
surl=urlarr(ubound(urlarr))
Set re = New RegExp
re.IgnoreCase = True
re.Global = True
re.Pattern = "^[A-Za-z][A-Za-z0-9_]{0,254}$"
tmparr=split(surl,spara)
t=""
for i=0 to ubound(tmparr)
	if instr(tmparr(i),"=")<>0 then
		tmparr2=split(tmparr(i),"&")
		for j=0 to ubound(tmparr2)			
			if instr(tmparr2(j),"=")<>0 then
				tmpt=split(tmparr2(j),"=")(0)
				'if re.test(tmpt) then
					t=t&tmparr2(j)&"&"
				'end if
			end if
		next
		erase tmparr2
	end if
next
erase tmparr
set re=nothing
if left(t,1)<>"?" then t="?"&t
if ubound(urlarr)>0 then t=urlarr(0)&t
erase urlarr
delUrlpara=t
end function

'获取网址的分页地址 返回http://www.ywhqs.com/product/list/T0_的格式
function getPageUrl(url_)
dim pos,lefts,rights
pos=instrrev(url_,"/")
lefts=left(url_,pos)
rights=right(url_,len(url_)-pos)
pos=instr(rights,"_p")
rights=left(rights,pos)
getPageUrl=lefts & rights
end function

'==================================================
'函数名：PostHttpPage
'作　用：登录
'参　数：RefererUr－－－－－登录地址
'参　数：PostUrl－－－－－提交地址
'参　数：PostData－－－－－用户参数
'==================================================
Function PostHttpPage(RefererUrl,PostUrl,PostData) 
    Dim xmlHttp 
    Dim RetStr      
    Set xmlHttp = CreateObject("Msx" & "ml2.XM" & "LHT" & "TP")  
    xmlHttp.Open "POST", PostUrl, False
    XmlHTTP.setRequestHeader "Content-Length",Len(PostData) 
    xmlHttp.setRequestHeader "Content-Type", "application/x-www-form-urlencoded"
    xmlHttp.setRequestHeader "Referer", RefererUrl
    xmlHttp.Send PostData 
    If Err.Number <> 0 Then 
        Set xmlHttp=Nothing
        PostHttpPage = "$False$"
        Exit Function
    End If
    PostHttpPage=bytesToBSTR(xmlHttp.responseBody,"GB2312")
    Set xmlHttp = nothing
End Function 

'删除指定的文件
function DelFile(byVal filename)
	On Error Resume Next	
	filename=server.MapPath(filename)	
	if err then
		DelFile=1
		err.clear
		filename=""
		exit function
	end if	
	dim delobj	
	set delobj=server.CreateObject("Scripting.FileSystemObject")
	if delobj.FileExists(filename) then
		delobj.DeleteFile(filename)
		if err then
			DelFile=3
			err.clear			
		else
			DelFile=0
		end if	
	else
		delFile=2
	end if
	set delobj=nothing	
	filename=""
end function

'查找内容里面的图片 返回一维数组 arr(0) 表示是否有找到 值0为没有找到 值为1有找到
function findContentPic(byval stbody)
redim tmparr(0)
dim matchs,i
dim findnum
tmparr(0)=0
findnum=0
dim regEx
set regEx=new regExp
regEx.ignoreCase=true
regEx.Global=True
regEx.pattern="src=.+?( |/>|>|/&gt;|&gt;)"
set matchs=regEx.execute(stbody)
for each i in matchs
	tmparr(0)=1
	redim preserve tmparr(findnum+1)
	tmparr(findnum+1)=replace(replace(replace(replace(replace(replace(replace(replace(i.value,"src=",""),"&quot;",""),"""","")," ",""),"/>",""),">",""),"/&gt;",""),"&gt;","")
	findnum=findnum+1
next
set regEx=nothing
set matchs=nothing
findContentPic=tmparr
stbody="":erase tmparr
end function

'检测传递的参数是否为大于0的数字型
Function Chknumber(byVal Para)
Chknumber=False
Para=trim(Para)
dim tempNum
If not IsNull(Para) and not Para="" and IsNumeric(Para) and not isempty(para) Then
	Chknumber=true
End If
Para="":tempNum=""
End Function

'sourse_要检测的数据 regExp_正则表达式 smode模式 为-1时提取所有匹配 大于等于0时提取对应的位置匹配值
'没有找到则返回空
Function getRegExpStr(ByVal sourse_, ByVal regExp_, smode)
Dim regEx, match, matches    ' 建立变量。
Dim RetStr
Dim ti
ti = 0
RetStr = ""
Set regEx = New RegExp ' 建立规范表达式。
regEx.Pattern = regExp_ ' 设置模式。
regEx.IgnoreCase = True ' 设置是否区分字母的大小写。
regEx.Global = True ' 设置全程性质。
Set matches = regEx.Execute(sourse_) ' 执行搜索。
For Each match In matches ' 重复匹配集合
    If smode = -1 Then
        RetStr = RetStr & match.Value
    Else
        If ti = smode Then
            RetStr = match.Value
            Exit For
        End If
    End If
    ti = ti + 1
Next
getRegExpStr = RetStr
Set regEx = Nothing
End Function

function getNumByStr(byval tstr_)
getNumByStr=getRegExpStr(tstr_,"\d+",-1)
end function
%>