<%
'获取列表 返回地址数组
'smode为1是数组的最后一列为产品的页面总数
function getPageList(surl,smode)
dim tstr,tbody
dim tarr,tpos
dim tmpurl,i
const thttp="http://www.ywhqs.com"
dim s,e
s="<li><div class=""photo""><a href="""
e=""""

i=0
tbody=getHttpPage(surl)
'产品地址
tstr=split(getBody(tbody,1,s,e,false,false),"|")
tmpurl=tstr(0)
tpos=tstr(1)
'response.Write(tbody)
'response.End()
erase tstr
redim tarr(1,i)
if tmpurl<>"$False$" then	
	tarr(0,i)=tmpurl
	
	'产品小图
	tstr=split(getBody(tbody,tpos,"src=""","""",false,false),"|")
	tarr(1,i)=tstr(0)
	tpos=tstr(1)
	erase tstr
else
	tarr(0,i)="$False$"
	tarr(1,i)=""	
end if
do while tmpurl<>"$False$" and i<100
	'产品链接
	tstr=split(getBody(tbody,tpos,s,e,false,false),"|")
	tmpurl=tstr(0)
	tpos=tstr(1)
	erase tstr
	i=i+1
	redim preserve tarr(1,i)
	if tmpurl<>"$False$" then
		tarr(0,i)=tmpurl
		tstr=split(getBody(tbody,tpos,"src=""","""",false,false),"|")
		tarr(1,i)=tstr(0)
		tpos=tstr(1)
		erase tstr
	else
		tarr(0,i)="$False$"
		tarr(1,i)=""
	end if
loop
if smode=1 then
	i=i+1
	redim preserve tarr(1,i)
	tstr=split(getBody(tbody,1,"条，共","页",false,false),"|")
	tmpurl=getinnertext(tstr(0))
	erase tstr
	if not chkrequest(tmpurl) then tarr(0,i)=0 else tarr(0,i)=tmpurl
end if
getPageList=tarr
erase tarr
tbody=""
end function

'获取产品参数
function getProductDetail(surl)
dim tstr,tbody
dim tpiclist,pichtml,tpos,i
dim tarr(15)
tbody=getHttpPage(surl)

'名称
tstr=split(getBody(tbody,1,"<div class=""price"">","</h2>",false,false),"|")
if tstr(0)<>"$False$" then tarr(0)=getinnertext(tstr(0)) else tarr(0)=""
erase tstr

'关键字
tarr(1)=tarr(0)

'描述
tarr(2)=tarr(0)

'大图 多个图片以|分隔
const thttp="http://www.ywhqs.com/"
tstr=split(getBody(tbody,1,"<li class=""on""><img src=""","""",false,false),"|")
if tstr(0)="$False$" then tarr(0)=""
tarr(3)=replace(tstr(0),"_syt","")
erase tstr

'no error
'编号
tstr=split(getBody(tbody,1,"商品编号：","</span>",false,false),"|")
if tstr(0)<>"$False$" then tarr(4)=getinnertext(tstr(0)) else tarr(4)=""
erase tstr
tarr(5)="个"

'尺寸
tstr=split(getBody(tbody,1,"<th>[单品尺寸]</th>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(6)=getinnertext(tstr(0)) else tarr(6)=""
erase tstr

'重量
tstr=split(getBody(tbody,1,"<th>[单品重量]</th>","公斤",false,false),"|")
if tstr(0)<>"$False$" then tarr(7)=getinnertext(tstr(0)) else tarr(7)="0"
'tarr(7)=lcase(tarr(7))
'tarr(7)=replace(tarr(7),"公斤","")
'tarr(7)=replace(tarr(7),"千克","")
'tarr(7)=replace(tarr(7),"kg","")
'tarr(7)=replace(tarr(7),"/","")
'tarr(7)=trim(tarr(7))
'if instr(tarr(7),"克")>1 or instr(tarr(7),"g")>1 then
'tarr(7)=replace(tarr(7),"克","")
'tarr(7)=replace(tarr(7),"g","")
'if isnumeric(tarr(7)) then
'	tarr(7)=cdbl(tarr(7))/1000 '如果单位为克，则转换为千克
'end if
'end if
'if not isnumeric(tarr(7)) then tarr(7)=0 else tarr(7)=cdbl(tarr(7))
'tarr(7)=cstr(tarr(7))
'if left(tarr(7),1)="." then tarr(7)="0"+tarr(7)
erase tstr

'材质
tstr=split(getBody(tbody,1,"<th>[商品材质]</th>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(8)=getinnertext(tstr(0)) else tarr(8)=""
erase tstr

'产地
'tstr=split(getBody(tbody,1,"[商品产地]</font></div></td>","</td>",false,false),"|")
'if tstr(0)<>"$False$" then tarr(9)=getinnertext(tstr(0)) else tarr(9)=""
'erase tstr
tarr(9)=""

'装箱数量
tstr=split(getBody(tbody,1,"<th> [装箱数量]</th>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(10)=getNumByStr(tstr(0)) else tarr(10)=""
erase tstr

'毛重净重
tstr=split(getBody(tbody,1,"<th>[毛重净重]</th>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(11)=getinnertext(tstr(0)) else tarr(11)=""
erase tstr
'no erorr

'批发价
tstr=split(getBody(tbody,1,"<p class=""mb10"">批发价：￥<strong>","</strong></p>",false,false),"|")
tarr(12)=getinnertext(tstr(0))
erase tstr

'成本价  需要登陆 暂时没有实现
tstr=split(getBody(tbody,1,"您的价格:<font color=#FF0000>","</font>",false,false),"|")
tarr(15)=tstr(0):tarr(15)=0
erase tstr

'现货数量
tstr=split(getBody(tbody,1,"<li class=""dat4"">现货数量：","</li>",false,false),"|")
if tstr(0)<>"$False$" then tarr(13)=getNumByStr(tstr(0)) else tarr(13)=0
if not chkrequest(tarr(13)) then tarr(13)=0
erase tstr
'商品简介
tstr=split(getBody(tbody,1,"<h3> 产品描述：</h3>","</div>",false,false),"|")
if tstr(0)<>"$False$" then tarr(14)=replace_text(tstr(0)) else tarr(14)=""
erase tstr

getProductDetail=tarr
tbody="":erase tarr
end function

'检查产品有效性 如果有效返回ok，否则返回错误信息
function chkValidPro(byVal proArr)
dim errmsg,finderr
errmsg="":finderr=false
if ubound(proArr)<>15 then
	chkValidPro="产品数组获取失败"
	exit function
end if

if ChkNull(proArr(0),2) then
	finderr=true:errmsg=errmsg&","&"产品名称获取失败"
end if
if ChkNull(proArr(3),5) then
	finderr=true:errmsg=errmsg&","&"产品图片获取失败"
end if
if ChkNull(proArr(4),1) then
	finderr=true:errmsg=errmsg&","&"产品编号获取失败"
end if

if not Chknumber(proArr(12)) or proArr(12)="0" then
	finderr=true:errmsg=errmsg&","&"产品普通会员价格获取失败 price="&proArr(12)
end if
'if not chkrequest(proArr(15)) then
'	finderr=true:errmsg=errmsg&","&"产品成本价格获取失败"
'end if
if ChkNull(proArr(14),5) then
	finderr=true:errmsg=errmsg&","&"产品介绍获取失败"
end if
if finderr then chkValidPro=right(errmsg,len(errmsg)-1) else chkValidPro="ok"
erase proArr
end function

'进行添加产品操作
function addwkProducts(lyurl,wksmallpic,ssortpath)
dim proarr,phh,picList,picArr,picExt,isvalidPro,smallpic,wkbigpic,bigpic,picname,pid
dim issave1,issave2
dim rs,rs1,hh,sql,rndnum
proarr=getProductDetail(lyurl)

isvalidPro=chkValidPro(proArr)
if isvalidPro<>"ok"  then
	addwkProducts=array(1,"<font color=red>产品参数错误,原因:<br>"&isvalidPro&" 已放弃添加!</font>&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a><br>")
	exit function
end if

picList=proArr(3)
picArr=split(picList,"|")
wkbigpic=picArr(0)
erase picArr
picExt=getExt(wkbigpic)	
phh=lcase(proArr(4))
phh=replace(phh,"hqs","wj") '编号替换
				
sql="select top 1 * from products where [lyid]=1 and lyurl='"&lyurl&"'"			
set rs=server.CreateObject("adodb.recordset")
rs.open sql,conn,1,3
if rs.bof and rs.eof then
	picname="/uploadpic/bashi/"&makeFilename()
	smallpic=picname&"small"&picExt
	bigpic=picname&"big"&picExt
	if chkNull(wksmallpic,10) then
		addwkProducts=array(3,"产品小图丢失")
		rs.close
		set rs=nothing
		erase proArr
		exit function
	end if
	if not chkPara(ssortpath) then
		addwkProducts=array(4,"产品分类没有选择")
		rs.close
		set rs=nothing
		erase proArr
		exit function
	end if	
else
	smallpic=rs("smallpic")
	bigpic=rs("bigpic1")	
	phh=rs("hh")
end if



if rs.bof and rs.eof then	
	issave1=SaveRemoteFile(bigpic,wkbigpic)	
	if wksmallpic="" then 
	smallpic=bigpic
	issave2=true
	else
	issave2=SaveRemoteFile(smallpic,wksmallpic)	
	end if
	if issave1 and issave2 then
		sql="select top 1 id from products where hh='"&phh&"'"
		set rs1=server.CreateObject("adodb.recordset")
		rs1.open sql,conn,1,1
		if not (rs1.bof and rs1.eof) then
			randomize timer
			rndnum=int(rnd*1000)
			phh=phh&rndnum
		end if
		rs1.close
		set rs1=nothing
		rs.addnew					
		rs("adduser")="caijihqs"
		rs("lyID")=1
		rs("lyUrl")=lyurl			
		rs("hits")		= 0
		rs("min")		= 1					
		rs("color")		= "3847"
		rs("size")		= "0"
		rs("baozhuang")	= ""
		rs("bid")		= 62			'子ID		
		rs("zheke")		= 1		'折扣
		rs("flag")		= 1
		rs("p_key")		= 0		'是否推荐
		rs("p_key1")	= 3	' 推荐度
		rs("keytime")	= now()
		rs("createdon")	= now()
		rs("ljxs")		= 0
		rs("serice")	= ""	'适合季节(体积)
		rs("tuai")		= ""		'图案(装箱规格)		
		rs("key1")		= 0
		rs("key2")		= 0
		rs("sxjf")		= 	0			'所属积分
		rs("hh")		= phh		'货号
		rs("sortpath")	= ssortpath		'商品类别路径
		rs("smallpic")	= smallpic	'小图
		rs("bigpic1")	= bigpic		'大图	
		proarr(14)=autoSavepic(proarr(14),phh)(2)	
		rs("name")		= proArr(0)		'名称	
		rs("keywords")	= proArr(1)	'关键字												
		rs("content")	= proarr(14)	'详细说明
		rs("ms")		= proarr(2)		'商品描述
		rs("price1")	= cdbl(proarr(12))*1.8	'市场价格	
		rs("price2")	= proarr(15)	'成本价格
		rs("ks")		= proarr(6)		'款式(尺寸)
		rs("zd")		= proarr(8)		'主要质地(材质)					
		rs("fg")		= proarr(7)		'风格(毛净重)
		rs("jl")		= proarr(5)
		rs("zhuangxiao")= proarr(10)	
		rs("mmid")		= 0	
		rs("price")		= proarr(12)	'普通会员价					
		rs("jf")		= clng(proarr(12))	'积分
		rs("max")		= proarr(13)
		rs("updatetime")  = now()	
		rs.update
		pid=rs("id")
		addwkProducts=array(0,pid)	
		
	else
		if issave1 then call delfile(smallpic)
		if issave2 then call delfile(bigpic)
		if not issave1 then 			
			addwkProducts=array(2,"<font color=red>产品小图保存失败，放弃添加!</font>&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a>&nbsp;本地："&server.MapPath(smallpic)&"&nbsp;远程："&wksmallpic&"<br>")	
		end if
		if not issave2 then 		
		addwkProducts=array(2,"<font color=red>产品大图保存失败，放弃添加!</font>&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a>&nbsp;本地："&server.MapPath(bigpic)&"&nbsp;远程："&wkbigpic&"<br>")	
		end if
	end if						
else	
	'rs("price")		= proarr(12)	'普通会员价					
	'rs("jf")		= clng(proarr(12))	'积分
	'rs("smallpic")	= smallpic	'小图
'	rs("bigpic1")	= bigpic		'大图
'	proarr(14)=autoSavepic(proarr(14),phh)(2)
'	rs("content")	= proarr(14)	'详细说明
'	rs("updatetime")= now()		
	rs("max")		= proarr(13)
	
	rs.update
	pid=rs("id")
	addwkProducts=array(0,pid)	
end if	
set rs=nothing	
erase proarr

end function

'进行添加产品操作
function downProducts(lyurl)
dim proarr,phh,picList,picArr,picExt,isvalidPro,smallpic,wkbigpic,bigpic,picname,pid
dim issave1,issave2
dim rs,sql
proarr=getProductDetail(lyurl)

isvalidPro=chkValidPro(proArr)
if isvalidPro<>"ok"  then
	downProducts=array(1,"<font color=red>产品参数错误,原因:<br>"&isvalidPro&" 已放弃添加!</font>&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a><br>")
	exit function
end if				
sql="select top 1 flag from products where [lyid]=1 and lyurl='"&lyurl&"'"			
set rs=server.CreateObject("adodb.recordset")
rs.open sql,conn,1,3
if rs.bof and rs.eof then
	downProducts=array(2,"本产品在58bashi上不存在&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a><br>")
	exit function
else
	rs("flag")=0
	rs.update
	downProducts=array(0,"")
end if
set rs=nothing	
erase proarr

end function

'进行添加产品操作
function updateProducts(lyurl)
dim proarr,phh,isvalidPro,pid
dim rs,sql
proarr=getProductDetail(lyurl)
'response.Write("地址："&lyurl&",名称:"&proArr(0)&",大图:"&proArr(3)&",编号:"&proArr(4)&",尺寸:"&proArr(6)&",重量:"&proArr(7)&",材质:"&proArr(8)&",装箱数量:"&proarr(10)&",毛重净重:"&proarr(11)&",批发价:"&proarr(12)&",现货数量:"&proarr(13)&"<br>---------------------")
'response.Write("<br>"&proarr(14))
'response.End()
isvalidPro=chkValidPro(proArr)
if isvalidPro<>"ok"  then
	updateProducts=array(1,"<font color=red>产品参数错误,原因:<br>"&isvalidPro&" 已放弃添加!</font>&nbsp;<a href="""&lyurl&""" target=""_blank"">查看此产品</a><br>")
	exit function
end if
phh=lcase(proArr(4))
phh=replace(phh,"hqs","wj") '编号替换
				
sql="select top 1 id,lyurl from products where hh='"&phh&"' and lyid=1"			
set rs=server.CreateObject("adodb.recordset")
rs.open sql,conn,1,3
if rs.bof and rs.eof then
	updateProducts=array(0,0)			
	rs.close
	set rs=nothing
	exit function
else	
	pid=clng(rs("id"))
	rs("lyurl")		= lyurl	
	rs.update	
	updateProducts=array(0,pid)	
end if	
set rs=nothing	
erase proarr

end function

'检查是否登陆
function chkwkLogin()
dim tbody
const thttp="http://www.ywhqs.com/profile.asp?action=profile"
tbody=getHttpPage(thttp)
response.Write(server.HTMLEncode(tbody))
if instr(tbody,"欢迎：<b>")>0 then chkwkLogin=true else chkwkLogin=false
tbody=""
end function

'自动保存内容里面的图片 图片保存地址：/uploadfiles 命名方式：编号+随机图片名
'stbody 要替换的内容 shh产品的编号
'返回数组 共找到的图片总数,已经保存的图片总数,替换后的内容
function autoSavePic(byval stbody,shh)
dim tmparr
dim tmpurl,picurl,i
dim tpic,picExt,findnum,savenum
dim issave
tmparr=findContentPic(stbody)
findnum=0:savenum=0
if tmparr(0)=1 then
	for i=1 to ubound(tmparr)
		tmpurl=tmparr(i)
		picurl=lcase(tmpurl)
		if left(picurl,4)<>"http" then
			if left(picurl,1)<>"/" then picurl="/"&picurl
			picurl="http://www.ywhqs.com"&picurl
		end if
		picExt=getExt(picurl)
		tpic="/uploadpic/bashi/"&shh&"_"&makeFilename()&picExt	
		issave=SaveRemoteFile(tpic,picurl)
		if issave then
			savenum=savenum+1
			stbody=replace(stbody,tmpurl,tpic)
		end if
		findnum=findnum+1
	next
end if
erase tmparr
autoSavePic=array(findnum,savenum,stbody)
stbody=""
end function

'检查网址是否有效
function chkUrl(byVal surl)
surl=lcase(surl)
if instr(surl,"ywhqs.com")=0 then 
chkUrl=false
else
chkUrl=true
end if
end function

function chkPara(byVal para)
	if para="" or isnull(para) or isempty(para) then
		chkPara=false
		exit function
	end if
	dim regEx
	set regEx=new regexp
	regEx.global=true
	regEx.pattern="^\d+(,?\d+)*$"
	chkPara=regEx.test(para)
	set regEx=nothing
end function
%>