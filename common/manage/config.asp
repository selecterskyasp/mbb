﻿<!--#include file="top.asp"-->
<!--#include file="kfSub.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css" href="style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<script type="text/javascript" src="../ajaxupload/AnPlus.js"></script>
<script type="text/javascript" src="../ajaxupload/AjaxUploader.js"></script>
</head>

<body>
<%
tqxarr=getqx(0,qxarr)
if tqxarr(0)<>1 then showqxmsg
dim action
action=request.QueryString("action")
if action="save" then
	if tqxarr(1)<>1 then showqxmsg
	tbody=chr(60)&"%"&vbcrlf
	tbody=tbody&""&vbcrlf
	tbody=tbody&"weburl			= """&replace(replace(request.Form("weburl"),"'",""),"""","")&"""'网站网址"&vbcrlf
	tbody=tbody&"sweburl		= right(weburl,len(weburl)-len(""http://""))"&vbcrlf
	tbody=tbody&"ssweburl		= replace(sweburl,""www."","""")"&vbcrlf	
	tbody=tbody&"webname			= """&replace(replace(request.Form("webname"),"'",""),"""","")&"""'网站名称"&vbcrlf
	tbody=tbody&"webtitle		= """&replace(replace(request.Form("webtitle"),"'",""),"""","")&"""'公司名称"&vbcrlf
	tbody=tbody&"webicp		= """&replace(replace(request.Form("webicp"),"'",""),"""","")&"""'icp备案号"&vbcrlf
	tbody=tbody&"weblogo		= """&replace(replace(request.Form("weblogo"),"'",""),"""","")&"""'网站LOgo"&vbcrlf
	tbody=tbody&"webbanner		= """&replace(replace(request.Form("webbanner"),"'",""),"""","")&"""'网站banner"&vbcrlf
	tbody=tbody&"webbannermemo		= """&replace_text(replace(request.Form("webbannermemo"),"'",""))&"""'网站banner说明"&vbcrlf
	tbody=tbody&"webcontact		= """&replace(replace(request.Form("webcontact"),"'",""),"""","")&"""'公司地址"&vbcrlf
	tbody=tbody&"webphone		= """&replace(replace(request.Form("webphone"),"'",""),"""","")&"""'联系电话"&vbcrlf
	tbody=tbody&"weballowext		= """&replace(replace(request.Form("weballowext"),"'",""),"""","")&"""'允许上传的文件"&vbcrlf
	tbody=tbody&"webclosereg		= "&request.Form("webclosereg")&"'是否关闭会员注册"&vbcrlf
	tbody=tbody&"websendmail		= "&request.Form("websendmail")&"'会员注册是否发送邮件"&vbcrlf
	tbody=tbody&"webhtml		= "&request.Form("webhtml")&"'网站浏览模式"&vbcrlf
	tbody=tbody&"webautozeng	= "&request.Form("webautozeng")&"'同步产品自动增加产品价格的百分比"&vbcrlf
	tbody=tbody&"webservicetime		= """&request.Form("webservicetime")&"""'会员客服服务时间"&vbcrlf
	tbody=tbody&"webkfshow		= """&request.Form("webkfshow")&"""'内页客服是否默认显示"&vbcrlf
	tbody=tbody&"webping		= """&replace(replace(request.Form("webping"),"'",""),"""","")&"""'屏蔽注册用户名"&vbcrlf
	tbody=tbody&"webpingkeyword		= """&replace(replace(request.Form("webpingkeyword"),"'",""),"""","")&"""'屏蔽的关键字"&vbcrlf
	tbody=tbody&"webcloseweb		= "&request.Form("webcloseweb")&"'是否关闭网站"&vbcrlf
	tbody=tbody&"%"&chr(62)&vbcrlf
	
	tarr=SaveToFileauto(tbody,"/inc/config.asp","utf-8")
	if tarr(0)<>0 then
	call alert("保存文件出错，错误ID号："&tarr(0)&" 错误信息："&tarr(1)&" 当前的操作没有保存","",1)
	end if
	erase tarr
	tbody=""	
	
	response.Redirect("config.asp")
end if	

call createPageheadJS()
call createPagetopJS()

'生成客服JS
tarr=createKFjs()
if tarr(0)<>0 then alert "保存文件产生错误，错误ID："&tarr(0)&"；错误描叙："&tarr(1)&"，当前操作没有保存","",1
erase tarr
if not chkNumber(webautozeng) then webautozeng=0
if not chkNumber(webkfshow) then webkfshow=1
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<form name="form1" method="post" action="config.asp?action=save" onSubmit="return Validator.Validate(this,2);">
<tr bgcolor="#efefef">
    <td colspan=2>　<strong>系统信息</strong><br /><font color="#FF0000">(注意：输入的字符不能包含标点符号（如："）等特殊字符，否则可能会导致整个网站不能访问，请谨慎修改)</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">公司名称</td>
	<td width="77%" align="left"><input type="text" name="webtitle" size=60 value="<%=webtitle%>" dataType="LimitB" min="4" max="50" msg="网站标题不能为空(4-50个字)！"></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">网站名称</td>
	<td width="77%" align="left"><input type="text" name="webname" size=30 value="<%=webname%>" dataType="LimitB" min="4" max="50" msg="网站标题不能为空(4-50个字)！">&nbsp;ICP备案号:<input type="text" name="webicp" size=30 value="<%=webicp%>"></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">网站网址</td>
	<td width="77%" align="left"><input type="text" name="weburl" size=30 value="<%=weburl%>" dataType="Url" msg="网站地址不匹配！">
<font color="#FF0000">(后面不要有/)</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">网站LOGO</td>
	<td width="77%" align="left">
<input type="text" name="weblogo" id="weblogo" size=30 value="<%=weblogo%>" accept="gif|jpg|png" datatype="Filter" msg="请添加 jpg|gif|png 格式!" />&nbsp;<INPUT alt="请单击“浏览”上传图" TYPE="button" value="上传" onClick="showUploader('weblogo',this);"></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">网站banner</td>
	<td width="77%" align="left">
<input type="text" name="webbanner" id="webbanner" size=30 value="<%=webbanner%>" accept="gif|jpg|png" datatype="Filter" msg="请添加 jpg|gif|png 格式!" />&nbsp;<INPUT alt="请单击“浏览”上传图" TYPE="button" value="上传" onClick="showUploader('webbanner',this);"></td>
</tr>
<div id="uploadContenter" style="background-color:#eeeeee;position:absolute;border:1px #555555 solid;padding:3px;"></div>
<iframe style="display:none;" name="AnUploader"></iframe>
<script type="text/javascript">
	/*======================================
	下面语句使上传控件显示在上面ID为uploadContenter的Div标签
	提交URL为upload.asp保存目录为upload
	表单提交到上面name属性为AnUploader的iframe里面；
	========================================*/
	
	var AjaxUp=new AjaxProcesser("uploadContenter");
	
	//设置提交到的iframe名称
	AjaxUp.target="AnUploader";  
	
	//上传处理页面,尽量不要更改
	AjaxUp.url="<%=webvirtual%>/ajaxupload/manageupload.asp"; 
	
	//保存目录
	AjaxUp.savePath="/uploadfiles";
	
	var contenter=document.getElementById("uploadContenter");
	contenter.style.display="none"; //隐藏容器
	
	function showUploader(objID,srcElement){
		AjaxUp.reset();  //重置上传控件		
		contenter.style.display="block"; //显示容器
		var ps=_.abs(srcElement);//作用--获取指定标签的绝对坐标,目的是为了把上传控件定位到按钮下面
		contenter.style.top=(ps.y + 30) + "px";  
		contenter.style.left=(ps.x-180) + "px";
		//上传成功时要执行的程序
		AjaxUp.succeed=function(files){
		    var fujian=document.getElementById(objID);
			fujian.value=AjaxUp.savePath + "/" + files[0].name;  //因为上传控件就只上传一个 文件，这里索引是0
			contenter.style.display="none";			
			document.getElementById("sperr").style.display="none";
		}
		//上传失败时要执行的程序
		AjaxUp.faild=function(msg){document.getElementById(errID).innerText="文件上传失败，原因："+msg;contenter.style.display="none";document.getElementById("sperr").style.display="block"}
	}
</script>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">文字说明</td>
	<td width="77%" align="left">
<input type="text" name="webbannermemo" id="webbannermemo" size=80 value="<%=webbannermemo%>" datatype="Require" msg="请输入banner下的文字说明!" /></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">公司地址</td>
	<td width="77%" align="left"><input type="text" name="webcontact" size="55" value="<%=webcontact%>" dataType="Require" msg="公司地址不能为空！"/>
	  &nbsp;&nbsp;客服电话
      <input type="text" name="webphone" size="20" value="<%=webphone%>" datatype="LimitB" min="6" max="15" msg="联系电话不匹配！"/></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">允许上传的文件</td>
	<td width="77%" align="left"><input type="text" name="weballowext" size="80" value="<%=weballowext%>" />
	  <font color="#FF0000">以|分隔</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">是否关闭会员注册</td>
	<td width="77%" align="left"><label>
	  <input type="radio" name="webclosereg" value="1" <%if webclosereg=1 then response.Write("checked")%> />
	  是
	  <input name="webclosereg" type="radio" value="0" <%if webclosereg=0 then response.Write("checked")%> />
	  否  
	是否邮件激活
	  <input type="radio" name="websendmail" value="1" <%if websendmail=1 then response.Write("checked")%> />
是
<input name="websendmail" type="radio" value="0" <%if websendmail=0 then response.Write("checked")%> />
否 是否关闭网站
<input type="radio" name="webcloseweb" value="1" <%if webcloseweb=1 then response.Write("checked")%>/>
是
<input name="webcloseweb" type="radio" value="0" <%if webcloseweb=0 then response.Write("checked")%> />
否 </label></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">网站浏览方式</td>
	<td width="77%" align="left"><label>
	  <input type="radio" name="webhtml" value="1" <%if webhtml=1 then response.Write("checked")%> />
	  动态浏览
	  <input name="webhtml" type="radio" value="2" <%if webhtml=2 then response.Write("checked")%> disabled="disabled" />
	  伪静态浏览
	   <input name="webhtml" type="radio" value="3" <%if webhtml=3 then response.Write("checked")%> disabled="disabled"/>
	  纯静态浏览
	</label></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">价格上调百分比</td>
	<td width="77%" align="left"><input type="text" style="width:50px" name="webautozeng"  value="<%=webautozeng%>" />%
	  <br /><span class="red">（此设置是在您同步我们的产品时，自动给新产品价格增加的百分比 请设置为0-100的数字 0为不增加）</span> </td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">客服服务时间</td>
	<td width="77%" align="left"><input type="text" name="webservicetime"  style="width:200px" value="<%=webservicetime%>" /> 
	内页客服是否显示 
	  <input name="webkfshow" type="radio" value="1" <%if webkfshow=1 then response.Write("checked")%>/>
是
<input name="webkfshow" type="radio" value="0" <%if webkfshow=0 then response.Write("checked")%>/>
否</td>
</tr>

<tr bgcolor="#ffffff">
    <td width="23%" align="center">屏蔽注册用户名</td>
	<td width="77%" align="left"><textarea name="webping" cols="60" rows="3"  datatype="Require" msg="网站描述不能为空！"><%=webping%></textarea>
	  <font color="#FF0000"> 以|分隔</font> </td>
</tr>
<tr bgcolor="#ffffff">
    <td width="23%" align="center">屏蔽关键字</td>
	<td width="77%" align="left"><textarea name="webpingkeyword" cols="60" rows="3"><%=webpingkeyword%></textarea>
	  <font color="#FF0000">以|分隔</font></td>
</tr>

<tr bgcolor="#ffffff" align="center">
    <td height="30" colspan=2><input type="submit" name="Submit" value="提 交" class=input1><div style="color:#FF0000; display:none" id="sperr"></div></td>
</tr>
</form>
<%
call closeconn()%>
</table>
</body>
</html>
