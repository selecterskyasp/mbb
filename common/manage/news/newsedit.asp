﻿<!--#include file="../top.asp"-->
<!--#include file="../../fckeditor/fckeditor.asp"-->
<%
tqxarr=getqx(17,qxarr)
if tqxarr(1)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>信息添加与修改</title>
<link href="../style/css.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<script src="<%=webvirtual%>/js/jquery.js" type="text/javascript"></script>
<script src="<%=webvirtual%>/jquery/lib/jquery.form.js" type="text/javascript"></script>
<script src="<%=webvirtual%>/js/function.js" language="javascript"></script>
<script language="javascript">
function _sendData(f,act,re,h,obj)
{
	document.getElementById("content").value=getEditorHTMLContents("content");
	return cjx.sendData(f,act,re,h,obj);	
}
// 获取编辑器中HTML内容   
function getEditorHTMLContents(EditorName) {  
    var oEditor = FCKeditorAPI.GetInstance(EditorName);   
    return(oEditor.GetXHTML(true));   
}   
   
// 获取编辑器中文字内容   
function getEditorTextContents(EditorName) {   
    var oEditor = FCKeditorAPI.GetInstance(EditorName);   
    return(oEditor.EditorDocument.body.innerText);   
}   
   
// 设置编辑器中内容   
function SetEditorContents(EditorName, ContentStr) {   
    var oEditor = FCKeditorAPI.GetInstance(EditorName) ;   
    oEditor.SetHTML(ContentStr) ;   
}
</script>
</head>

<body>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
  <tr>
    <td class="tbTitle">后台 > <a href="list.asp">信息管理</a> > 信息添加与修改</td>
  </tr>
</table>
<%
id=request.querystring("id")
if id="" then 
	title		= ""	
	classid		= 0	
	keyword		= ""
	ms			= ""
	ly			= ""
	Sourcer		= webname
	content		= ""
	action		= "add"
else
set rs=server.CreateObject("adodb.recordset")
rs.open "select top 1 [name],bid,keywords,ly,content,ms from news where id="&cint(id),conn,1,1
if not rs.eof then 
	title		= rs("name")	
	classid		= rs("bid")	
	keyword		= rs("keywords")
	ms			= rs("ms")
	Sourcer		= rs("ly")
	content		= rs("content")
	action		= "edit"
end if 
rs.close
set rs=nothing
end if 
%>
<table border="0" cellspacing="0" cellpadding="0" class="MsgTbl">
<form name="form1" id="form1" method="post" onsubmit="return Validator.Validate(this,3);">
	<input type="hidden" name="action" value="<%=action%>">	
	<input type="hidden" name="id" value="<%=id%>">	
  <tr>
    <td class="tbLabel">信息标题:</td>
    <td class="tbMsg"><input name="title" type="text" value="<%=title%>" dataType="LimitB" min="2" max="50" msg="请输入信息标题2-50字以内!" size="80"/></td>
  </tr>
  <tr>
    <td class="tbLabel">信息类别:</td>
    <td class="tbMsg">
	<select name="classid" dataType="Require" msg="请选择信息类别!">
	<%
	set rs=server.createobject("adodb.recordset")
	rs.open "select id,name from curclass where parentid=2 order by adddate desc",conn,1,1
	if not rs.eof then 
	 	do while not rs.eof 
	%>
		<option value="<%=rs("id")%>" <%if cint(rs("id")) = cint(classid) then response.write "selected"%>><%=rs("name")%></option>
	<%
		rs.movenext
		loop
	end if 
	rs.close
	set rs=nothing
	%>
	</select>	</td>
  </tr>
  <tr>
    <td class="tbLabel">关 键 字:</td>
    <td class="tbMsg"><input name="keyword" type="text" value="<%=keyword%>" size="65" dataType="LimitB" min="2" max="200" msg="请输入关键字2-200字以内!"/>
    （多个关键字以|分隔）</td>
  </tr> 
  <tr>
    <td class="tbLabel">信息描叙:</td>
    <td class="tbMsg"><textarea name="ms" cols="80" datatype="LimitB" min="1" max="500" msg="请输入关键字500字以内!" require="false"><%=ms%></textarea>
      <br />
      （如果留空为提取信息内容的前150字）</td>
  </tr>
  <tr>
    <td class="tbLabel">信息内容:</td>
    <td class="tbMsg">
	<%
	Dim oFCKeditor
	Set oFCKeditor = New FCKeditor
	oFCKeditor.BasePath =webvirtual&"/fckeditor/"
	oFCKeditor.Value	= replace_t(content)
	oFCKeditor.width	= "100%"
	oFCKeditor.height	= "350"	
	'oFCKeditor.ToolbarSet	= "Basic"
	oFCKeditor.Create "content"
	set oFCKeditor=nothing
	%>	
	</td>
  </tr>
  <tr>
    <td class="tbLabel">信息来源:</td>
    <td class="tbMsg"><input name="Sourcer" type="text" value="<%=Sourcer%>" size="10" dataType="Require" msg="信息来源!"/></td>
  </tr>
  <tr>
    <td class="tbLabel">&nbsp;</td>
    <td class="tbMsg"><input name="submit" type="submit" value="提交" onclick="return _sendData('form1','save.asp','result','list.asp?page=<%=request.QueryString("page")%>',this);"/>
	<input name="submit" type="reset" value="重置" /></td>
  </tr>
   <tr>
    <td class="tbLabel"></td>  
	<td class="tbMsg"><span id="result" class="red"></span></td> 
  </tr> 
 </form>
</table>
<%
call closeconn()
%>
</body>
</html>
