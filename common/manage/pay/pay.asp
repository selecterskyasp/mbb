﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(7,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="pay.asp">支付方式管理</a>
	</td>
</tr>
</table>
<%
sql="select id,[name],url,flag,[default] from pay order by adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">名称</td>
	<td align="center">接口地址</td>	
	<td align="center">状态</td>
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 	
	for i=0 to ubound(tarr,2)		
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tarr(1,i)%><%if tarr(4,i) then response.Write("&nbsp;<span class=red>【默认】</font>")%></td>
	<td align="center"><%=tarr(2,i)%></td>	
	<td align="center"><%if tarr(3,i) then response.Write("<a href=""pay_save.asp?id="&tarr(0,i)&"&myAction=hid"" title=""点击隐藏"">已显示</a>") else response.Write("<a href=""pay_save.asp?id="&tarr(0,i)&"&myAction=ope"" title=""点击显示""><span class=red>已隐藏</span></a>")%></td>
	<td align="center"><a href="pay_save.asp?id=<%=tarr(0,i)%>&myAction=deleteSort" onclick="return confirm('该操作不能恢复，确认吗？');">删除</a> | <a href="pay.asp?pc=editSort&id=<%=tarr(0,i)%>">修改</a> | <a href="pay_save.asp?id=<%=tarr(0,i)%>&myAction=sort">排序</a> | <a href="pay_save.asp?id=<%=tarr(0,i)%>&myAction=def">设为默认</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="5" align="right"><a href="pay.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	if tqxarr(1)<>1 then showqxmsg
	id=clng(request.QueryString("id"))
	dim name,url,flag,default,adddate
	name="":url="":flag=true:default=false
	if pc="editSort" then		
		sql="select [name],url,[flag],[default] from pay where id="&id
		tarr=getdbvalue(sql,4)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		name=tarr(1):url=tarr(2):flag=tarr(3):default=tarr(4)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="pay_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">支付名称：</td>
      <td width="86%" align="left" bgcolor="#FFFFFF"><input name="tname" type="text" value="<%=name%>" size="30" dataType="LimitB" min="3" max="20" msg="支付名称必须为3-20个汉字以内"></td>
   </tr>
     <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">接口网址：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="url" type="text" value="<%=url%>" size="50" dataType="LimitB" min="5" max="80" msg="接口网址必须为5-80个字以内">     
       <span class="red">请输入支付接口的地址</span></td>
   </tr>   
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">是否显示：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="flag" type="radio" value="1" <%if flag then response.Write("checked")%>>是 <input name="flag" type="radio" value="0" <%if not flag then response.Write("checked")%>>否    是否默认：<input name="default" type="radio" value="1" <%if default then response.Write("checked")%>>是 <input name="default" type="radio" value="0" <%if not default then response.Write("checked")%>>否     
     </td>
   </tr>   
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
	   <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>