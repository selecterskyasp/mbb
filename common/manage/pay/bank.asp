﻿<!--#include file="../top.asp"-->
<%
tqxarr=getqx(8,qxarr)
if tqxarr(0)<>1 then showqxmsg
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>分类</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
<script type="text/javascript" src="../../ajaxupload/AnPlus.js"></script>
<script type="text/javascript" src="../../ajaxupload/AjaxUploader.js"></script>
</head>
<body>
<table width="90%" height="50" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#efefef">
    <td align="left">
	管理后台 - > <a href="bank.asp">银行支付帐号管理</a>
	</td>
</tr>
</table>
<%
sql="select id,bank,num,[name],flag,[default] from bank order by adddate desc"
tarr=getDBvalueList(sql)
%>
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<tr bgcolor="#ffffff">
    <td align="center">银行名称</td>
	<td align="center">银行帐号</td>
	<td align="center">收款人</td>
	<td align="center">状态</td>
	<td align="center">操作</td>
</tr>
<%
if clng(tarr(0,0))<>0 then 
	dim tempid,nextcount,ttarr,tparentid,prelink
	for i=0 to ubound(tarr,2)		
		%>
<tr bgcolor="#ffffff">
    <td align="center"><%=tarr(1,i)%><%if tarr(5,i) then response.Write("&nbsp;<span class=red>【推荐】</font>")%></td>
	<td align="center"><%=tarr(2,i)%></td>
	<td align="center"><%=tarr(3,i)%></td>
	<td align="center"><%if tarr(4,i) then response.Write("<a href=""bank_save.asp?id="&tarr(0,i)&"&myAction=hid"" title=""点击隐藏"">已显示</a>") else response.Write("<a href=""bank_save.asp?id="&tarr(0,i)&"&myAction=ope"" title=""点击显示""><span class=red>已隐藏</span></a>")%></td>
	<td align="center"><a href="bank_save.asp?id=<%=tarr(0,i)%>&myAction=deleteSort" onclick="return confirm('该操作不能恢复，确认吗？');">删除</a> | <a href="bank.asp?pc=editSort&id=<%=tarr(0,i)%>">修改</a> | <a href="bank_save.asp?id=<%=tarr(0,i)%>&myAction=sort">排序</a> | <a href="bank_save.asp?id=<%=tarr(0,i)%>&myAction=def">设为推荐</a></td>
</tr>
<%		
	next
end if
erase tarr
%>
<tr bgcolor="#efefef">
    <td colspan="5" align="right"><a href="bank.asp?pc=addSort">添加</a> | <a href="javascript:history.back();">返回</a></td>
</tr>
</table>
<%
dim pc
pc=request.QueryString("pc")' 类别操作的动作
if pc<>"" then 
	if tqxarr(1)<>1 then showqxmsg
	id=clng(request.QueryString("id"))
	dim bank,num,name,area,url,flag,default,adddate
	bank="":num="":name="":area="":url="":flag=true:default=false
	if pc="editSort" then
		sql="select [bank],num,[name],url,[flag],[default],area,images from bank where id="&id
		tarr=getdbvalue(sql,8)
		if tarr(0)=0 then
			alert "分类不存在","",1
		end if
		bank=tarr(1):num=tarr(2):name=tarr(3):url=tarr(4):flag=tarr(5):default=tarr(6):area=tarr(7):images=tarr(8)
		erase tarr
	end if
%>
<table width="90%" border="0" align="center" cellpadding="3" cellspacing="1" bgcolor="#cccccc">
   <form action="bank_save.asp" method="post" name="add" onSubmit="return Validator.Validate(this,2);">
		<input name="myAction" type="hidden"  value="<%=pc%>">		
		<input type="hidden" name="id" value="<%=id%>">
   <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">银行名称：</td>
      <td width="86%" align="left" bgcolor="#FFFFFF"><input name="bank" type="text" value="<%=bank%>" size="30" dataType="LimitB" min="3" max="20" msg="银行名称必须为3-20个汉字以内"></td>
   </tr>
     <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">银行卡号：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="num" type="text" value="<%=num%>" size="50" dataType="LimitB" min="10" max="30" msg="银行卡号必须为10-30个数字以内">     
       <span class="red">请认真填写，如果出错会导致客户打款不成功</span></td>
   </tr>
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">开户人：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="tname" type="text" value="<%=name%>" size="20" dataType="LimitB" min="2" max="20" msg="开户人必须为2-20个字以内">     
      <span class="red">开户人的真实姓名</span></td>
   </tr> 
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">银行网站：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="url" type="text" value="<%=url%>" size="50" dataType="LimitB" min="4" max="80" msg="请输入银行的官方网站4-80个字符">     
      <span class="red">如工商银行网站：http://www.icbc.com.cn</span></td>
   </tr> <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">银行LOGO：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="images" type="text" value="<%=images%>" size="50" dataType="LimitB" min="4" max="80" msg="银行LOGO必须上传"> <INPUT alt="请单击“浏览”上传图" TYPE="button" value="浏览" onClick="showUploader('images',this);"> 
	  <div style="color:#FF0000; display:none" id="sperr"></div>    
      <span class="red">图片的尺寸必须为：107*20</span></td>
   </tr>
   <div id="uploadContenter" style="background-color:#eeeeee;position:absolute;border:1px #555555 solid;padding:3px;"></div>
<iframe style="display:none;" name="AnUploader"></iframe>
<script type="text/javascript">
	/*======================================
	下面语句使上传控件显示在上面ID为uploadContenter的Div标签
	提交URL为upload.asp保存目录为upload
	表单提交到上面name属性为AnUploader的iframe里面；
	========================================*/
	
	var AjaxUp=new AjaxProcesser("uploadContenter");
	
	//设置提交到的iframe名称
	AjaxUp.target="AnUploader";  
	
	//上传处理页面,尽量不要更改
	AjaxUp.url="/common/ajaxupload/manageupload.asp"; 
	
	//保存目录
	AjaxUp.savePath="/uploadfiles";
	
	var contenter=document.getElementById("uploadContenter");
	contenter.style.display="none"; //隐藏容器
	
	function showUploader(objID,srcElement){
		AjaxUp.reset();  //重置上传控件
		var errID="sperr";
		contenter.style.display="block"; //显示容器
		var ps=_.abs(srcElement);//作用--获取指定标签的绝对坐标,目的是为了把上传控件定位到按钮下面
		contenter.style.top=(ps.y + 30) + "px";  
		contenter.style.left=(ps.x-180) + "px";
		//上传成功时要执行的程序
		AjaxUp.succeed=function(files){
		    var fujian=document.getElementById(objID);
			fujian.value=AjaxUp.savePath + "/" + files[0].name;  //因为上传控件就只上传一个 文件，这里索引是0
			contenter.style.display="none";			
			document.getElementById(errID).style.display="none";
		}
		//上传失败时要执行的程序
		AjaxUp.faild=function(msg){document.getElementById(errID).innerText="文件上传失败，原因："+msg;contenter.style.display="none";document.getElementById(errID).style.display="block"}
	}
</script>

   <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">开户支行：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="area" type="text" value="<%=area%>" size="50" dataType="LimitB" min="4" max="30" msg="开户支行号必须为4-30个字以内">     
      <span class="red">如：**市**分行**支行</span></td>
   </tr> 
    <tr> 
	   <td width="14%" align="right" bgcolor="#FFFFFF">是否显示：</td>
      <td align="left" bgcolor="#FFFFFF"><input name="flag" type="radio" value="1" <%if flag then response.Write("checked")%>>是 <input name="flag" type="radio" value="0" <%if not flag then response.Write("checked")%>>否    是否推荐：<input name="default" type="radio" value="1" <%if default then response.Write("checked")%>>是 <input name="default" type="radio" value="0" <%if not default then response.Write("checked")%>>否     
     </td>
   </tr>   
   <tr> 
	   <td align="right" bgcolor="#FFFFFF">&nbsp;</td>
	   <td bgcolor="#FFFFFF">
			<input name="add" type="submit"  value="提交">
			<input type="reset" name="Submit2" value="重置">
		</td>
   </tr>
   </form>
</table>
<%
end if

call closeconn()
%>
</body>
</html>