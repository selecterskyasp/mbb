﻿<!--#include file="../top.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link rel="stylesheet" type="text/css" href="../style/css.css">
<script language="javascript" src="<%=webvirtual%>/js/validator.js"></script>
</head>

<body>
<%
tqxarr=getqx(8,qxarr)
if tqxarr(0)<>1 then showqxmsg
dim action
action=request.QueryString("action")
if action="save" then
	if tqxarr(1)<>1 then showqxmsg
	tbody=chr(60)&"%"&vbcrlf
	tbody=tbody&""&vbcrlf
	tbody=tbody&"weburl			= """&weburl&""" '网站地址"&vbcrlf	
	tbody=tbody&"sweburl			= """&sweburl&""" '网站短地址"&vbcrlf
	tbody=tbody&"webname			= """&webname&""" '网站名称"&vbcrlf
	tbody=tbody&"seller_email			= """&checkstr(request.Form("seller_email"))&""" '请设置成您自己的支付宝帐户"&vbcrlf	
	tbody=tbody&"partner			= """&checkstr(request.Form("partner"))&""" '请设置成您自己的支付宝帐户"&vbcrlf
	tbody=tbody&"key			= """&checkstr(request.Form("key"))&""" '支付宝的安全校验码"&vbcrlf
	tbody=tbody&"seller_name			= """&checkstr(request.Form("seller_name"))&""" '支付宝的安全校验码"&vbcrlf
	tbody=tbody&""&vbcrlf
	tbody=tbody&"%"&chr(62)&vbcrlf
	
	tarr=SaveToFileauto(tbody,"/otherpay/alipay/config.asp","gb2312")
	tarr2=SaveToFileauto(tbody,"/otherpay/alipay/configutf8.asp","utf-8")
	if tarr(0)<>0 then
	call alert("保存文件出错，错误ID号："&tarr(0)&" 错误信息："&tarr(1)&" 当前的操作没有保存","",1)
	end if
	erase tarr
	tbody=""	
	
	response.Redirect("alipay.asp")
end if
%>
<!--#include virtual="/otherpay/alipay/configutf8.asp"-->
<table width="90%" height="0" border="0" align="center" cellpadding="5" cellspacing="1" bgcolor="#cccccc">
<form name="form1" method="post" action="alipay.asp?action=save" onSubmit="return Validator.Validate(this,2);">
<tr bgcolor="#efefef">
    <td colspan=2>　<strong>支付宝接口配置</strong><br />
      <font color="#FF0000">注：登陆 www.alipay.com 后, 点商家服务,可以看到支付宝安全校验码和合作id,在导航栏的下面</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="21%" align="center">签约支付宝账号:</td>
	<td width="79%" align="left"><input type="text" name="seller_email" size=60 value="<%=seller_email%>" dataType="Email"  msg="请填写有效的邮箱！">&nbsp;<font color="#FF0000">请设置成您自己的支付宝帐户</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="21%" align="center">签约商家ID:</td>
	<td width="79%" align="left"><input type="text" name="partner" size=60 value="<%=partner%>" dataType="Number" min="4" max="50" msg="商家ID是一串大于15位的数字（一般是16位）">&nbsp;<font color="#FF0000">支付宝的账户的合作者身份ID</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="21%" align="center">安全校验码：</td>
	<td width="79%" align="left"><input type="text" name="key" size=60 value="<%=key%>" dataType="LimitB" min="32" max="64" msg="安全校验码应该是至少32位的密文">&nbsp;<font color="#FF0000">支付宝的安全校验码</font></td>
</tr>
<tr bgcolor="#ffffff">
    <td width="21%" align="center">签约商家真实姓名:</td>
	<td width="79%" align="left"><input type="text" name="seller_name" size=60 value="<%=seller_name%>" dataType="LimitB" min="2" max="20" msg="请正确填写商家的真实姓名"></td>
</tr>
<tr bgcolor="#ffffff" align="center">
    <td height="30" colspan=2><input type="submit" name="Submit" value="提 交" class=input1><div style="color:#FF0000; display:none" id="sperr"></div></td>
</tr>
</form>
<%
call closeconn()%>
</table>
</body>
</html>
