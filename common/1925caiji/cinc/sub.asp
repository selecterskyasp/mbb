<%
'获取列表 返回地址数组
'smode为1是数组的最后一列为产品的页面总数
function getPageList(byval surl,byval smode)
dim tstr,tbody
dim tarr,tpos
dim tmpurl,i
const thttp="http://www.1925.cn/"
i=0
tbody=getHttpPage(surl)
tstr=split(getBody(tbody,1,"<td height=""136"" colspan=""2""><div align=""center""><a href=""","""",false,false),"|")
tmpurl=tstr(0)
tpos=tstr(1)
erase tstr
redim tarr(1,i)
if tmpurl<>"$False$" then
tarr(0,i)=thttp&tmpurl
tstr=split(getBody(tbody,tpos,"<img src=""","""",false,false),"|")
tarr(1,i)=thttp&tstr(0)
erase tstr
else
tarr(0,i)="$False$"
tarr(1,i)=""
end if
do while tmpurl<>"$False$" and i<100
tstr=split(getBody(tbody,tpos,"<td height=""136"" colspan=""2""><div align=""center""><a href=""","""",false,false),"|")
tmpurl=tstr(0)
tpos=tstr(1)
erase tstr
i=i+1
redim preserve tarr(1,i)
if tmpurl<>"$False$" then
tarr(0,i)=thttp&tmpurl
tstr=split(getBody(tbody,tpos,"<img src=""","""",false,false),"|")
tarr(1,i)=thttp&tstr(0)
erase tstr
else
tarr(0,i)="$False$"
tarr(1,i)=""
end if
loop
if smode=1 then
	i=i+1
	redim preserve tarr(1,i)
	tstr=split(getBody(tbody,1,"<td height=""24"" align=""center"">共<font color=""#FF0000"">","</font>页",false,false),"|")
	tmpurl=tstr(0)
	erase tstr
	if not chkrequest(tmpurl) then tarr(0,i)=0 else tarr(0,i)=tmpurl
end if
getPageList=tarr
erase tarr
tbody=""
end function

'获取产品参数
function getProductDetail(byVal surl)
dim tstr,tbody
dim tpiclist,pichtml,tpos,i
dim tarr(14)
tbody=getHttpPage(surl)

'名称
tstr=split(getBody(tbody,1,"[商品名称]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(0)=getinnertext(tstr(0)) else tarr(0)=""
erase tstr

'关键字
tstr=split(getBody(tbody,1,"name=""keywords"" content=""","""",false,false),"|")
if tstr(0)<>"$False$" then tarr(1)=getinnertext(tstr(0)) else tarr(1)=""
erase tstr

'描述
tstr=split(getBody(tbody,1,"name=""description"" content=""","""",false,false),"|")
if tstr(0)<>"$False$" then tarr(2)=getinnertext(tstr(0)) else tarr(2)=""
erase tstr

'图片 多个图片以|分隔
const thttp="http://www.15wk.com/"
tstr=split(getBody(tbody,1,"<table width=""500"" border=""0"" cellspacing=""0"" cellpadding=""0"">","</table>",false,false),"|")
if tstr(0)<>"$False$" then 
	pichtml=tstr(0)
	erase tstr
	tstr=split(getBody(pichtml,1,"src=""","""",false,false),"|")
	i=0
	tpiclist=""
	do while tstr(0)<>"$False$" and i<7
		i=i+1
		if tpiclist="" then 
			tpiclist=thttp&tstr(0) 
		else 
			if instr(tpiclist,tstr(0))=0 then tpiclist=tpiclist&"|"&thttp&tstr(0)
		end if
		tpos=tstr(1)
		erase tstr
		tstr=split(getBody(pichtml,tpos,"src=""","""",false,false),"|")
	loop
end if
erase tstr
tarr(3)=tpiclist

'编号
tstr=split(getBody(tbody,1,"[商品编号]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(4)="wk"&getinnertext(tstr(0)) else tarr(4)=""
erase tstr

'单位
tstr=split(getBody(tbody,1,"[商品单位]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(5)=getinnertext(tstr(0)) else tarr(5)=""
erase tstr

'尺寸
tstr=split(getBody(tbody,1,"[商品尺寸]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(6)=getinnertext(tstr(0)) else tarr(6)=""
erase tstr

'重量
tstr=split(getBody(tbody,1,"[单个重量]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(7)=getinnertext(tstr(0)) else tarr(7)=""
erase tstr

'材质
tstr=split(getBody(tbody,1,"[商品材质]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(8)=getinnertext(tstr(0)) else tarr(8)=""
erase tstr

'产地
tstr=split(getBody(tbody,1,"[商品产地]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(9)=getinnertext(tstr(0)) else tarr(9)=""
erase tstr

'装箱数量
tstr=split(getBody(tbody,1,"[装箱数量]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(10)=getinnertext(tstr(0)) else tarr(10)=""
erase tstr

'毛重净重
tstr=split(getBody(tbody,1,"[毛重净重]</font></div></td>","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(11)=getinnertext(tstr(0)) else tarr(11)=""
erase tstr

'批发价
tstr=split(getBody(tbody,1,"批发价:￥<font color=""#0000FF"">","</font>",false,false),"|")
if chkrequest(tstr(0)) then tarr(12)=tstr(0) else tarr(12)=0
erase tstr

'现货数量
tstr=split(getBody(tbody,1,"[现货数量]</font></div></td>","<img src=""",false,false),"|")
if tstr(0)<>"$False$" then tarr(13)=getinnertext(tstr(0)) else tarr(13)=0
if not chkrequest(tarr(13)) then tarr(13)=0
erase tstr

'商品简介
tstr=split(getBody(tbody,1,"<td class=""12lan"">","</td>",false,false),"|")
if tstr(0)<>"$False$" then tarr(14)=replace_text(tstr(0)) else tarr(14)=""
erase tstr

getProductDetail=tarr
tbody="":erase tarr
end function

'检查产品有效性 如果有效返回ok，否则返回错误信息
function chkValidPro(byVal proArr)
dim errmsg,finderr
errmsg="":finderr=false
if ubound(proArr)<>14 then
	chkValidPro="产品数组获取失败"
	exit function
end if
if ChkNull(proArr(0),2) then
	finderr=true:errmsg=errmsg&","&"产品名称获取失败"
end if
if ChkNull(proArr(3),5) then
	finderr=true:errmsg=errmsg&","&"产品图片获取失败"
end if
if ChkNull(proArr(4),1) then
	finderr=true:errmsg=errmsg&","&"产品编号获取失败"
end if
if ChkNull(proArr(5),1) then
	finderr=true:errmsg=errmsg&","&"产品单位获取失败"
end if
if ChkNull(proArr(14),5) then
	finderr=true:errmsg=errmsg&","&"产品介绍获取失败"
end if
if finderr then chkValidPro=right(errmsg,len(errmsg)-1) else chkValidPro="ok"
erase proArr
end function

'检查产品是否存在
function chkproExists(byVal pname,byVal phh)
sql="select top 1 id from products where [name]='"&pname&"' or hh='"&phh&"'"
dim tarr
tarr=getDBValue(sql,1)
if tarr(0)<>0 then
chkproExists=tarr(1)
else
chkproExists=0
end if
erase tarr
end function
%>