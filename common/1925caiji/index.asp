﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%Option Explicit
Response.Buffer=TRUE
server.ScriptTimeout=99999
%>
<!--#include file="cinc/function.asp"-->
<!--#include file="cinc/sub.asp"-->
<!--#include file="cinc/conn.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>1925.cn采集插件 v1.00 by selectersky</title>
<script language="javascript" src="/js/validator.js"></script>
<script type="text/javascript" src="/js/jquery.js"></script>
<script type="text/javascript" src="/js/extend.js"></script>
<script language="JavaScript" type="text/JavaScript">
$(document).ready(function() {
		var selects1 = new linkSelect({
		el: 'sortpath',
	url: '/xml/class.xml',
			root: 'list32',
			field: 'list',
			text: 'Text',
			value: 'ID'
			});
		});
</script>
<style type="text/css">
body{margin:0;padding:0;border:0px none;background:#fff;color:#000000;font-size:12px;font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif;line-height:180%;}	
	select,input{font-size:12px;color:#000;}
	h1{margin:0;font-size:14px;color:#1b49ac; margin-top:15px}	
	A:link,A:visited,A:active{ text-decoration: none;color: #111;}
	input,textarea{
	border:1px solid #e5e5e5;	
	margin: 1px 0px 4px 0px;	
	color: #333;	
	padding:2px 0px 0px 3px;	
}
	td{ height:22px}
textarea:focus,input:focus{	
	border: 1px solid #FFFF00;
}
.tbHeader { height: 22px; background-color:#CEDFEF; text-align:center; }
</style>
</head>
<body>
<%
dim act
act=request.QueryString("act")
select case act
case "1"
call act1()
case "2"
call act2()
case else
call act1()
end select
%>

<%sub act1()%>
<div>
<h1>第一步 输入范围</h1>
<form action="index.asp?act=2" method="post" onSubmit="return Validator.Validate(this,2);">
  <table width="600" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td  align="right">输入产品分类网址：</td>
      <td><input name="furl" type="text" id="furl" size="60" maxlength="100" dataType="Url" msg="输了正确的网址"/>
        <br />
        (如:http://www.1925.cn/925/showmore8_1.html)      </td>
    </tr>
  <tr>
      <td  align="right">输入采集起始页码：</td>
      <td>从<input name="furlnum" type="text"  value="1" id="furlnum" size="60" maxlength="100" dataType="Integer" msg="输入整数" /><br />至<input name="turlnum" type="text" id="turlnum" size="60" maxlength="100" value="0" dataType="Integer" msg="输入整数"/>
        <br />
        (注:输入0为采集从当前页面开始之后的所有页面的产品)      </td>
    </tr>
	<tr>
      <td  align="right">选择采集产品分类：</td>
      <td>
	  <div id="sortpath"></div>
        </td>
    </tr>
   	<tr>
      <td colspan="2"  align="center"><label>
        <input type="submit" name="Submit" value="开始采集" />
        <input type="reset" name="Submit2" value="重新填写" />
      </label></td>    
    </tr>
  </table>
  </form> 
</div>
<%end sub%>

<%sub act2()
dim prourl
dim furl,furlnum,turlnum,sortpath,tmpurl
dim i,j
dim proListArr,proArr,isvalidPro
dim ttpage
dim ttpro,usepro,tproNum
dim picArr,picList,bigpic,smallpic,picExt,picname,issave,proisexist
furl=request.Form("furl")
furlnum=cint(request.Form("furlnum"))
turlnum=cint(request.Form("turlnum"))
sortpath=replace(request.Form("sortpath")," ","")
'删除网站里面的分页
furl=delUrlpara(furl,"page")
if not chkUrl(furl) then
	showErrmsg "网址不正确!",""
end if
if furlnum<1 then
	showErrmsg "起始页不能少于1!",""
end if
if not chkpara(sortpath) then
	showErrmsg "请选择产品分类!",""
end if
response.Write("<font color=red>正在获取第 <font color=red>"&furlnum&"</font>页面信息...</font>")
response.Flush()
tmpurl=furl&"page="&furlnum
proListArr=getPageList(tmpurl,1)
response.Write("<font color=green>完成</font>&nbsp;<a href="""&tmpurl&""" target=""_blank"">查看</a><br>")
response.Flush()
ttpage=cint(proListArr(0,ubound(proListArr,2)))
tproNum=ubound(proListArr,2)-1
if ttpage=0 or tproNum<1 then
	showErrmsg "当前分类下没有找到产品!",""
end if
if turlnum=0 or turlnum>ttpage then turlnum=ttpage
ttpro=0:usepro=0
i=furlnum
do while  i<=turlnum
	response.Write("正在处理第 <font color=red>"&i&"</font> 页的产品信息，当前页面产品数量 <font color=red>"&tproNum&"</font>....<h1>")
	response.Flush()
	for j=0 to tproNum-1
		ttpro=ttpro+1
		response.Write("正在处理第 <font color=red>"&j+1&"</font> 个产品....")
		response.Flush()
		proArr=getProductDetail(proListArr(0,j))
		isvalidPro=chkValidPro(proArr)
		if isvalidPro<>"ok"  then
			response.Write("<font color=red>产品参数错误,原因:<br>"&isvalidPro&" 已放弃添加!</font>&nbsp;<a href="""&proListArr(0,j)&""" target=""_blank"">查看此产品</a><br>")
		else
			proisexist=clng(chkproExists(proArr(0),proArr(4)))
			if proisexist=0  then
			picList=proArr(3)
			picArr=split(picList,"|")
			bigpic=picArr(0)
			erase picArr
			smallpic=proListArr(1,j)
			picExt=getExt(bigpic)
			picname="/uploadpic/"&makeFilename()
			issave=SaveRemoteFile(picname&"big"&picExt,bigpic)
			if issave then
				if chknull(smallpic,5) then issave=false else issave=SaveRemoteFile(picname&"small"&picExt,smallpic)			
			if issave then
			set rs=server.CreateObject("adodb.recordset")
			rs.open "select * from products",conn,1,3
			rs.addnew
			rs("adduser")	= "admin"
			rs("name")		= proArr(0)		'名称
			rs("keywords")	= proArr(1)	'关键字
			rs("smallpic")	= picname&"small"&picExt	'小图
			rs("bigpic1")	= picname&"big"&picExt		'大图			
			rs("bid")		= 62			'子ID
			rs("sortpath")	= sortpath		'商品类别路径
			rs("content")	= proarr(14)	'详细说明
			rs("ms")		= proarr(2)		'商品描述
			rs("sxjf")		= 	0			'所属积分
			rs("price1")	= proarr(12)	'价格
			rs("price2")	= cdbl(proarr(12))*1.8	'市场价格
			rs("zheke")		= 1		'折扣
			rs("ks")		= proarr(6)		'款式(尺寸)
			rs("zd")		= proarr(8)		'主要质地(材质)
			rs("tuai")		= ""		'图案(装箱规格)
			rs("fg")		= proarr(7)		'风格(毛净重)
			rs("serice")	= ""	'适合季节(体积)
			rs("hh")		= proarr(4)		'货号
			rs("flag")		= 0
			rs("p_key")		= 0		'是否推荐
			rs("p_key1")	= 5	' 推荐度
			rs("keytime")	= now()
			rs("createdon")	= now()
			rs("ljxs")		= 0
			rs("jl")		= proarr(5)
			rs("zhuangxiao")= proarr(10)
			rs("hits")		= 0
			rs("min")		= 1
			rs("max")		= proarr(13)
			rs("color")		= "0"
			rs("size")		= "0"
			rs("baozhuang")	= ""
			rs("key1")		= 0
			rs.update
			set rs=nothing			
			response.Write("<font color=green>产品已经成功添加!</font><br>")
			usepro=usepro+1			
			else
				response.Write("<font color=red>产品小图保存失败，放弃添加!</font>&nbsp;<a href="""&proListArr(0,j)&""" target=""_blank"">查看此产品</a>&nbsp;<a href="""&smallpic&""" target=""_blank"">图片地址</a><br>")
			end if
			else
				response.Write("<font color=red>产品大图保存失败，放弃添加!</font>&nbsp;<a href="""&proListArr(0,j)&""" target=""_blank"">查看此产品</a>&nbsp;<a href="""&picList&""" target=""_blank"">图片地址</a><br>")
			end if
			else
				response.Write("<font color=red>产品已经存在，放弃添加!</font>&nbsp;<a href="""&proListArr(0,j)&""" target=""_blank"">查看此产品</a>&nbsp;已存在产品库ID号："&proisexist&"<br>")
			end if
		end if
		erase proarr
		response.Flush()
	next
	i=i+1
	if i<=turlnum then
		tmpurl=furl&"page="&i
		response.Write("正在获取第 <font color=red>"&i&"</font> 页面信息...")
		response.Flush()
		erase proListArr
		proListArr=getPageList(tmpurl,0)
		tproNum=ubound(proListArr,2)
		response.Write("<font color=green>完成</font>&nbsp;<a href="""&tmpurl&""" target=""_blank"">查看</a><br>")
		response.Flush()
	end if
loop
call closeconn()
response.Write("<font color=green>所有操作已经完成!</font>&nbsp;总共找到产品数:<font color=red>"&ttpro&"</font>&nbsp;其中已经加入数据库产品数:<font color=red>"&usepro&"</font>")
response.Flush()
end sub%>
</body>
</html>
