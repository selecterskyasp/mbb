<%
'将产品添加到购物车数据库 0为修改 1为添加 可累加
function addDbShopcart(sid,snum,scolor,ssize,smemo,smode)
dim tmparr,uid
dim tmparr2,i
'检查用户是否存在
uid=getUserLoginInfo()(0)
if not chkrequest(uid) then	
	addDbShopcart=1
	exit function
end if
sql="select top 1 id from [user] where id="&uid
tmparr=getDBvalue(sql,1)
if tmparr(0)=0 then
	addDbShopcart=1
	exit function
end if
erase tmparr

'检查产品是否存在
if not chkrequest(sid) then
	addDbShopcart=2
	exit function
end if
sql="select top 1 id,min,max,color,size from products where id="&sid
tmparr=getDBvalue(sql,5)
if tmparr(0)=0 then
	addDbShopcart=2
	erase tmparr
	exit function
end if
'产品数量是否正确
if not chkrequest(snum) then
	addDbShopcart=3
	erase tmparr
	exit function	
end if
snum=clng(snum)
min=clng(tmparr(2))
max=clng(tmparr(3))
if snum<min or (snum>max and max<>-1) then
	addDbShopcart=3
	erase tmparr
	exit function	
end if
if not chkPara(scolor) then
	addDbShopcart=4
	erase tmparr
	exit function	
end if
tmparr2=split(scolor,",")
for i=0 to ubound(tmparr2)
	if instr(tmparr(4),tmparr2(i))=0 then
		erase tmparr:erase tmparr2
		addDbShopcart=4
		exit function
	end if
next
erase tmparr2
if not chkPara(ssize) then
	addDbShopcart=5
	erase tmparr
	exit function		
end if
tmparr2=split(ssize,",")
for i=0 to ubound(tmparr2)
	if instr(tmparr(5),tmparr2(i))=0 then
		erase tmparr:erase tmparr2
		addDbShopcart=5
		exit function
	end if
next
erase tmparr
erase tmparr2
sql="select top 1 pnum from shopcart where pid="&sid&" and userid="&uid
tmparr=getDBvalue(sql,1)
if tmparr(0)=0 then
	sql="insert into shopcart (pid,pcolor,psize,pmemo,pnum,createdon,userid,buymethod)values("&sid&",'"&scolor&"','"&ssize&"','"&smemo&"',"&snum&",'"&now()&"',"&uid&",0)"
	call setDBvalue(sql)
else	
	curNum=clng(tmparr(1))
	if smode=1 then
	if (curNum+snum)>max and max<>-1 then
		addDbShopcart=3
		erase tmparr
		exit function
	end if
	snum=snum+curNum	
	end if
	sql="update shopcart set pcolor='"&scolor&"',psize='"&ssize&"',pmemo='"&smemo&"',pnum="&snum&" where pid="&sid&" and userid="&uid
	call setDBvalue(sql)
end if
sql="select count(id) from shopcart where userid="&uid
pronum=getdbvalue(sql,1)(1)
call writecookies("","buyproductnum",pronum)
addDbShopcart=0
end function

'产品保存格式 产品ID:产品颜色(以逗号分开):产品数量| 产品ID:产品颜色(以逗号分开):产品数量
function addTmpShopcart(sid,snum,scolor,ssize)
if not chkrequest(sid) then
	addTmpShopcart=1:exit function
end if
if not chkrequest(snum) then
	addTmpShopcart=2:exit function
end if
if not chkPara(scolor) then
	addTmpShopcart=3:exit function
end if
if not chkPara(ssize) then
	addTmpShopcart=4:exit function
end if
dim tmparr,tmparr2,st,i
dim isfind
isfind=false
'st=session("shopcart")
st=readcookies("","shopcart")
if chkNull(st,7) then'如果当前购物车为空
	st=sid&":"&snum&":"&scolor&":"&ssize
else
	tmparr=split(st,"|")
	'查找产品是否存在,如果找到了，刚更新购物车
	for i=0 to ubound(tmparr)	
		tmparr2=split(tmparr(i),":")				
		if clng(tmparr2(0))=clng(sid) then					
			tmparr(i)=sid&":"&snum&":"&scolor&":"&ssize	
			isfind=true
			exit for		
		end if
		erase tmparr2
	next 
	if isfind then'如果找到了，使更新购物车生效
		st=""
		for i=0 to ubound(tmparr)
			if st="" then st=tmparr(i) else st=st&"|"&tmparr(i)			
		next		
	else'如果没有找到，则添加在前面
		st=sid&":"&snum&":"&scolor&":"&ssize&"|"&st
	end if	
end if
call writecookies("","shopcart",st)
'session("shopcart")=st
addTmpShopcart=0
pronum=ubound(split(st,"|"))+1
call writecookies("","buyproductnum",pronum)
end function

'删除购物车数据库中的产品
function delDbShopcart(sid)
dim tmparr,uid
uid=getUserLoginInfo()(0)
if not chkrequest(sid) or not chkrequest(uid) then
	delDbShopcart=1
	exit function
end if
sql="select top 1 id from shopcart where pid="&sid&" and userid="&uid
if getDBvalue(sql,1)(0)<>0 then
	sql="delete shopcart where pid="&sid&" and userid="&uid
	call setDBvalue(sql)
	delDbShopcart=0
else
	delDbShopcart=2
end if
end function

'产品保存格式 产品ID:产品数量:产品颜色(以逗号分开):产品尺寸(以逗号分开) 多个产品以|分割
function delTmpShopcart(sid)
dim tmparr,tmparr2,st,i,j
dim isfind
isfind=false
'st=session("shopcart")
st=readcookies("","shopcart")
if not chkrequest(sid) then
	delTmpShopcart=1
	exit function	
end if
if not chkNull(st,7) then'如果当前购物车不为空
	tmparr=split(st,"|")
	for i=0 to ubound(tmparr)	
		tmparr2=split(tmparr(i),":")				
		if clng(tmparr2(0))=clng(sid) then	
			isfind=true
			exit for		
		end if
		erase tmparr2
	next 
	
	if isfind then'如果找到了，刚更新购物
		st=""		
		for j=0 to ubound(tmparr)
			if i<>j  then
				if st="" then st=tmparr(j) else st=st&"|"&tmparr(j)				
			end if
		next	
		call writecookies("","shopcart",st)	
		'session("shopcart")=st
		delTmpShopcart=0		
	else
		delTmpShopcart=2		
	end if
else
	delTmpShopcart=2
end if
end function
%>