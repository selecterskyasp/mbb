﻿<!--#include virtual="/inc/top.asp"-->
<%
dim userid
if not isLogin() then
	call closeconn()
	response.Redirect("/reg")	
else
	userid=clng(getUserLoginInfo()(0))
	sql="select top 1 [type],sdate,edate from [user] where id="&userid
	tarr=getdbvalue(sql,3)
	sdate=tarr(2):edate=tarr(3):utype=cint(tarr(1))
	'vip会员检查是否过期
	if utype<>1 then
		if datediff("d",sdate,edate)<0 then
			sql="update [user] set type=1 where id="&userid
			call setdbvalue(sql)
			response.Cookies("isLogin")("type")=1
		end if
	end if
	erase tarr
end if
dim area,areaold,address,postcode,truename,tel,mobile,qqmsn,email,shfs,message,addressID
addressID=request.Form("addressID")
area=replace(request.Form("area")," ","")
areaold=replace(request.Form("areaold")," ","")
address=checkstr(request.Form("address"))
postcode=checkstr(request.Form("postcode"))
truename=checkstr(request.Form("truename"))
tel=checkstr(request.Form("tel"))
mobile=checkstr(request.Form("mobile"))
qqmsn=checkstr(request.Form("qqmsn"))
email=checkstr(request.Form("email"))
shId=checkstr(request.Form("shfs"))
message=checkstr(request.Form("message"))
buyMethod=request.Form("buyMethod")

dim tarr,tarr2,i
dim taddr,yunshu,yunfei
'------------数据判断------------------
dim finderr,errmsg
finderr=false:errmsg=""
if buyMethod<>"0" and buyMethod<>"1" then
	finderr=true
	errmsg=errmsg&"<li>请选择购买方式</li>"
end if
buyMethod=cint(buyMethod)
if not chkNumber(addressID) then
	finderr=true
	errmsg=errmsg&"<li>请选择收货地址</li>"
else
	addressID=cint(addressID)
	if addressID>0 then
		sql="select top 1 id from address where id="&addressID&" and userid="&userid
		tarr=getdbvalue(sql,1)
		if tarr(0)=0 then
			finderr=true
			errmsg=errmsg&"<li>您选择的收货地址在您的地址薄中并不存在，请确认</li>"
		end if
	else
		if addrssID<>0 and addressID<>-1 then
			finderr=true
			errmsg=errmsg&"<li>请选择正确的收货地址</li>"
		end if
	end if
end if
'其它联系方式
if addressID=-1 then
	if not chkPara(area) then		
			finderr=true
			errmsg=errmsg&"<li>请选择城市地区</li>"			
	end if
	
	if not chkRange(address,2,100) then
		finderr=true
		errmsg=errmsg&"<li>请输入您所在的街道地址(不需要输入城市地区2-100字)</li>"		
	end if
	
	if not chkRange(postcode,2,10) then
		finderr=true
		errmsg=errmsg&"<li>请输入正确的邮政编码</li>"
	end if
	
	if not chkRange(truename,2,20) then
		finderr=true
		errmsg=errmsg&"<li>请正确输入您的真实姓名</li>"
	end if
	
	if not chkRange(tel,6,20) and not chkRange(mobile,11,20) then
		finderr=true
		errmsg=errmsg&"<li>电话号码(6-20字符)和手机(11-20字符)必须填写其中一个</li>"
	end if
end if

if not chkrequest(shId) then
	finderr=true
	errmsg=errmsg&"<li>请选择货运方式</li>"
else
	shId=cint(shId)
	sql="select top 1 name,number from class where id="&shId
	tarr=getDBvalue(sql,2)
	if tarr(0)=0 then
		finderr=true
		errmsg=errmsg&"<li>请选择发货方式</li>"	
	end if
	erase tarr
end if

if not chkNull(message,1) then
	if len(message)>300 then
		finderr=true
		errmsg=errmsg&"<li>备注信息请控制在300字以内</li>"	
	end if
end if

'积分购买
if paymethod=1 then
	ttjifen=0
	sql="select pid from shopcart where flag=0 and userid="&userid
	tarr=getdbvaluelist(sql)
	if clng(tarr(0,0))<>0 then
		for i=0 to ubound(tarr,2)
			sql="select jf,key2 from products where id="&tarr(0,0)
			tarr2=getdbvalue(sql,1)			
			if tarr2(0)<>0 then
				if not tarr2(2) then
					finderr=true
					errmsg=errmsg&"<li>购物车里产品ID为"&tarr(0,0)&"的产品不属于积分产品，请确认</li>"	
					exit for
				end if
				ttjifen=ttjifen+clng(tarr2(1))
			else
				finderr=true
				errmsg=errmsg&"<li>购物车里产品ID为"&tarr(0,0)&"的产品不存在，请确认或联系我们</li>"	
				exit for			
			end if			
			erase tarr2
		next
		sql="select jifen from [user] where id="&userid
		tarr2=getdbvalue(sql,1)
		if ttjifen>clng(tarr2(1)) then
			errmsg=errmsg&"<li>您的积分不够!</li>"	
		end if
		erase tarr2
	else
		finderr=true
		errmsg=errmsg&"<li>购物车里面没有产品</li>"	
	end if
	erase tarr
end if

if finderr then
	showmsg errmsg,"",0
end if
dim dnumber,comm
dim errnum
'产生18位的定单号
sql="select top 1 number from shopcart where len(number)>1 and flag=1 and userid="&userid
tarr=getdbvalue(sql,1)
if tarr(0)<>0 then dnumber=tarr(1)
erase tarr
if not chkDingdanNumber(dnumber) then dnumber=MakeDingdanNumber()
set comm=server.CreateObject("adodb.command")
with comm
.activeconnection=conn
.commandtype=4
.commandtext="createDingdan"
.prepared=true
.parameters.append .createparameter("@return",16,4)
.parameters.append .createparameter("@number",200,1,20,dnumber)
.parameters.append .createparameter("@userid",20,1,4,userid)
.parameters.append .createparameter("@area",200,1,150,area)
.parameters.append .createparameter("@address",200,1,100,address)
.parameters.append .createparameter("@postcode",200,1,10,postcode)
.parameters.append .createparameter("@truename",200,1,10,truename)
.parameters.append .createparameter("@tel",200,1,20,tel)
.parameters.append .createparameter("@mobile",200,1,20,mobile)
.parameters.append .createparameter("@addressID",20,1,4,addressID)
.parameters.append .createparameter("@shID",3,1,4,shID)
.parameters.append .createparameter("@message",200,1,300,message)
.parameters.append .createparameter("@buyMethod",16,1,2,buyMethod)
.execute()
end with
errnum=cint(comm(0))
set comm=nothing
dim arr(13)
arr(0)="成功"
arr(1)="购物车里面没有产品"
arr(2)="请选择发货方式"
arr(3)="购物车里面没有可用产品"
arr(4)="对应的会员等级的产品价格不存在"
arr(5)="添加订单到数据库失败"
arr(6)=""
arr(7)="用户不存在"
arr(8)="积分购买必须每一个产品为积分产品"
arr(9)="用户等级不存在"
arr(10)="您的批发金额不够 不能少于"&webqipi&" 元"
arr(11)="收货地址不存在"
arr(12)="添加到收货地址薄失败"
arr(13)="订单号重复，请到购物车重新下单"
if errnum<>0 then
	showmsg "生成订单失败，错误ID号："&errnum&"，可能原因："&arr(errnum)&"，请确认您的操作!","",0	
end if
erase arr
call setCartCookies(0,0)	
sql="select id from dingdan where number='"&dnumber&"' and userid="&userid
tarr=getdbvalue(sql,1)
if tarr(0)=0 then alert "无法找到已经生成的订单，可能订单生成失败","",1
id=tarr(1)
erase tarr
call closeconn()
response.Redirect("/products/pay.asp?id="&id)
response.End()
%>
