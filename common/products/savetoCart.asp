<!--#include file="../inc/conn.asp"-->
<!--#include file="../inc/safe_info.asp"-->
<!--#include file="../inc/myfun.asp"-->
<!--#include file="shopcartSub.asp"-->
<%
id=request.QueryString("id")
color=request.QueryString("color")
num=request.QueryString("num")
size=request.QueryString("size")

if not chkrequest(id) then
	showmsg "产品参数丢失","",0
end if

dim tarr,errnum
sql="select top 1 color,size,min,max from  products where flag=1 and id="&id
tarr=getDBvalue(sql,4)
if tarr(0)=0 then
	showmsg "产品不存在或者已经下架","",0
end if
if clng(tarr(4))<clng(tarr(3)) and clng(tarr(4))<>-1 then
	showmsg "产品库存不足，不能购买","",0
end if
if not chkPara(color) then
	color=tarr(1)
end if
if not chkPara(size) then
	size=tarr(2)
end if
if not chkrequest(num) then
	num=tarr(3)
else
	num=clng(num)
	if (num<clng(tarr(3)) or num>clng(tarr(4))) and clng(tarr(4))<>-1 then
		showmsg "购买产品的数量必须在 "&tarr(3)&" 和 "&tarr(4)&" 之间","",0
	end if
end if
erase tarr


if chklogin("isLogin") then
	errnum=addDbShopcart(id,num,color,size,"",1)
	if errnum<>0 then
		showmsg "添加产品失败，错误ID号："&errnum,"",0
	else
		call closeconn()
		response.Redirect("my_cart.asp")
	end if
else
	errnum=addTmpShopcart(id,num,color,size)
	if errnum<>0 then
		showmsg "添加产品失败，错误ID号："&errnum,"",0
	else
		call closeconn()		
		response.Redirect("my_cart.asp")	
	end if
end if
%>
