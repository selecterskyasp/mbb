<!--#include virtual="/otherpay/alipay/config.asp"-->
<%
	'功能：设置商户有关信息及返回路径
	'接口名称：实物批发接口
	'版本：2.0
	'日期：2008-10-24
	'作者：支付宝公司销售部技术支持团队
	'联系：0571-26888888
	'版权：支付宝公司
      
	mainname        = webname   '网站名称 或商户名称
	show_url        = weburl    '网站的网址   比如:www.alipay.com 
	s_site_url		= sweburl 
	'seller_email	= "xibao16488@qq.com"	'请填写签约支付宝账号
	'partner			= "2088002936302304"	'填写签约支付宝账号对应的partnerID
	'key			    = "w2ljvx9szju9iqaykwc35d5et8hzanw9"	'填写签约账号对应的安全校验码
	return_url		= weburl&"/pay/alipay/success.asp"	'付完款后跳转的页面 要用 http://格式的完整路径
	'如果使用了return_Alipay_Notify.asp，请在return_Alipay_Notify.asp文件中添加相应的合作身份者ID和安全校验码 
	logistics_fee		= "0.00"				'物流配送费用
	logistics_payment	= "BUYER_PAY"			'物流配送费用付款方式：SELLER_PAY(卖家支付)、BUYER_PAY(买家支付)、BUYER_PAY_AFTER_RECEIVE(货到付款)
	logistics_type	= "EXPRESS"				    '物流配送方式：POST(平邮)、EMS(EMS)、EXPRESS(其他快递)
	'如需添加物流信息，可以增加logistics_fee_1,logistics_payment_1,logistics_type_1,logistics_fee_2,logistics_payment_2,logistics_type_2
	'另外还需在alipayto/alipayto.asp对应位置添加。

'登陆到www.alipay.com 后,点商家服务,进去后可以看到 key 和partner  
	  
%>
