<!--#include file="alipay_Config.asp"-->
<!--#include file="alipayto/alipay_payto.asp"-->

<%
response.Charset="gb2312"
session.CodePage=936
id=request.QueryString("id")
did=request.QueryString("did")
price=request.QueryString("price")
yunfei=request.QueryString("yunfei")
shid=request.QueryString("shid")


if not isnumeric(did) or len(did)<>18 then
	response.Write("错误的订单号参数传递")
	response.End()
end if

if not isnumeric(id) or id="" or isnull(id) then	
	response.Write("错误的ID参数传递")
	response.End()
end if

if not isnumeric(price) or price="" or isnull(price) then
	response.Write("错误的价格参数传递")
	response.End()
end if

if not isnumeric(yunfei) or yunfei="" or isnull(yunfei) then
	response.Write("错误的运费参数传递")
	response.End()
end if

if not isnumeric(shid)  or shid="" or isnull(shid)  then
	response.Write("错误的运费ID号参数传递")
	response.End()
end if

total=cdbl(price)
shid=clng(yunfei)
id=trim(id)
show_url      		= return_url&id                 '网站的网址
subject				=	"支付宝支付产品"		'商品名称
body					=	"商品的详细信息请登陆会员中心查看"		'body			商品描述
out_trade_no    = did       
price		  			= cdbl(total)				'price商品单价			0.01～50000.00
quantity    	    = "1"               '商品数量,如果走购物车默认为1
discount      		= "0"               '商品折扣
seller_email   	= seller_email   '卖家的支付宝帐号
logistics_fee		= yunfei				'物流配送费用

'物流配送方式：POST(平邮)、EMS(EMS)、EXPRESS(其他快递)
'登陆 www.alipay.com 后, 点商家服务,可以看到支付宝安全校验码和合作id,导航栏的下面	 
select case shid
	case 57
		'shname="汽车托运"
		logistics_type		= "EXPRESS"
	case 58
		'shname="国内包裹"
		logistics_type		= "POST"	
	case 59
		logistics_type		= "EMS"	
		'shname="EMS特快"
	case 60
		'shname="普通快递"
		logistics_type		= "EXPRESS"		
	case else
		logistics_type		= "EXPRESS"	
end select
logistics_payment	= "BUYER_PAY"			'物流配送费用付款方式：SELLER_PAY(卖家支付)、BUYER_PAY(买家支付)、BUYER_PAY_AFTER_RECEIVE(货到付款)
			
Set AlipayObj	= New creatAlipayItemURL
itemUrl=AlipayObj.creatAlipayItemURL(subject,body,out_trade_no,price,quantity,seller_email)
'response.Redirect itemUrl
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<style>
body{margin:0; padding:0; border:0px none; background:#fff; color:#000; font-size:12px; font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif; line-height:180% }
</style>
<title>无标题文档</title>
</head>

<body>
      <div id="myTab1_Content1">	  
	  <div style="margin-bottom:10px;text-indent:12px">
	  真实姓名：<%=seller_name%>
      </div>
	  <div style="margin-bottom:10px;text-indent:12px">
	  支付宝帐户：<%=seller_email%>
      </div>
	  <div style="margin-bottom:10px;text-indent:12px">
      须支付金额：<strong style="color:#ff6600"><%=total+yunfei%></strong> 元(其中运费<strong style="color:#ff6600"><%=yunfei%></strong> 元) 
      </div>	 
	  <div style="margin-bottom:10px;text-indent:12px">
<a href="<%=itemUrl%>" id="spanlink" target="_blank"><img src="/skin/default/images/btn_pay.gif" width="126" height="31" border="0" /></a>
      </div>
	  
	  </div>
</body>
</html>