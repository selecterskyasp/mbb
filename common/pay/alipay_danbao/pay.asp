<!--#include file="alipayto/alipay_payto.asp"-->

<%
response.Charset="gb2312"
session.CodePage=936
id=request.QueryString("id")
did=request.QueryString("did")
price=request.QueryString("price")
yunfei=request.QueryString("yunfei")
shid=request.QueryString("shid")


'得到价格 如果价格为0，刚返回面议 para价格 sdot小数位数
function getPrice(byval para,sdot)
para=trim(para)
if isnull(para) or para="" or len(para)<1 or not isnumeric(para) then
	getPrice=0
	exit function
end if
para=cdbl(para)
if para=0 then
	getPrice=0
	exit function
end if
para=formatnumber(para,sdot)
if left(cstr(para),1)="." then para="0"+cstr(para) else para=cstr(para)
getPrice=para
para=""
end function

if not isnumeric(did) or len(did)<>18 then
	response.Write("错误的订单号参数传递")
	response.End()
end if

if not isnumeric(id) or id="" or isnull(id) then	
	response.Write("错误的ID参数传递")
	response.End()
end if

if not isnumeric(price) or price="" or isnull(price) then
	response.Write("错误的价格参数传递")
	response.End()
end if

if not isnumeric(yunfei) or yunfei="" or isnull(yunfei) then
	response.Write("错误的运费参数传递")
	response.End()
end if

if not isnumeric(shid)  or shid="" or isnull(shid)  then
	response.Write("错误的运费ID号参数传递")
	response.End()
end if

shid=clng(shid)
id=trim(id)
price=cdbl(price)

if clng(yunfei)>0 then
	logistics_fee	= yunfei				'物流配送费用
end if

subject			=	"来自"&webname&"的产品"		'商品名称
body			=	"请进入"&webname&"会员中心查看详细资料"		'商品描述
out_trade_no    =   did       						'按时间获取的订单号
'price		    =	cdbl(total)		'price商品单价	0.01～50000.00,注：不要出现3,000.00，价格不支持","号
quantity        =   "1"               					'商品数量,如果走购物车默认为1
discount        =   "0"               					'商品折扣

	t5				=	"支付宝付款来自"&webname&"的产品"						'按钮悬停说明
	
	out_trade_no		=	did'goods	'order_no		商户订单号
	subject			=	"来自"&webname&"的产品"		'subject		商品名称
	body			=	"请进入"&webname&"会员中心查看详细资料"'tempBody			'body			商品描述
	'price			=	"0.01" 					'price			商品单价			0.01～50000.00
	quantity		=	"1"						'type支付类型			1：商品购买2：服务购买3：网络拍卖4：捐赠
	receive_name		= "支付宝"'trim(request.form("buyerName"))			'buyer_name		买家姓名
	receive_address	=	"上海市浦东新区民生路1199弄1号27楼"'trim(request.form("address"))	'buyer_address	买家地址
	receive_zip	=	"200050"'trim(request.form("postcode"))		'buyer_zipcode	买家邮编
	receive_phone		=	"02152734798"'trim(request.form("usertel"))			'buyer_tel		买家电话号码
	receive_mobile 	=	"13255555555"	'buyer_mobile	买家手机号码
	'seller_email  = ""     '卖家帐户（收款帐户），例如：gwl25@126.com

	Set AlipayObj	= New creatAlipayItemURL
	itemUrl=AlipayObj.creatAlipayItemURL(t5,subject,body,out_trade_no,price,quantity,seller_email)

'Set AlipayObj	= New creatAlipayItemURL itemUrl=AlipayObj.creatAlipayItemURL(subject,body,out_trade_no,price,quantity,seller_email)
'response.redirect itemUrl

'Set AlipayObj	= New creatAlipayItemURL
'	itemUrl=AlipayObj.creatAlipayItemURL(subject,body,out_trade_no,price,quantity,seller_email)			
'response.Redirect itemUrl
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<style>
body{margin:0; padding:0; border:0px none; background:#fff; color:#000; font-size:12px; font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif; line-height:180% }
</style>
<title>无标题文档</title>
</head>

<body>
      <div id="myTab1_Content1">	  
	  <div style="margin-bottom:10px;text-indent:12px">
	  真实姓名：<%=seller_name%>
      </div>
	  <div style="margin-bottom:10px;text-indent:12px">
	  支付宝帐户：<%=seller_email%>
      </div>
	  <div style="margin-bottom:10px;text-indent:12px">
      须支付金额：<strong style="color:#ff6600"><%=getPrice((price+yunfei),2)%></strong> 元(其中运费 <strong style="color:#ff6600"><%=yunfei%></strong> 元) 
      </div>	 
	  <div style="margin-bottom:10px;text-indent:12px">
<a href="<%=itemUrl%>" id="spanlink" target="_blank"><img src="/skin/mbb_default/images/btn_pay.gif" width="126" height="31" border="0" /></a>
      </div>
	  
	  </div>
</body>
</html>