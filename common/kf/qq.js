﻿
var persistclose=0 //set to 0 or 1. 1 means once the bar is manually closed, it will remain closed for browser session
var startX = 852 //set x offset of bar in pixels
var startY = 130 //set y offset of bar in pixels
var verticalpos="fromtop" //enter "fromtop" or "frombottom"
var online= new Array();

function iecompattest(){
return (document.compatMode && document.compatMode!="BackCompat")? document.documentElement : document.body
}

function closekf()
{
	document.getElementById("ServiceContent").style.display="none";
	document.getElementById("kfbanner").style.display="block";
	document.cookie="kfshow=2";	
}
function showkf()
{
	document.getElementById("ServiceContent").style.display="block";
	document.getElementById("kfbanner").style.display="none";
	document.cookie="kfshow=1";
}
function staticbar(){
	barheight=document.getElementById("CustomerService").offsetHeight
	var ns = (navigator.appName.indexOf("Netscape") != -1) || window.opera;
	var d = document;
	function ml(id){
		var el=d.getElementById(id);
		if (!persistclose || persistclose && get_cookie("remainclosed")=="")
		el.style.visibility="visible"
		if(d.layers)el.style=el;
		el.sP=function(x,y){this.style.left=x+"px";this.style.top=y+"px";};
		el.x = startX;
		if (verticalpos=="fromtop")
		el.y = startY;
		else{
		el.y = ns ? pageYOffset + innerHeight : iecompattest().scrollTop + iecompattest().clientHeight;
		el.y -= startY;
		}
		return el;
	}
	window.stayTopLeft=function(){
		if (verticalpos=="fromtop"){
		var pY = ns ? pageYOffset : iecompattest().scrollTop;
		ftlObj.y += (pY + startY - ftlObj.y)/8;
		}
		else{
		var pY = ns ? pageYOffset + innerHeight - barheight: iecompattest().scrollTop + iecompattest().clientHeight - barheight;
		ftlObj.y += (pY - startY - ftlObj.y)/8;
		}
		ftlObj.sP(ftlObj.x, ftlObj.y);
		setTimeout("stayTopLeft()", 10);
	}
	ftlObj = ml("CustomerService");
	stayTopLeft();
	
	if(cjx.GetCookies("kfshow")=="2") closekf();
	if(cjx.GetCookies("kfshow")=="1") showkf();	
}

if (window.addEventListener)
window.addEventListener("load", staticbar, false)
else if (window.attachEvent)
window.attachEvent("onload", staticbar)
else if (document.getElementById)
window.onload=staticbar
