<!--#include virtual="/inc/top.asp"-->
<%
id=request.QueryString("id")
dnum=request.QueryString("dnum")
num=request.QueryString("num")
act=request.QueryString("act")
prelink=lcase(request.ServerVariables("HTTP_REFERER"))
p=request.QueryString("p")
if not chkrequest(id) or not chkDingdanNumber(dnum) then alert "参数有误","",1
act=cint(act)
if act=0 and not chkrequest(num) then alert "购买产品数量为整数","",1
if act=1 then num=0
sql="select top 1 userid from dingdan where number='"&dnum&"'"
tarr=getdbvalue(sql,1)
if tarr(0)=0 then  alert "订单不存在","",1
userid=clng(tarr(1))
erase tarr

'如果是总后台产品订单
if p="1" then
	p="/manage/dingdan/dingdandetail.asp?id="&dnum	
else
	if not isLogin() then
		alert "请先登陆","/reg",1
	end if
	getuserid=clng(getUserLoginInfo()(0))
	if getuserid<>userid then alert "非法订单操作","",1
	p="/member/shopcartDetail.asp?id="&dnum
end if

set comm=server.CreateObject("adodb.command")
with comm
	.activeconnection=conn
	.commandtype=4
	.commandtext="modifyDingdan"
	.prepared=true
	.parameters.append .createparameter("@return",16,4)
	.parameters.append .createparameter("@pid",20,1,4,id)
	.parameters.append .createparameter("@number",200,1,20,dnum)
	.parameters.append .createparameter("@pnum",20,1,4,num)
	.parameters.append .createparameter("@userid",20,1,4,userid)	
	.parameters.append .createparameter("@mode",16,1,1,act)	
	.execute()
end with
errnum=cint(comm(0))
set comm=nothing
dim arr(12)
arr(0)="成功"
arr(1)="产品不存在，可能已经下架"
arr(2)="订单不存在"
arr(3)="会员不存在"
arr(4)="当前订单状态不是 已经下单和付款没有确认"
arr(5)="产品库存不足"
arr(6)="购买的产品数量少于最小起订量"
arr(7)="购买的产品数量大于最大供应量"
arr(8)="订单清单里面没有此产品"
arr(9)="订单的总金额少于起批金额"
arr(10)="当前订单里面只剩一个产品，不能进行删除操作"
arr(11)="至少有一个产品对应的会员价格没有设置"
arr(12)="会员等级不存在"
if errnum<>0 then
	alert arr(errnum)&"，请确认您的操作!","",1	
end if
erase arr
call closeconn()
response.Redirect(p)
%>
