<%
'此函数必须包含xmlsub.aspp
'新建结点
'const xmlFileName="/xml/class.xml" 使用本函数前，必须的调用的页面定义xmlFileName的值为xml文件的路径
function createNode(byVal Parent,byVal id,byVal text)
	'读取xml
	dim pNode,tNode,tNodename
	call ConnectXmlDoc(xmlFileName)
	call createXmlobjNode("")	
	if parent=0 then
		set pNode=XmlobjDoc.SelectSingleNode("//root")
		tNodename="list"&id
	else
		set pNode=GetNodeByValue("ID",Parent)	
		tNodename="list"
	end if
	if not pNode is nothing then		
		set tNode=CreateXmlNode(pNode,tNodename,0,0,"",-1)
		call CreateXmlNode(tNode,"ID",1,1,id,-1)
		call CreateXmlNode(tNode,"Text",1,1,text,-1)
		set tNode=nothing	
		call saveXmlDoc(xmlFileName)		
		set pNode=nothing
		createNode=0
	else
		createNode=1
	end if
	call closeXmlobjNode()
	call closeXmlDoc()
end function

'编辑结点
function editNode(byVal Parent,byVal id,byVal text)
	'读取xml
	dim pNode,tNode,nodeName
	call ConnectXmlDoc(xmlFileName)
	call createXmlobjNode("")
	set pNode=GetNodeByValue("ID",id)	
	if not pNode is nothing then
		call EditXmlNode(pNode,"Text",text,1)
		call saveXmlDoc(xmlFileName)	
		set pNode=nothing
	else
		call createNode(Parent,id,text)
	end if
	editNode=0
	call closeXmlobjNode()
	call closeXmlDoc()
end function

'编辑结点
function DelNode(byVal id)
	'读取xml
	dim pNode,tNode,nodeName
	call ConnectXmlDoc(xmlFileName)
	call createXmlobjNode("")
	set pNode=GetNodeByValue("ID",id)	
	if not pNode is nothing then
		call DelXmlNode(pNode,"",1,0)
		call saveXmlDoc(xmlFileName)	
		set pNode=nothing
		DelNode=0
	else
		DelNode=1
	end if
	call closeXmlobjNode()
	call closeXmlDoc()
end function

'根据ID返回xml的Text值 多个ID用,分隔
function getTextlist(id)
dim idarr,i
getTextlist=""
if id="" then exit function
idarr=split(id,",")
call ConnectXmlDoc(xmlFileName)
call createXmlobjNode("")
dim pNode
for i=0 to ubound(idarr)
	if isnumeric(idarr(i)) and idarr(i)<>"" and not isnull(idarr(i)) and not isempty(idarr(i)) then
		set pNode=GetNodeByValue("ID",idarr(i))
		if not pNode is nothing then
			if getTextlist="" then 	getTextlist=getNodeValue(pNode,"Text") else getTextlist=getTextlist&"&nbsp;"&getNodeValue(pNode,"Text")				
			set pNode=nothing
		end if
	end if
next
erase idarr
call closeXmlobjNode()
call CloseXmlDoc()
end function

function listNode()
	call ConnectXmlDoc(xmlFileName)	
	response.Write(getNodeList(""))
	call closeXmlDoc()
end function
%>
