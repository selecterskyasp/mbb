﻿<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%Option Explicit
Response.Buffer=TRUE
server.ScriptTimeout=99999
%>
<!--#include file="cinc/function.asp"-->
<!--#include file="cinc/sub.asp"-->
<!--#include virtual="/inc/conn.asp"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>万客产品采集插件 v1.03 by selectersky</title>
<script language="javascript" src="/common/js/validator.js"></script>
<script type="text/javascript" src="/common/js/jquery.js"></script>
<script type="text/javascript" src="/common/js/extend.js"></script>
<script language="JavaScript" type="text/JavaScript">
$(document).ready(function() {
		var selects1 = new linkSelect({
		el: 'sortpath',
	url: '/xml/class.xml',
			root: 'list32',
			field: 'list',
			text: 'Text',
			value: 'ID'
			});
		});
</script>
<style type="text/css">
body{margin:0;padding:0;border:0px none;background:#fff;color:#000000;font-size:12px;font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif;line-height:180%;}	
	select,input{font-size:12px;color:#000;}
	h1{margin:0;font-size:14px;color:#1b49ac; margin-top:15px}	
	A:link,A:visited,A:active{ text-decoration: none;color: #111;}
	input,textarea{
	border:1px solid #e5e5e5;	
	margin: 1px 0px 4px 0px;	
	color: #333;	
	padding:2px 0px 0px 3px;	
}
	td{ height:22px}
textarea:focus,input:focus{	
	border: 1px solid #FFFF00;
}
.tbHeader { height: 22px; background-color:#CEDFEF; text-align:center; }
</style>
</head>
<body>
<!--
v1.03  10-11-2009
1、修改产品添加 作为独立的模块
v1.02  09-21-2009
1,修改由于数据库变动引起的不能采集的问题 
2,增加采集是加入采集来源网站
v1.01  05-14-2009
1,修改不能添加第二页产品的问题 
2,修改不能从指定起始页添加产品的问题 
3,修改检查产品是否存在时，如果产品存在，则返回存在的ID号，方便查询
4,修改产品存在，错误提示为大图保存失败的问题
5,修改一些友好提示，方便查错
6,可以删除提交网站里面的分页，增加友好性
-->
<%
dim act
act=request.QueryString("act")
select case act
case "1"
call act1()
case "2"
call act2()
case else
call act1()
end select
%>

<%sub act1()%>
<div>
<h1>第一步 输入范围</h1>
<form action="index.asp?act=2" method="post" onSubmit="return Validator.Validate(this,2);">
  <table width="600" border="0" cellspacing="0" cellpadding="0">
   <tr>
      <td  align="right">输入产品分类网址：</td>
      <td><input name="furl" type="text" id="furl" size="60" maxlength="100" dataType="Url" msg="输了正确的网址"/>
        <br />
        (如:http://www.15wk.com/ProList.asp?bid=20&sid=166)      </td>
    </tr>
  <tr>
      <td  align="right">输入采集起始页码：</td>
      <td>从<input name="furlnum" type="text"  value="1" id="furlnum" size="60" maxlength="100" dataType="Integer" msg="输入整数" /><br />至<input name="turlnum" type="text" id="turlnum" size="60" maxlength="100" value="0" dataType="Integer" msg="输入整数"/>
        <br />
        (注:输入0为采集从当前页面开始之后的所有页面的产品)      </td>
    </tr>
	<tr>
      <td  align="right">选择采集产品分类：</td>
      <td>
	  <div id="sortpath"></div>
        </td>
    </tr>
   	<tr>
      <td colspan="2"  align="center"><label>
        <input type="submit" name="Submit" value="开始采集" />
        <input type="reset" name="Submit2" value="重新填写" />
      </label></td>    
    </tr>
  </table>
  </form> 
</div>
<%end sub%>

<%sub act2()
dim prourl
dim furl,furlnum,turlnum,sortpath,tmpurl
dim i,j
dim proListArr,tarr
dim ttpage
dim ttpro,usepro,tproNum
furl=request.Form("furl")
furlnum=cint(request.Form("furlnum"))
turlnum=cint(request.Form("turlnum"))
sortpath=replace(request.Form("sortpath")," ","")
'删除网站里面的分页
furl=delUrlpara(furl,"page")
if not chkUrl(furl) then
	showErrmsg "网址不正确!",""
end if
if furlnum<1 then
	showErrmsg "起始页不能少于1!",""
end if
if not chkpara(sortpath) then
	showErrmsg "请选择产品分类!",""
end if
response.Write("<font color=red>正在获取第 <font color=red>"&furlnum&"</font> 页的信息...</font>")
response.Flush()
tmpurl=furl&"page="&furlnum
proListArr=getPageList(tmpurl,1)
response.Write("<font color=green>完成</font>&nbsp;<a href="""&tmpurl&""" target=""_blank"">查看</a><br>")
response.Flush()
ttpage=cint(proListArr(0,ubound(proListArr,2)))
tproNum=ubound(proListArr,2)-1
if ttpage=0 or tproNum<1 then
	showErrmsg "当前分类下没有找到产品或者万客的网站有问题!",""
end if
if turlnum=0 or turlnum>ttpage then turlnum=ttpage
ttpro=0:usepro=0
i=furlnum
do while  i<=turlnum	
	response.Write("正在处理第 <font color=red>"&i&"</font> 页的产品信息，当前页面产品数量 <font color=red>"&tproNum&"</font>....<hr>")
	response.Flush()
	for j=0 to tproNum-1
		ttpro=ttpro+1
		response.Write("正在处理第 <font color=red>"&j+1&"</font> 个产品....")
		response.Flush()
		tarr=addwkproducts(proListArr(0,j),proListArr(1,j),sortpath)		
		if tarr(0)=0 then
			usepro=usepro+1	
			response.Write("<font color=green>添加/修改成功</font>")
		else
			response.Write("<font color=red>添加失败</font><br><font color=red>可能原因："&tarr(1)&"</font>")
		end if	
		response.Write("<br>")
		response.Flush()
		'if ttpro>1 then exit for
	next
	i=i+1
	if i<=turlnum then
		tmpurl=furl&"page="&i
		response.Write("正在获取第 <font color=red>"&i&"</font> 页面信息...")
		response.Flush()
		erase proListArr
		proListArr=getPageList(tmpurl,0)
		tproNum=ubound(proListArr,2)
		response.Write("<font color=green>完成</font>&nbsp;<a href="""&tmpurl&""" target=""_blank"">查看</a><br>")
		response.Flush()
	end if
loop
response.Write("<font color=green>所有操作已经完成!</font>&nbsp;总共找到产品数:<font color=red>"&ttpro&"</font>&nbsp;其中已经加入数据库产品数:<font color=red>"&usepro&"</font>")
response.Flush()
end sub
conn.close
set conn=nothing
%>
</body>
</html>
