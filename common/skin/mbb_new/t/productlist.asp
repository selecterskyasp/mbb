<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
<title>彩棉网</title>
<link href="include/styles.css" rel="stylesheet" type="text/css" />
</head>

<body>
<!--#include file="include/head.asp"-->

<!--banner-->
<div class="banner_ad"><img src="images/banner02.jpg"></div>
<!--banner end-->
<div class="pd_view">首页 -> 男袜 > 商品列表</div>
<div class="content">
	<!--左边分类开始-->
	<div class="productList_left">
		<div class="pd_one write font14"><b>全部商品分类</b></div>
		<div class="pd_line">
			<h1>男士(Men)</h1>
			<ul>
				<li><a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a> | <a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a></li>
			</ul>
			<h1>女士(Women)</h1>
			<ul>
				<li><a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a> | <a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a></li>
			</ul>
			<h1>儿童(Kids)</h1>
			<ul>
				<li><a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a> | <a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a></li>
			</ul>
			<h1>儿童(Kids)</h1>
			<ul>
				<li><a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a> | <a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a></li>
			</ul>
			<h1>儿童(Kids)</h1>
			<ul>
				<li><a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a> | <a href="#">普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜普通长袜</a> | <a href="#">短袜</a> | <a href="#">船袜</a></li>
			</ul>
		</div>
		
		<!--价格区间开始-->
		<div class="pd_one margin12 write font14"><b>价格区间查看</b></div>
		<div class="pd_price">
			<ul>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">10元以上</a></li>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">10-50元</a></li>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">50-80元</a></li>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">80-100元</a></li>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">100-120元</a></li>
				<li><img src="images/gonggao_ico.gif" align="absmiddle"> <a href="#">120元以上</a></li>
			</ul>
		</div>
		
		<!--商品品牌开始-->
		<div class="pd_one margin12 write font14"><b>商品品牌查看</b></div>
		<div class="pd_price">
			<ul>
				<li><img src="images/la.gif"></li>
				<li><img src="images/ml.gif"></li>
			</ul>
		</div>
		
		<!--浏览过的商品开始-->
		<div class="pd_one margin12">
			<ul>
				<li class="l write font14"><b>最近看过的商品</b></li>
				<li class="r"><a href="#">清除..</a>&nbsp;&nbsp;</li>
			</ul>
		</div>
		<div class="pd_liunan">
			<ul>
				<li>
					<dl>
					  <a href="#"><img src="http://img2.vcotton.com/upload/12/42/792.jpg" width=60 height=60 border="0"></a>
					</dl>
					<dd><a href="#">安迪沃男士时尚潮流船袜 中花灰</a></dd>
					<dd class="red margin6"><b>￥45元</b>&nbsp;&nbsp;<s class="light_grey">￥55元</s></dd>
				</li>
			</ul>
			<ul>
				<li>
					<dl>
					  <a href="#"><img src="http://img2.vcotton.com/upload/12/42/792.jpg" width=60 height=60 border="0"></a>
					</dl>
					<dd><a href="#">安迪沃男士时尚潮流船袜 中花灰</a></dd>
					<dd class="red margin6"><b>￥45元</b>&nbsp;&nbsp;<s class="light_grey">￥55元</s></dd>
				</li>
			</ul>
			<ul>
				<li>
					<dl>
					  <a href="#"><img src="http://img2.vcotton.com/upload/12/42/792.jpg" width=60 height=60 border="0"></a>
					</dl>
					<dd><a href="#">安迪沃男士时尚潮流船袜 中花灰</a></dd>
					<dd class="red margin6"><b>￥45元</b>&nbsp;&nbsp;<s class="light_grey">￥55元</s></dd>
				</li>
			</ul>
		</div>
	</div>
	<!--左边分类结束-->
	
	<!--右边商品开始-->
	<div class="productList_right">
		<div class="oby_box">
			<!--排序方式-->
			<ul class="l">
				<li>排序方式：
				  <select name="select">
				    <option>按新品上架</option>
				    <option>按销量从高到低</option>
				    <option>按评价从高到低</option>
				    <option>按价格从低到高</option>
				    <option>按价格从高到低</option>
			      </select></li>
				  <li><img src="images/jiege.gif" align="absmiddle" ></li>
			</ul>
			<!--翻页-->
			<ul class="r">
				<div class="pages">
					<ul><li class=prev><span>上一页</span></li><li class=thispage><a href=#>1</a></li><li class=other><a href=#>2</a></li><li class=other><a href=#>3</a></li><li class=other><a href=#>4</a></li><li class=end><a href=#>5</a></li><li class=ellipsis>...</li><li class=end><a href=#>91</a></li><li class=next><a href=#>下一页</a></li></ul>
				</div>
			</ul>
	  </div>
		<!--产品开始-->
		<div class="search_list margin6">
			<ul>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li><a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li>
					<a href="#"><img src="http://img2.vcotton.com/upload/6/6/3506.jpg" /></a>
					<h3><a href="#">波林男士时尚字母船袜 紫色</a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
				<li style='margin-right:0'>
					<a href="#"><img src="http://img2.vcotton.com/upload/7/27/3827.jpg" /></a>
					<h3><a href="#">波林男士时尚横条船袜 暮蓝色 </a></h3>
					<span class="pres">折后价：<b>￥15</b></span>&nbsp;&nbsp;&nbsp;&nbsp;<s class="light_grey">原价：￥25</s>
				</li>
			</ul>
		</div>
		<!--产品结束-->
		
		<!--翻页开始-->
			<!--翻页-->
				<div class="pages r">
					<ul><li class=prev><span>上一页</span></li><li class=thispage><a href=#>1</a></li><li class=other><a href=#>2</a></li><li class=other><a href=#>3</a></li><li class=other><a href=#>4</a></li><li class=end><a href=#>5</a></li><li class=ellipsis>...</li><li class=end><a href=#>91</a></li><li class=next><a href=#>下一页</a></li></ul>
				</div>
		<!--翻页结束-->
		
	</div>
</div>
<br>
<!--#include file="include/copy.asp"-->
</body>
</html>
