<!--顶部登陆-->
<div class="top_nav">
	<div class="user_login">您好，欢迎光临彩棉网！ <a href="#"><span>登录</span></a> | <a href="#"><span>注册</span></a> | &nbsp;<font class="head_r_my"><a href="#">我的账户</a></font> | <a href="#">购物车</a> | <a href="#">帮助</a> | <a href="#">关于彩棉</a> | <font class="head_r_fad">&nbsp;<a href="#"><span>收藏本站</span></a></font></div>
</div>
<!--顶部登陆end-->

<!--头部开始-->
<div class="header">
	<div class="logo">
		<ul>
			<li class="l"><img src="images/logo.gif" alt="纯棉全球高端品牌袜子营销中心"><img src="images/logo_text.gif"></li>
			<li class="r"><img src="images/logo_tel.gif"></li>
		</ul>
	</div>
	<div class="nav">
		<ul class="l">
			<li style=" line-height:32px;">&nbsp;　<a href="#" class="main">首页</a>　　<a href="#" class="main">男袜</a>　　<a href="#" class="main">女袜</a>　　<a href="#" class="main">童袜</a>　　<a href="#" class="main">情侣袜</a>　　<a href="#" class="main">男内衣</a>　　<a href="#" class="main">女内衣</a>　　<a href="#" class="main">围巾</a></li>
		</ul>
		<ul class="r nav_shop">
			<li class="nav_shop1">&nbsp;</li>
			<li class="nav_shop2">购物车共有 10 件商品</li>
			<li class="nav_shop3"><a href=#><b>去结算</b></a></li>
		</ul>
	</div>
	<div id="search">
		<div class="vcotton_box">
			<div class="search_box">
				<input name="keywords" class="head_search text" type="text" id="keyword"  value="请输入您想要的商品.."/>
				<input type="submit" value="" class="but">
			</div>
			<span class="searchtit">热门关键词：</span>
			<ul class="hotkey">
				<li><a href="/searchs?keywords=%E8%88%B9%E8%A2%9C">船袜</a></li>
				<li><a href="/searchs?keywords=%E5%95%86%E5%8A%A1">商务</a></li>
				<li><a href="/searchs?keywords=%E7%BB%8F%E5%85%B8">经典</a></li>
				<li><a href="/searchs?keywords=%E7%94%B7%E5%A3%AB">男士</a></li>
				<li><a href="/searchs?keywords=%E5%A5%B3%E5%A3%AB">女士</a></li>
				<li><a href="/searchs?keywords=%E5%AD%A6%E9%99%A2">学院</a></li>
				<li><a href="/searchs?keywords=%E7%9F%AD%E8%A2%9C">短袜</a></li>
				<li><a href="/searchs?keywords=%E8%BF%90%E5%8A%A8">运动</a></li>
				<li><a href="/searchs/?attr[52]=%E7%BA%AF%E8%89%B2&display=%E7%BA%AF%E8%89%B2">纯色</a></li>
				<li><a href="/searchs?keywords=%E6%97%B6%E5%B0%9A">时尚</a></li>
				<li><a href="/searchs?attr[56]=%E4%B8%89%E6%9D%A1%E8%A3%85&display=%E4%B8%89%E6%9D%A1%E8%A3%85">三条装</a></li>
				<li><a href="/searchs?keywords=%E4%B8%9D%E5%85%89%E6%A3%89">丝光棉</a></li>
				<li><a href="/searchs?attr[54]=%E7%AB%8B%E4%BD%93%E5%89%AA%E8%A3%81&display=%E7%AB%8B%E4%BD%93%E5%89%AA%E8%A3%81">立体剪裁</a></li>
				<li><a href="/searchs?keywords=%E5%8D%B0%E8%8A%B1">印花</a></li>
			</ul>		
		</div>
	</div>
</div>
<!--头部开始end-->