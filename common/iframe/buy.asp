﻿<!--#include virtual="/inc/top.asp"-->
<!--#include file="../products/shopcartSub.asp"-->
<%
id=request.QueryString("id")
color=request.QueryString("color")
num=request.QueryString("num")
size=request.QueryString("size")

if not chkrequest(id) then
	alert "产品参数丢失","",1
end if

dim tarr,errnum
sql="select top 1 color,size,min,max from  products where flag=1 and id="&id

tarr=getDBvalue(sql,4)
if tarr(0)=0 then
	alert "产品不存在或者已经下架","",1
end if
if clng(tarr(4))<clng(tarr(3)) and clng(tarr(4))<>-1 then
	alert "产品库存不足，不能购买","",1
end if
if not chkPara(color) then
	color=tarr(1)
end if
if not chkPara(size) then
	size=tarr(2)
end if
if not chkrequest(num) then
	num=tarr(3)
else
	num=clng(num)
	if (num<clng(tarr(3)) or num>clng(tarr(4))) and clng(tarr(4))<>-1 then
		alert "购买产品的数量必须在 "&tarr(3)&" 和 "&tarr(4)&" 之间","",1
	end if
end if
erase tarr


if isLogin() then

	errnum=addDbShopcart(id,num,color,size,"",1)
	if errnum<>0 then
		alert "添加产品失败，错误ID号："&errnum,"",1	
	end if
else
	
	errnum=addTmpShopcart(id,num,color,size)
	if errnum<>0 then
		alert "添加产品失败，错误ID号："&errnum,"",1	
	end if
end if
call closeconn()
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<style>
body{background:#fff; color:#000; font-size:12px; font-family:'宋体',arial,'Lucida Grande','Lucida Sans Unicode','新宋体',verdana,sans-serif; line-height:180%}
</style>
</head>

<body>
<div style="padding:10px" align="center">
<img src="<%=webskinpath%>/images/login_ico_right.gif" align="absmiddle" vspace="10" id="pic" />
        <font style="font-size:14px;font-weight:bold;color:#007236;" id="finfont">成功加入到购物车!</font>
        <font style="font-size:14px;font-weight:bold;color:#007236;display:none;" id="repeatfont">您已经添加过此商品到购物车!</font><br />
		您的购物车中已有 <a href="javascript:parent.location='/products/my_cart.asp'" target="_blank"><b style="text-decoration:underline;"><%=request.Cookies("buyproductnum")%></b></a> 类商品<br />
		<a href="#" onclick="parent.location='/products/my_cart.asp';">查看购物车</a>&nbsp;<a href="javascript:parent.window.jBox.close();">继续购物</a>
</div>
</body>
</html>
