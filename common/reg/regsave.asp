<!--#include virtual="/inc/top.asp"-->
<!--#include virtual="/common/inc/md5.asp"-->
<!--#include virtual="/common/member/mailSendSub.asp"-->
<%
'addrP=request.ServerVariables("LOCAL_ADDR")

code=lcase(request.Form("f_code"))
if code<>lcase(session("getcode")) or chkNull(code,1) then
	alert "验证码错误，请确认！","",1
end if

f_email=checkstr(request.form("f_email"))
f_pw1=checkstr(request.form("f_pw1"))
f_pw2=checkstr(request.Form("f_pw2"))
f_name=checkstr(request.Form("f_name"))
f_sex=request.Form("f_sex")
area=replace(request.Form("area")," ","")
f_address=checkstr(request.Form("f_address"))
f_yb=checkstr(request.Form("f_yb"))
f_tel=checkstr(request.Form("f_tel"))
f_mobile=checkstr(request.Form("f_mobile"))

errmsg=""
founderr=false
'---------------------数据判断-----------------------------

if not IsValidEmail(f_email) then
	founderr=true
	errmsg="<li>邮箱输入错误或不合法</li>"
end if

tarr=split(webping,"|")
for i=0 to ubound(tarr)
	if not chkNull(tarr(i),2) then
		if instr(f_email,tarr(i))>0 then
			finderr=true
			errmsg=errmsg&"<br>您的用户名有敏感关键字，无法提交"
			exit for
		end if
	end if
next
erase tarr

if f_pw1<>f_pw2 then
	founderr=true
	errmsg=errmsg&"<li>两次密码输入不一样</li>"
end if

if len(f_pw1)<6 or len(f_pw1)>20 then
	founderr=true
	errmsg=errmsg&"<li>密码输入错误(长度为6-20字符)</li>"
end if

if not (chkRange(f_name,2,6) and chkChinese(f_name)) then
	founderr=true
	errmsg=errmsg&"<li>真实姓名输入错误(长度为2-6个汉字)</li>"	
end if

if f_sex="0" and f_sex="1" then
	founderr=true
	errmsg=errmsg&"<li>请选择您的性别</li>"		
end if

if not chkPara(area) then
	founderr=true
	errmsg=errmsg&"<li>请选中您所在的省份城市地区</li>"		
end if

if not chkRange(f_address,4,50) then
	founderr=true
	errmsg=errmsg&"<li>详细街道地址输入错误(4-50字)</li>"		
end if
if not (chkrequest(f_yb) and chkRange(f_yb,2,10)) then
	founderr=true
	errmsg=errmsg&"<li>邮政编码输入错误(6为数字)</li>"		
end if

if not chkRange(f_tel,2,20) and not chkRange(f_mobile,2,20) then
	founderr=true
	errmsg=errmsg&"<li>联系电话和手机请至少输入一个</li>"		
end if

if cint(webclosereg)=1 then
founderr=true
errmsg=errmsg&"<li>会员注册已经关闭,请与我们联系</li>"
end if

citypingname=split(webping,"|")
for i=0 to UBound(citypingname)
if citypingname(i)<>"" then
if instr(f_email,citypingname(i))<>0 then
founderr=true
errmsg=errmsg&"<li>此用户名已被禁止注册</li>"
exit for
end if
end if
next
if not founderr then
	set rs=Server.CreateObject("Adodb.Recordset")
	rs.Open "Select id from [user] where useremail='"&f_email&"'",conn,1,1
	if not rs.EOF then
	founderr=true
	errmsg=errmsg&"<li>您注册的邮箱已经存在，请选用其它邮箱</li>" 
	end if
	rs.close
	set rs=nothing
end if
if founderr then
	call showmsg(errmsg,"",0)
end if
'-----------------------保存数据---------------------------
f_pw1=md5(f_pw1)
jihouNum=getRndNum(16)
'是否需要激活 websendmail=true 是,否则 不是
if websendmail then
isjihou=0
flag=0
else
isjihou=1
flag=1
end if
sql="insert into [user] (type,zheke,useremail,password,truename,area,address,postcode,tel,mobile,sex,createdon,flag,money,jifen,jihouNum,isjihou,buynum,loginNum,isfindpass)values(1,1,'"&f_email&"','"&f_pw1&"','"&f_name&"','"&area&"','"&f_address&"','"&f_yb&"','"&f_tel&"','"&f_mobile&"',"&f_sex&",'"&now()&"',"&flag&",0,0,'"&jihouNum&"',"&isjihou&",0,0,0)"
id=setDBvalue(sql)
response.Cookies("islogin").path="/"
response.Cookies("islogin")("id") = id
response.cookies("islogin")("pass")=f_pw1
response.cookies("islogin")("email")=f_email	
response.Cookies("isLogin")("type")=1	
Response.Cookies("isLogin").Expires=dateadd("d",7,now())
if websendmail then
	call  sendJihouMail(id)		
	response.Redirect("reguser2.asp?email="&f_email)		
else
	call showmsg("恭喜，"&webname&"会员注册成功，马上登陆体验我们的会员服务吧","/common/member",1)
end if
	
%>