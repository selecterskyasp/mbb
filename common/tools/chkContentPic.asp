<%@LANGUAGE="VBSCRIPT" CODEPAGE="65001"%>
<%Option Explicit
on error resume next
Response.Buffer=TRUE
server.ScriptTimeout=99999
%>
<!--#include virtual="/inc/conn.asp"-->
<!--#include file="../inc/safe_info.asp"-->
<%
'功能：检查产品内容的图片是否是本站的链接 如果不是，则远程下载和更新
dim tarr,tarr2,num
dim content,id,hh
sql="select id,content,hh from products"
tarr=getdbvaluelist(sql)
num=0
dim i
if clng(tarr(0,0))<>0 then
	for i=0 to ubound(tarr,2)
		content=tarr(1,i)
		hh=tarr(2,i)	
		id=tarr(0,i)
		response.Write("正在检查产品ID为 <b>"&id&"</b> 的记录...")
		response.Flush()
		tarr2=autoReplacePic(content,hh)		
		if tarr2(1)>0 then
			content=tarr2(2)
			sql="update products set content='"&content&"' where id="&id
			conn.execute(sql)
			num=num+1
		end if
		response.Write(" <font color=green>检查完成</font>，共找到了 <b>"&tarr2(0)&"</b> 个图片，共保存了 <b>"&tarr2(1)&"</b> 个图片<br>")
		response.Flush()
		erase tarr2	
			
	next
end if
erase tarr
response.Write("<font color=red>所以操作已经完成，共替换了 <b>"&num&"</b> 个产品!</font>")
call closeconn()

'自动保存内容里面的图片 图片保存地址：/userfiles 命名方式：编号+随机图片名
'stbody 要替换的内容 shh产品的编号
'返回数组 共找到的图片总数,已经保存的图片总数,替换后的内容
function autoReplacePic(byval stbody,shh)
on error resume next
dim tmparr
dim tmpurl,picurl,i
dim tpic,picExt,findnum,savenum
dim issave
tmparr=findContentPic(stbody)
findnum=0:savenum=0
if tmparr(0)=1 then
	for i=1 to ubound(tmparr)
		picurl=lcase(tmparr(i))
		tmpurl=tmparr(i)
		if left(picurl,4)<>"http" then
			if left(picurl,1)<>"/" then picurl="/"&picurl
			picurl="http://www.ywhqs.com"&picurl
		end if
		if instr(tmpurl,"uploadpages")=0 then
			picExt=getExt(picurl)
			tpic="/uploadfiles/"&shh&"_"&makeFilename()&picExt			
			issave=SaveRemoteFile(tpic,picurl)
			if issave then
				savenum=savenum+1
				stbody=replace(stbody,tmpurl,tpic)
			end if
			findnum=findnum+1
		end if
	next
end if
erase tmparr
autoReplacePic=array(findnum,savenum,stbody)
stbody=""
end function

'==================================================
'过程名：SaveRemoteFile
'作  用：保存远程的图片到本地
'参  数：LocalFileName ------ 本地文件名
'参  数：RemoteFileUrl ------ 远程文件URL
'==================================================
Function SaveRemoteFile(byVal LocalFileName,byVal RemoteFileUrl)
on error resume next
    SaveRemoteFile=false
	dim Ads,Retrieval,GetRemoteData	
	

	Set Retrieval = Server.CreateObject("Microsoft.XMLHTTP")
	With Retrieval
		.Open "Get", RemoteFileUrl, False, "", ""
		.Send
		if err then
			SaveRemoteFile=False
            Exit Function
		end if
        If .Readystate<>4 or .status<>200 then
            SaveRemoteFile=False
            Exit Function
        End If
		GetRemoteData = .ResponseBody
	End With
	Set Retrieval = Nothing
	Set Ads = Server.CreateObject("Adodb.Stream")
	With Ads
		.Type = 1
		.Open
		.Write GetRemoteData
		.SaveToFile server.MapPath(LocalFileName),2
		if err then
			SaveRemoteFile=False
            Exit Function
		end if
		.Cancel()
		.Close()
	End With
	Set Ads=nothing	
	SaveRemoteFile=true
end Function

'获取扩展名
function GetExt(byVal FileName)
	GetExt = "."&right(FileName,len(FileName)-instrrev(FileName,"."))	
	FileName=""	
end function

'查找内容里面的图片 返回一维数组 arr(0) 表示是否有找到 值0为没有找到 值为1有找到
function findContentPic(byval stbody)
redim tmparr(0)
dim matchs,i
dim findnum
tmparr(0)=0
findnum=0
dim regEx
set regEx=new regExp
regEx.ignoreCase=true
regEx.Global=True
regEx.pattern="src=.+?( |/>|>|/&gt;|&gt;)"
set matchs=regEx.execute(stbody)
for each i in matchs
	tmparr(0)=1
	redim preserve tmparr(findnum+1)
	tmparr(findnum+1)=replace(replace(replace(replace(replace(replace(replace(replace(i.value,"src=",""),"&quot;",""),"""","")," ",""),"/>",""),">",""),"/&gt;",""),"&gt;","")
	findnum=findnum+1
next
set regEx=nothing
set matchs=nothing
findContentPic=tmparr
stbody="":erase tmparr
end function
'生成一个文件名 注：无扩展名
function makeFilename()
dim tmpfilename
dim rndnum
randomize timer
rndnum=int(rnd*100)
tmpfilename=replace(replace(replace(replace(now(),":",""),"/","")," ","_"),"-","")
makeFilename=tmpfilename&rndnum
end function
%>
